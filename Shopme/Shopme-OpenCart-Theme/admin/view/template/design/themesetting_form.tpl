<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-banner" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
	  
	   <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $text_form; ?></a></li>
            <li><a href="#tab-data" data-toggle="tab">support</a></li>
			<li><a href="https://www.createwebsite.net/freebies/shopme/documentation.html" target="_blank">Documentation</a></li>
          
          
          </ul>
		  <div class="tab-content">
		   <div class="tab-pane active" id="tab-general">
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-banner" class="form-horizontal">
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
		  
		  <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_facebook; ?></label>
            <div class="col-sm-10">
              <input type="text" name="facebook" value="<?php echo $facebook; ?>" placeholder="<?php echo $entry_facebook; ?>" id="input-name" class="form-control" />
            </div>
          </div>
		  
		  <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_twitter; ?></label>
            <div class="col-sm-10">
              <input type="text" name="twitter" value="<?php echo $twitter; ?>" placeholder="<?php echo $entry_twitter; ?>" id="input-name" class="form-control" />
            </div>
          </div>
		  
		  <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_googleplus; ?></label>
            <div class="col-sm-10">
              <input type="text" name="googleplus" value="<?php echo $googleplus; ?>" placeholder="<?php echo $entry_googleplus; ?>" id="input-name" class="form-control" />
            </div>
          </div>
		  
		  <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_pintrace; ?></label>
            <div class="col-sm-10">
              <input type="text" name="pintrace" value="<?php echo $pintrace; ?>" placeholder="<?php echo $entry_pintrace; ?>" id="input-name" class="form-control" />
            </div>
          </div>
		  
		  <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_linkedin; ?></label>
            <div class="col-sm-10">
              <input type="text" name="linkedin" value="<?php echo $linkedin; ?>" placeholder="<?php echo $entry_linkedin; ?>" id="input-name" class="form-control" />
            </div>
          </div>
		  
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="status" id="input-status" class="form-control">
                <?php if ($status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <br />
          
         
        </form>
      </div>
	  </div>
	  <div class="tab-pane" id="tab-data">
	 If you have any issue while using please keep contact us via:<br />
  Helpdesk: <a href="https://www.createwebsite.net/">https://www.createwebsite.net</a><br />
  Email: Send us for <a href="mailto:info@createwebsite.net">info@createwebsite.net</a> <br />
	  </div>
	  
	  
	  <div class="tab-pane" id="tab-documentation">
	 
	  </div>
	  
	  
	    </div>
	  
    </div>
  </div>
   
  <script type="text/javascript"><!--
$('#language a:first').tab('show');
//--></script> 
</div>
<?php echo $footer; ?> 