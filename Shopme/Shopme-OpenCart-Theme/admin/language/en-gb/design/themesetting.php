<?php
// Heading
$_['heading_title']     = 'Theme setting';

// Text
$_['text_success']      = 'Success: You have modified Theme setting!';
$_['text_list']         = 'Theme List';
$_['text_add']          = 'Add Theme';
$_['text_edit']         = 'Edit Theme';
$_['text_default']      = 'Default';

// Column
$_['column_name']       = 'Theme Name';
$_['column_status']     = 'Status';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']        = 'Theme Name';
$_['entry_facebook']        = 'Facebook';
$_['entry_twitter']        = 'Twitter';
$_['entry_googleplus']        = 'Google Plus';
$_['entry_pintrace']        = 'Pintrace';
$_['entry_linkedin']        = 'Linkedin';
$_['entry_title']       = 'Title';
$_['entry_link']        = 'Link';
$_['entry_image']       = 'Image';
$_['entry_status']      = 'Status';
$_['entry_sort_order']  = 'Sort Order';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Theme setting!';
$_['error_name']       = 'Theme Name must be between 3 and 64 characters!';
$_['error_title']      = 'Theme Title must be between 2 and 64 characters!';