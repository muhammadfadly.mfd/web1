-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 31, 2021 at 08:05 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fadli`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_amonia`
--

CREATE TABLE `tbl_amonia` (
  `id` int(100) NOT NULL,
  `status` text NOT NULL,
  `nilai` int(11) NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_amonia`
--

INSERT INTO `tbl_amonia` (`id`, `status`, `nilai`, `waktu`) VALUES
(1, 'aman', 13, '2020-11-20 06:48:42'),
(2, 'aman', 14, '2020-11-20 06:48:42'),
(3, 'bahaya', 25, '2020-11-20 06:49:14'),
(4, 'aman', 17, '2020-11-20 06:49:26'),
(5, 'aman', 13, '2020-12-06 05:14:51'),
(6, 'aman', 14, '2020-12-06 05:14:51'),
(7, 'aman', 13, '2020-12-06 05:16:08'),
(8, 'aman', 14, '2020-12-06 05:16:08'),
(9, 'aman', 13, '2020-12-06 05:16:08'),
(10, 'aman', 14, '2020-12-06 05:16:08'),
(11, 'aman', 13, '2020-12-06 05:16:08'),
(12, 'aman', 14, '2020-12-06 05:16:08'),
(13, 'aman', 13, '2020-12-06 05:16:08'),
(14, 'aman', 14, '2020-12-06 05:16:08'),
(15, 'aman', 13, '2020-12-06 05:16:08'),
(16, 'aman', 14, '2020-12-06 05:16:08'),
(17, 'aman', 20, '2021-01-08 17:29:06'),
(18, 'aman', 13, '2020-11-20 06:48:42'),
(19, 'aman', 23, '2021-01-31 04:30:22'),
(20, 'aman', 25, '2021-01-29 05:56:51'),
(21, 'aman', 29, '2021-01-30 05:56:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_amonia`
--
ALTER TABLE `tbl_amonia`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_amonia`
--
ALTER TABLE `tbl_amonia`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
