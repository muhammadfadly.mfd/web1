-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 29 Jan 2021 pada 10.31
-- Versi server: 10.4.17-MariaDB
-- Versi PHP: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sensor_amoniak`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_amonia`
--

CREATE TABLE `tbl_amonia` (
  `id` int(100) NOT NULL,
  `status` text NOT NULL,
  `nilai` int(11) NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_amonia`
--

INSERT INTO `tbl_amonia` (`id`, `status`, `nilai`, `waktu`) VALUES
(1, 'aman', 13, '2020-11-20 06:48:42'),
(2, 'aman', 14, '2020-11-20 06:48:42'),
(3, 'bahaya', 25, '2020-11-20 06:49:14'),
(4, 'aman', 17, '2020-11-20 06:49:26'),
(5, 'aman', 13, '2020-12-06 05:14:51'),
(6, 'aman', 14, '2020-12-06 05:14:51'),
(7, 'aman', 13, '2020-12-06 05:16:08'),
(8, 'aman', 14, '2020-12-06 05:16:08'),
(9, 'aman', 13, '2020-12-06 05:16:08'),
(10, 'aman', 14, '2020-12-06 05:16:08'),
(11, 'aman', 13, '2020-12-06 05:16:08'),
(12, 'aman', 14, '2020-12-06 05:16:08'),
(13, 'aman', 13, '2020-12-06 05:16:08'),
(14, 'aman', 14, '2020-12-06 05:16:08'),
(15, 'aman', 13, '2020-12-06 05:16:08'),
(16, 'aman', 14, '2020-12-06 05:16:08'),
(17, 'aman', 20, '2021-01-08 17:29:06');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_amonia`
--
ALTER TABLE `tbl_amonia`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_amonia`
--
ALTER TABLE `tbl_amonia`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
