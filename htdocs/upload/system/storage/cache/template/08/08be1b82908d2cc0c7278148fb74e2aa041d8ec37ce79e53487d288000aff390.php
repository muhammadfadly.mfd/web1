<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* zDream/template/extension/module/featured.twig */
class __TwigTemplate_1effbc27efb14e88282a479dd3f126f362f6e52f08601609a8a3d28313019bfd extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"heading\"><h3>";
        echo ($context["heading_title"] ?? null);
        echo "</h3></div>
<div class=\"product-content\">
\t<div class=\"row\">
\t ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 5
            echo "\t  <div class=\"product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12\">
\t\t<div class=\"product-thumb transition\">
\t\t  <div class=\"image\"><a href=\"";
            // line 7
            echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 7);
            echo "\"><img src=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb", [], "any", false, false, false, 7);
            echo "\" alt=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 7);
            echo "\" title=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 7);
            echo "\" class=\"img-responsive\" /></a></div>
\t\t  <div class=\"caption\">
\t\t\t<h4><a href=\"";
            // line 9
            echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 9);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 9);
            echo "</a></h4>
\t\t\t
\t\t\t";
            // line 11
            if (twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 11)) {
                // line 12
                echo "\t\t\t<div class=\"rating\">
\t\t\t  ";
                // line 13
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(5);
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 14
                    echo "\t\t\t  ";
                    if ((twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 14) < $context["i"])) {
                        // line 15
                        echo "\t\t\t  <span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
\t\t\t  ";
                    } else {
                        // line 17
                        echo "\t\t\t  <span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
\t\t\t  ";
                    }
                    // line 19
                    echo "\t\t\t  ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 20
                echo "\t\t\t</div>
\t\t\t";
            }
            // line 22
            echo "\t\t\t";
            if (twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 22)) {
                // line 23
                echo "\t\t\t<p class=\"price\">
\t\t\t  ";
                // line 24
                if ( !twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 24)) {
                    // line 25
                    echo "\t\t\t  ";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 25);
                    echo "
\t\t\t  ";
                } else {
                    // line 27
                    echo "\t\t\t  <span class=\"price-new\">";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 27);
                    echo "</span> <span class=\"price-old\">";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 27);
                    echo "</span>
\t\t\t  ";
                }
                // line 29
                echo "


\t\t\t</p>
\t\t\t";
            }
            // line 34
            echo "\t\t  </div>
\t\t  <div class=\"button-group\">
\t\t\t<button type=\"button\" onclick=\"cart.add('";
            // line 36
            echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 36);
            echo "');\"><i class=\"fa fa-shopping-cart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
            echo ($context["button_cart"] ?? null);
            echo "</span></button>
\t\t\t<button type=\"button\" data-toggle=\"tooltip\" title=\"";
            // line 37
            echo ($context["button_wishlist"] ?? null);
            echo "\" onclick=\"wishlist.add('";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 37);
            echo "');\"><i class=\"fa fa-heart\"></i></button>
\t\t\t<button type=\"button\" data-toggle=\"tooltip\" title=\"";
            // line 38
            echo ($context["button_compare"] ?? null);
            echo "\" onclick=\"compare.add('";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 38);
            echo "');\"><i class=\"fa fa-exchange\"></i></button>
\t\t  </div>
\t\t</div>
\t  </div>
\t  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "\t</div>
\t
\t  ";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 46
            echo "\t  <div class=\"product-layout-col\">
\t   <div class=\"product-thumb transition\">
\t\t<div class=\"col-sm-4\">
\t\t  <div class=\"image\"><a href=\"";
            // line 49
            echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 49);
            echo "\"><img src=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb", [], "any", false, false, false, 49);
            echo "\" alt=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 49);
            echo "\" title=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 49);
            echo "\" class=\"img-responsive\" /></a></div>
\t\t</div>
\t\t<div class=\"col-sm-8\">
\t\t  <div class=\"caption\">
\t\t\t<h4><a href=\"";
            // line 53
            echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 53);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 53);
            echo "</a></h4>
\t\t\t";
            // line 54
            if (twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 54)) {
                // line 55
                echo "\t\t\t<div class=\"rating\">
\t\t\t  ";
                // line 56
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(5);
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 57
                    echo "\t\t\t  ";
                    if ((twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 57) < $context["i"])) {
                        // line 58
                        echo "\t\t\t  <span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
\t\t\t  ";
                    } else {
                        // line 60
                        echo "\t\t\t  <span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
\t\t\t  ";
                    }
                    // line 62
                    echo "\t\t\t  ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 63
                echo "\t\t\t</div>
\t\t\t";
            }
            // line 65
            echo "\t\t\t";
            if (twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 65)) {
                // line 66
                echo "\t\t\t<p class=\"price\">
\t\t\t  ";
                // line 67
                if ( !twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 67)) {
                    // line 68
                    echo "\t\t\t  ";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 68);
                    echo "
\t\t\t  ";
                } else {
                    // line 70
                    echo "\t\t\t  <span class=\"price-new\">";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 70);
                    echo "</span> <span class=\"price-old\">";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 70);
                    echo "</span>
\t\t\t  ";
                }
                // line 72
                echo "


\t\t\t</p>
\t\t\t";
            }
            // line 77
            echo "\t\t  </div>
\t\t </div>
\t   </div>
\t  </div>
\t  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 82
        echo "\t
</div>";
    }

    public function getTemplateName()
    {
        return "zDream/template/extension/module/featured.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  259 => 82,  249 => 77,  242 => 72,  234 => 70,  228 => 68,  226 => 67,  223 => 66,  220 => 65,  216 => 63,  210 => 62,  206 => 60,  202 => 58,  199 => 57,  195 => 56,  192 => 55,  190 => 54,  184 => 53,  171 => 49,  166 => 46,  162 => 45,  158 => 43,  145 => 38,  139 => 37,  133 => 36,  129 => 34,  122 => 29,  114 => 27,  108 => 25,  106 => 24,  103 => 23,  100 => 22,  96 => 20,  90 => 19,  86 => 17,  82 => 15,  79 => 14,  75 => 13,  72 => 12,  70 => 11,  63 => 9,  52 => 7,  48 => 5,  44 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "zDream/template/extension/module/featured.twig", "");
    }
}
