<?php
require("koneksi.php");
$perintah = "SELECT *FROM datasensor ORDER BY waktu ASC";
$eksekusi = mysqli_query($konek, $perintah);
$cek = mysqli_affected_rows($konek);

if($cek > 0){
    $response["kode"] = 1;
    $response["pesan"] = "Data Tersedia";
    $response["data"]= array();

    while($ambil = mysqli_fetch_object($eksekusi)){
        $F["id"] = $ambil->id;
        $F["hum"] = $ambil->hum;
        $F["temp"] = $ambil->temp;
        $F["nh3"] = $ambil->nh3;
        $F["waktu"] = $ambil->waktu;

        array_push($response["data"], $F );
    }
}
else{
    $response["kode"] = 0;
    $response["pesan"] = "Data Tidak Tersedia";
}

echo json_encode($response);
mysqli_close($konek);
