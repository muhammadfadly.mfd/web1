<?php 
$dbhost = 'localhost';
$dbname = 'db_amonia';
$dbuser = 'root';
$dbpass = '';

try{
    $conn = new PDO("mysql:host={$dbhost};dbname={$dbname}",$dbuser, $dbpass);
    $conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
}catch(PDOException $ex){
    die($ex->getMessage());
}
$stmt = $conn->prepare("SELECT * FROM datasensor WHERE waktu >= CURDATE()");
$stmt->execute();
$json=[];
while($row= $stmt->fetch(PDO::FETCH_ASSOC)){
    extract($row);
    $json[]= [(int)$nh3];
}
echo json_encode($json);
?>
