<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/fonts/simple-line-icons/style.min.css">
    <link rel="stylesheet" type="text/css" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <title>Sensor Amoniak</title>
</head>

<body>
    <div class="container mt-2 text-center">
        <h2><strong>DATA SENSOR AMONIA</strong></h2>
    </div>
    <div class="container mt-2">
        <div id="ok2" style="width:100%; height:400px;"></div>
    </div>
    <div class="grey-bg container-fluid">
  <section id="minimal-statistics">
    <?php 
    $koneksi = mysqli_connect('localhost','root','','db_amonia');
    $data_barang = mysqli_query($koneksi,"SELECT * FROM datasensor where DAY(`waktu`) = DAY(CURDATE()) ");
    $harian = mysqli_num_rows($data_barang);
    ?>
    <div class="row mt-3">
      <div class="col-xl-3 col-sm-6 col-12"> 
        <div class="card">
          <div class="card-content bg-primary">
            <div class="card-body">
              <div class="media d-flex">
                <div class="align-self-center">
                  <i class="icon-pencil primary font-large-2 float-left text-white "></i>
                </div>
                <div class="media-body text-white text-right">
                  <h3><?php echo $harian; ?></h3>
                  <span> Hari ini</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php 
    $kon = mysqli_connect('localhost','root','','db_amonia');
    $data_bulan = mysqli_query($kon,"SELECT * FROM datasensor WHERE YEARWEEK(waktu)=YEARWEEK(NOW())");
    $bulan = mysqli_num_rows($data_bulan);
    ?>
      <div class="col-xl-3 col-sm-6 col-12">
        <div class="card">
          <div class="card-content bg-success">
            <div class="card-body">
              <div class="media d-flex">
                <div class="align-self-center">
                  <i class="icon-speech warning font-large-2 float-left text-white"></i>
                </div>
                <div class="media-body text-right text-white">
                  <h3><?php echo $bulan; ?></h3>
                  <span>Data Per Minggu</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php 
    $koneksi = mysqli_connect('localhost','root','','db_amonia');
    $data_tahun = mysqli_query($koneksi,"SELECT * FROM datasensor where MONTH(`waktu`) = MONTH(CURDATE())");
    $tahun = mysqli_num_rows($data_tahun);
    ?>
      <div class="col-xl-3 col-sm-6 col-12">
        <div class="card">
          <div class="card-content bg-danger">
            <div class="card-body">
              <div class="media d-flex">
                <div class="align-self-center">
                  <i class="icon-graph success font-large-2 float-left text-white"></i>
                </div>
                <div class="media-body text-right text-white">
                  <h3><?php echo $tahun ?></h3>
                  <span>Data Perbulan</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
     
  </div>
</section>
</div>
    
    <script type="text/javascript">
        $(document).ready(function() {
            
            var options = {
           
            tooltip: {
                headerFormat: '<b>{Oke}',
            },
                chart: {
                    renderTo: 'ok2',
                    type: 'spline'
                },
                title: {
        text: 'Sensor Amoniak'
    },

                series:[{
                    name: 'Sensor Hari Ini'
                }]
            };
            $.getJSON('data.php', function(data) {
                options.series[0].data = data;
                var chart = new Highcharts.Chart(options);
            });
        });
    </script>
   
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQRFi3dgfSVKpc1B9idTEuN3cBScszNHP9sw5jtS38n2tVEGi2yFNYwhIi4NKxyj" crossorigin="anonymous"></script>


</body>

</html>