<!doctype html>
<html lang="en">
<head>
    <title>Track Aduan</title>
    <link rel="stylesheet" type="text/css" href="asset/bootstrap-4.3.1/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="costum.css">
    <link href="font/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>
     
<section class="form_pppk" id="form_pppk">

  <div class="container">
  <div class="row">
  <div class="col-sm-12 text-center ">
  <h3 class="text-center"> <b> <font color="000080"> </font> </b> </h3>
  <br>
  </div>
  </div>

  <div class="row">
   
    <div class="col-sm-8 offset-sm-2  daftar">
    <div class="scheduleTab">
    <div class="tab-content">
    <div id="day1" class="tab-pane active">
          <div class="head-form">
              <p><b>Track Aduan Anda!</b><p>
          </div>
  <div class="isiform">
  <form method="get" action="track_aduan.php" >

  <div class="form-group">
    <label for="no_tiket">No. Tiket</label>
    <input type="text" class="form-control" id="no_tiket" placeholder="Masukan No. Tiket Anda" name="no_tiket" required="" autocomplete="off">
  </div>

  <div class="form-group">
    <button type="submit" class="btn btn-info">Track</button>
  </div>

  <br>
  <?php
  
  if (isset($_GET['no_tiket'])!="") {
  	
   $notiket= $_GET['no_tiket'];

  ?>

  <table class="table table-bordered table-dark" align="center">
    <thead class="thead-dark">
      <tr>
        <td width="10" align="center">No.</td>
        <td width="150" align="center">No. Tiket</td>
        <td width="150" align="center">No. Aset</td>
        <td width="150" align="center">Status</td>
        <td width="150" align="center">Tanggal</td>
      </tr>
    </thead>
    <tbody>
  <?php
  	require'koneksi.php';

	$sql1 = "SELECT status,no_pppk,no_aset,tb_log_status.tanggal from tb_log_status WHERE no_pppk = '$notiket' and id_status in (SELECT MAX(id_status) from tb_log_status WHERE no_pppk = '$notiket' GROUP by no_aset) GROUP by no_aset";
    $query2 = mysqli_query($conn, $sql1);
    $no   = 1;
    while ( $row = mysqli_fetch_array($query2))
    {
      echo ' <tr class="odd gradeX">
          <td align="center">'.$no.'</td>
          <td align="center">'.$row['no_pppk'].'</td>
          <td align="center">'.$row['no_aset'].'</td>
          <td align="center">'.$row['status'].'</td>
          <td align="center">'.$row['tanggal'].'</td>
          </tr>
      '; 
      $no++;       
    }


    if (mysqli_num_rows($query2) == 0) {
    	echo ' <tr class="odd gradeX">
          <td colspan="5" align="center"> Data Tidak Ditemukan</td>
          </tr>
      ';
    }
  ?>
  </tbody>
</table>

<br>

    <a href="index.php" class="btn btn-success btn-md active" role="button" aria-pressed="true">Selesai</a>

<?php
}else{
	?>	
<!-- <table class="table table-bordered table-dark" align="center">
    <thead class="thead-dark">
      <tr>
        <td width="Auto" align="center">No.</td>
        <td width="150" align="center">No. Tiket</td>
        <td width="150" align="center">No. Aset</td>
        <td width="150" align="center">No. Status</td>
      </tr>
    </thead>
    <tbody>
    <tr>
        <td colspan="4" align="center">Data Tidak Ditemukan</td>
      </tr>
     </tbody>
     </table> 	 -->
<?php
}
?>

    
  </div>
</section>
  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="asset/jQuary-3.3.1/jquery-3.3.1.min.js"></script>
    <script src="asset/bootstrap-4.3.1/js/bootstrap.min.js"></script>

  </body>
</html>
