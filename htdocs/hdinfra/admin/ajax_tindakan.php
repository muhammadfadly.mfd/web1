<?php
require'../koneksi.php';
$idpppk= $_GET['idpppk'];
$aset= $_GET['aset'];
?>
<form method="post" action="proses/proses-tindakan.php">
  <div class="form-group">
    <label for="no_pppk">No. Tiket</label>
    <input required=""  name="no_pppk" type="text" class="form-control input-sm" id="no_pppk" value="<?php echo $idpppk; ?>" readonly>
  </div>
  
  <div class="form-group">
    <label for="no_aset">No. Aset</label>
    <input required=""  name="no_aset" type="text" class="form-control input-sm" id="no_pppk" value="<?php echo $aset; ?>" readonly>
  </div>

  <div class="form-group">
    <label for="Sparepart">Sparepart</label>
    <select class="form-control"  name="sparepart">
      <option value="--">--Pilih Sparepart--</option>
       <?php
        
         $tampil3 = $conn->query("SELECT * FROM tb_sparepart ORDER BY id_sparepart ASC");
         while ($row3 = mysqli_fetch_array($tampil3)){
         ?>             
         <option value="<?php echo $row3['nama_sparepart'];?>"><?php echo $row3['nama_sparepart'];?></option>';
      <?php }?>

      <!-- <option value="LCD">LCD</option>
      <option value="RAM">RAM</option>
      <option value="Hardisk">Hardisk</option> 
 -->
  

    </select>
  </div>

  <div class="form-group">
    <label for="keluhan">Keterangan</label>
    <textarea class="form-control" name="keterangan" rows="3"></textarea>
  </div>

  <div class="form-group">
    <label for="no_aset">Biaya</label>
    <input required="" type="number" name="biaya" class="form-control input-sm" autocomplete="off">
  </div>

  <button type="submit" class="btn btn-success" name="add">Simpan</button>
</form>

<script>
function myFunction() {
  document.getElementById("frm1").submit();
}
</script>