<div class="table-responsive">
  <table border="1" cellspacing="0" style="background-color: #DCDCDC; color: #696969" width="100%">
   <thead style="background-color: #90EE90; height: 30px;">
     <tr>
       <th><center>No. Tiket</center></th>
       <th><center>No. Aset</center></th>
      <th><center>Jenis</center></th>
      <th><center>Keluhan</center></th>
      <th><center>Tindakan</center></th>
      <th><center>Tanggal</center></th>
     </tr>
   </thead>
   <tbody>
    <?php
    include "../../koneksi.php";
    $page = (isset($_POST['page']))? $_POST['page'] : 1;
    $limit = 10; // Jumlah data per halamannya
    $no = (($page - 1) * $limit) + 1; // Untuk setting awal nomor pada halaman yang aktif
    $limit_start = ($page - 1) * $limit;
    if(isset($_POST['search']) && $_POST['search'] == true){ 
      $param = '%'.mysqli_real_escape_string($conn, $keyword).'%';
      $sql = mysqli_query($conn, "SELECT DISTINCT tb_aset.jenis,tb_detail.no_pppk,tb_detail.no_aset,tb_detail.keluhan,tb_master.tanggal,(SELECT GROUP_CONCAT(keterangan) from tb_tindakan where tb_tindakan.no_pppk = tb_detail.no_pppk and tb_tindakan.no_aset = tb_detail.no_aset  GROUP BY tb_detail.no_aset ) as tindakan FROM tb_master JOIN tb_detail on tb_master.no_pppk=tb_detail.no_pppk join tb_tindakan on tb_master.no_pppk=tb_tindakan.no_pppk and tb_detail.no_aset=tb_tindakan.no_aset JOIN tb_aset on tb_aset.no_aset=tb_detail.no_aset WHERE tanggal LIKE '".$param."' OR tb_detail.no_pppk LIKE '".$param."' OR tb_detail.no_aset LIKE '".$param."' OR tb_aset.jenis LIKE '".$param."' OR tb_detail.keluhan LIKE '".$param."' or keterangan LIKE '".$param."' LIMIT ".$limit_start.",".$limit);
 
      $sql2 = mysqli_query($conn, "SELECT DISTINCT COUNT(*) AS jumlah,tb_aset.jenis,tb_detail.no_pppk,tb_detail.no_aset,tb_detail.keluhan,tb_master.tanggal,(SELECT GROUP_CONCAT(keterangan) from tb_tindakan where tb_tindakan.no_pppk = tb_detail.no_pppk and tb_tindakan.no_aset = tb_detail.no_aset GROUP BY tb_detail.no_aset ) as tindakan FROM tb_master JOIN tb_detail on tb_master.no_pppk=tb_detail.no_pppk join tb_tindakan on tb_master.no_pppk=tb_tindakan.no_pppk and tb_detail.no_aset=tb_tindakan.no_aset JOIN tb_aset on tb_aset.no_aset=tb_detail.no_aset WHERE tanggal LIKE '".$param."' OR tb_detail.no_pppk LIKE '".$param."' OR tb_detail.no_aset LIKE '".$param."' OR tb_aset.jenis LIKE '".$param."' OR tb_detail.keluhan LIKE '".$param."'");
      $get_jumlah = mysqli_num_rows($sql2);
    }else{ 
      $sql = mysqli_query($conn, "SELECT DISTINCT tb_aset.jenis,tb_detail.no_pppk,tb_detail.no_aset,tb_detail.keluhan,tb_master.tanggal,(SELECT GROUP_CONCAT(keterangan) from tb_tindakan where tb_tindakan.no_pppk = tb_detail.no_pppk and tb_tindakan.no_aset = tb_detail.no_aset GROUP BY tb_detail.no_aset ) as tindakan FROM tb_master JOIN tb_detail on tb_master.no_pppk=tb_detail.no_pppk join tb_tindakan on tb_master.no_pppk=tb_tindakan.no_pppk and tb_detail.no_aset=tb_tindakan.no_aset JOIN tb_aset on tb_aset.no_aset=tb_detail.no_aset LIMIT ".$limit_start.",".$limit);
      $sql2 = mysqli_query($conn, "SELECT DISTINCT tb_aset.jenis,tb_detail.no_pppk,tb_detail.no_aset,tb_detail.keluhan,tb_master.tanggal,(SELECT GROUP_CONCAT(keterangan) from tb_tindakan where tb_tindakan.no_pppk = tb_detail.no_pppk and tb_tindakan.no_aset = tb_detail.no_aset GROUP BY tb_detail.no_aset ) as tindakan FROM tb_master JOIN tb_detail on tb_master.no_pppk=tb_detail.no_pppk join tb_tindakan on tb_master.no_pppk=tb_tindakan.no_pppk and tb_detail.no_aset=tb_tindakan.no_aset JOIN tb_aset on tb_aset.no_aset=tb_detail.no_aset");
      $get_jumlah = mysqli_num_rows($sql2);
    }
    $i=0;
    while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
      $forpage[$i]=$no;
      ?>
      <tr>

        <td><center><?= $data['no_pppk']; ?></center></td>
        <td><center><?= $data['no_aset']; ?></center></td>
        <td><center><?= $data['jenis']; ?></center></td>
        <td><center><?= $data['keluhan']; ?></center></td>
        <td><center><?= $data['tindakan']; ?></center></td>
        <td><center><?php echo date('d-m-Y H:i:s', strtotime($data['tanggal'])); ?></center></td>                 
      </tr>
      <?php
      $no++;
      $i++;
    }
    ?>
    </tbody>
  </table>
</div>

<?php
$count = mysqli_num_rows($sql);

if($count > 0){ // Jika datanya ada, tampilkan paginationnya
    ?>

    <div class="informasi-data "><p>Menampilkan <?= min($forpage) ?> hingga <?= max($forpage) ?> dari <?= $get_jumlah ?> entri</div>
   
    <ul class="pagination">
      <!-- LINK FIRST AND PREV -->
      <?php
      if($page == 1){ // Jika page adalah page ke 1, maka disable link PREV
      ?>
        <li class="disabled"><a href="#">First</a></li>
        <li class="disabled"><a href="#">&laquo;</a></li>
      <?php
      }else{ // Jika page bukan page ke 1
        $link_prev = ($page > 1)? $page - 1 : 1;
      ?>
        <li><a href="javascript:void(0);" onclick="searchWithPagination(1, false)">First</a></li>
        <li><a href="javascript:void(0);" onclick="searchWithPagination(<?php echo $link_prev; ?>, false)">&laquo;</a></li>
      <?php
      }
      ?>

      <!-- LINK NUMBER -->
      <?php
      $jumlah_page = ceil($get_jumlah / $limit); // Hitung jumlah halamannya
      $jumlah_number = 3; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
      $start_number = ($page > $jumlah_number)? $page - $jumlah_number : 1; // Untuk awal link number
      $end_number = ($page < ($jumlah_page - $jumlah_number))? $page + $jumlah_number : $jumlah_page; // Untuk akhir link number

      for($i = $start_number; $i <= $end_number; $i++){
        $link_active = ($page == $i)? ' class="active"' : '';
      ?>
        <li<?php echo $link_active; ?>><a href="javascript:void(0);" onclick="searchWithPagination(<?php echo $i; ?>, false)"><?php echo $i; ?></a></li>
      <?php
      }
      ?>

      <!-- LINK NEXT AND LAST -->
      <?php
      // Jika page sama dengan jumlah page, maka disable link NEXT nya
      // Artinya page tersebut adalah page terakhir
      if($page == $jumlah_page){ // Jika page terakhir
      ?>
        <li class="disabled"><a href="#">&raquo;</a></li>
        <li class="disabled"><a href="#">Last</a></li>
      <?php
      }else{ // Jika Bukan page terakhir
        $link_next = ($page < $jumlah_page)? $page + 1 : $jumlah_page;
      ?>
        <li><a href="javascript:void(0);" onclick="searchWithPagination(<?php echo $link_next; ?>, false)">&raquo;</a></li>
        <li><a href="javascript:void(0);" onclick="searchWithPagination(<?php echo $jumlah_page; ?>, false)">Last</a></li>
      <?php
      }
      ?>
    </ul>
    <?php
}
?>
