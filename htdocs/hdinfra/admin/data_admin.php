<?php 
require_once ('include/header.php');
?>
<style type="text/css">
  .img-circle, img{
    width: 70px;
    border-radius: 50%;
    border: 4px solid #4682B4;}
  }
</style>


        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  
                  <div class="x_content"> 

                    <form action="proses/pro_user.php" method="POST" class="form-horizontal form-label-left" novalidate>
                    <center><img src="../img/adm_hd.png" class="img-circle"></center>
                    <!-- <div class="img-admin"></div> profile_img -->
                    <center><b><h3>Data Admin</h3></b></center>
                    <hr>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">No. Badge</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="no_badge" class="form-control col-md-7 col-xs-12" name="no_badge"  required="required" type="text" autocomplete="off">
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Username</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="username" class="form-control col-md-7 col-xs-12" name="username"
                          required="required" type="text" autocomplete="off">
                        </div>
                      </div>
                     
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Password</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="password" type="text" name="password" required="required"
                          class="form-control col-md-7 col-xs-12" autocomplete="off">
                        </div>
                      </div>
                       
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3"> 
                        <button id="send" type="submit" class="btn btn-success" name="submit">Simpan</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
           <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  
                  <div class="x_content">
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead style="background-color: #000080; color: #FFFFFF;">
                        <tr>
                          <th><center>No. </center></th>
                          <th><center>No. Badge</center></th>
                          <th><center>Username</center></th>
                          <th><center>Password</center></th>
                          <th><center>Edit</center></th>
                          <th><center>Hapus</center></th>
                        </tr>
                      </thead>
                  
                      <tbody>
                         <?php 
                          include "../koneksi.php";
                              $sql = mysqli_query ($conn, "SELECT * FROM user  ORDER BY id_user ASC");
                              $no=0;      
                              while ($hasil=mysqli_fetch_array($sql)) {
                              $no=$no+1;
                      ?>
                        <tr> 
                          <td><center><?= "$no"; ?></center></td>
                          <td><center><?= $hasil['no_badge']; ?></center></td>
                          <td><center><?= $hasil['username']; ?></center></td>
                          <td><center><?= $hasil['password']; ?></center></td>

                          <td><center><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit_user" data-id="<?php echo $hasil['id_user']; ?>"><i class="fa fa-pencil"></i></button></center></td>



                          <td><center>
                            <a data-href="proses/hapususer.php?idhps=<?php echo $hasil['id_user']; ?>" data-toggle="modal" data-target="#konfirmasi_hapus"><button type="button" class="btn btn-warning btn-circle"><i class="fa fa-trash"></i></button></a>

                            <!-- <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#konfirmasi_hapus" data-id="<?php echo $hasil['id_user']; ?>"><i class="fa fa-trash"></i></button></center> -->
                          </td>

                        </tr> 

                          <?php } ?>
                      </tbody>
                    
                    </table>
          
          
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


<div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <b >Anda yakin ingin menghapus data ini?</b><br><br>
                    <a class="btn btn-danger btn-ok"> Hapus</a>
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
                </div>
            </div>
        </div>
    </div>                
  

<div class="modal fade" id="edit_user" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><b>Edit Data Admin</b></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="fetched-data"></div>
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button> -->
                </div>
            </div>
        </div>
</div>

<script src="asset_admin/vendors/jquery/dist/jquery.min.js"></script>
   <script src="include/ajax.js"></script>
    <script src="asset_admin/build/js/custom.min.js"></script>

        <script>
function searchTable() {
    var input;
    var saring;
    var status; 
    var tbody; 
    var tr; 
    var td;
    var i; 
    var j;
    input = document.getElementById("input");
    saring = input.value.toUpperCase();
    tbody = document.getElementsByTagName("tbody")[0];;
    tr = tbody.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td");
        for (j = 0; j < td.length; j++) {
            if (td[j].innerHTML.toUpperCase().indexOf(saring) > -1) {
                status = true;
            }
        }
        if (status) {
            tr[i].style.display = "";
            status = false;
        } else {
            tr[i].style.display = "none";
        }
    }
}

  $(document).ready(function() {
        $('#konfirmasi_hapus').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });
    });

    $(document).ready(function(){
        $('#edit_user').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            //menggunakan fungsi ajax untuk pengambilan data
             // alert(rowid);
            $.ajax({
                type : 'post',
                // url : 'ajax_keluhan.php',
                url : 'ajax_edit_user.php',
                data :  'rowid='+ rowid,
                success : function(data){
                
                $('.fetched-data').html(data);//menampilkan data ke dalam modal
                }
            });
         });
    });

</script>
        <!-- /page content -->
        
        <!-- footer content -->
<?php
require_once ('include/footer.php');
?>
	
  </body>
</html>