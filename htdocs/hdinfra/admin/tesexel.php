<?php
require_once 'asset_admin/PHPExcel-1.8/Classes/PHPExcel.php';

//create PHPExcel object
$excel = new PHPExcel();

include "../koneksi.php";
$sql = mysqli_query($conn, "SELECT * FROM tb_aset");


$i=1;

// echo $index={"A","B","C","D","E","F","G","H"};

while($data = mysqli_fetch_array($sql)){
//insert some data to PHPExcel object


$excel->setActiveSheetIndex(0)
 ->setCellValue($i,$data['no_aset'])
 ->setCellValue('B1','World');
$i++;
}



//redirect to browser (download) instead of saving the result as a file

//this is for MS Office Excel 2007 xlsx format
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="test.xlsx"');


//this is for MS Office Excel 2003 xls format
//header('Content-Type: application/vnd.ms-excel');
//header('Content-Disposition: attachment; filename="test.xlsx"');


header('Cache-Control: max-age=0');

//write the result to a file
//for excel 2007 format
$file = PHPExcel_IOFactory::createWriter($excel,'Excel2007');

//for excel 2003 format
//$file = PHPExcel_IOFactory::createWriter($excel,'Excel5');

//output to php output instead of filename
$file->save('php://output');

?>