-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 20 Feb 2020 pada 14.08
-- Versi server: 10.1.34-MariaDB
-- Versi PHP: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pppk`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_aset`
--

CREATE TABLE `tb_aset` (
  `no_aset` varchar(50) NOT NULL,
  `serial_number` varchar(30) NOT NULL,
  `spesifikasi` varchar(225) NOT NULL,
  `jenis` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_aset`
--

INSERT INTO `tb_aset` (`no_aset`, `serial_number`, `spesifikasi`, `jenis`) VALUES
('N16214MA40089', 'SES13352877', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N16214MA40090', 'SES13352766', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N21214MA40061', 'SES13352980', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N29214MA40083', 'SES13352774', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N29214MA40084', 'SES13352786', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N29214MA40085', 'SES13352795', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N29214MA40086', 'SES13352800', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N29214MA40087', 'SES13352875', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N29314MA40043', 'SES13352884', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N29314MA40044', 'SES13352885', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N29314MA40045', 'SES13353044', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N29314MA40046', 'SES13352959', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N31314MA43070', '105929', 'Fuji Xerox Docuprint P255dw', 'PCCOMP PRINTER LASER JET A4 MONOCOL'),
('N32414MA43011', 'CN3CJ2JK0Z', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43012', 'CN3CJ2JJZF', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43013', 'CN3CJ2JK0R', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43014', 'CN3CJ2JKR2', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43015', 'CN3CJ2JK06', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43016', 'CN3CJ2JK0W', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43017', 'CN3CJ2JKGW', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43018', 'CN3CJ2JJX9', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43019', 'CN46K2J0K3', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43020', 'CN3C2JK0T', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43021', 'CN3CJ2JK15', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43022', 'CN3CJ2JK0M', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43023', 'CN3CJ2JKQB', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43024', 'CN3CJ2JKN1', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43025', 'CN3CJ2JKRO', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43026', 'CN46K2J0GX', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43027', 'CN46K2J0KD', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43028', 'CN3CJ2JKPP', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43029', 'CN3CJ2JK0G', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43030', 'CN3CJ2K13', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43031', 'CN3CJ2K1S', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43032', 'CN3CJ2K0C', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43033', 'CN3CJ2JJZW', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43034', 'CN3CJ2JK08', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43035', 'CN3CJ2JQN', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43036', 'CN46K2J0GJ', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43037', 'CN46K2J0JB', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43038', 'CN3CJ2JKQT', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43039', 'CN3CJ2K02', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43040', 'CN3CJ2JKNN', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43041', 'CN3CJ2J0GK', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43042', 'CN3CJ2J0GK', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43043', 'CN3CJ2J01G', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43044', 'CN3CJ2JK1M', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43045', 'CN3CJ2JK1P', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43046', 'CN3CJ2JKQG', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43047', 'CN3CJ2J0JZ', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43048', 'CN3CJ2JJZ8', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43049', 'CN3CJ2JKQP', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43050', 'CN3CJ2JK11', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43051', 'CN3CJ2JJX1', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43052', 'CN3CJ2J0JS', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43053', 'CN3CJ2JK0J', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43054', 'CN3CJ2JK1D', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43055', 'CN3CJ2JJZC', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43056', 'CN39B2NH7B', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43057', 'CN3CJ2J0GB', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43058', 'CN3CJ2JJZY', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43059', 'CN46K2J0HZ', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43060', 'CN46K2JJXW', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N32414MA43066', '130114', 'Fuji Xerox DocuPrint P255dw', 'PCCOMP PRINTER LASER JET A4 MONOCOL'),
('N32416MA43001', 'E74706J5H128251', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N32416MA43007', 'E74706J5H128429', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N34414MA43063', '129456', 'Fuji Xerox DocuPrint P255dw', 'PCCOMP PRINTER LASER JET A4 MONOCOL'),
('N34714MA43065', '130151', 'Fuji Xerox DocuPrint P255dw', 'PCCOMP PRINTER LASER JET A4 MONOCOL'),
('N34714MA43071', '109520', 'Fuji Xerox Docuprint P255dw', 'PCCOMP PRINTER LASER JET A4 MONOCOL'),
('N34814MA40097', 'SES13353045', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N34814MA40098', 'SES13352770', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N34814MA40099', 'SES13353038', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N35014MA4023', 'ES12606546', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N35014MA4024', 'ES12616116', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N35014MA43067', '130139', 'Fuji Xerox Docuprint P255dw', 'PCCOMP PRINTER LASER JET A4 MONOCOL'),
('N36614MA43007', 'CN3CJ2JKR7', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N36616MA43004', 'E74706J5H128226', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N36616MA43005', 'E74706J5H128250', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N37216MA43039', 'E74706J5H128347', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N38114MA40038', 'SES13353012', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N38114MA43069', '130134', 'Fuji Xerox Docuprint P255dw', 'PCCOMP PRINTER LASER JET A4 MONOCOL'),
('N3811MA43008', 'CN3CJ2KKY', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N3811MA43009', 'CN46K2J0K7', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N38914MA40088', 'SES13352870', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N40114MA40027', 'ES12613940', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N40114MA40028', 'ES12606548', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N40114MA40029', 'ES12616090', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N41114MA40026', 'ES12606231', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N41114MA40064', 'SES13353139', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41114MA40065', 'SES13353053', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41114MA40066', 'SES13352888', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41114MA40067', 'SES13352773', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41114MA40068', 'SES13352781', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41114MA40069', 'SES13353037', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41114MA40070', 'SES13353046', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41114MA40071', 'SES13352796', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41114MA40072', 'SES13352889', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41114MA40073', 'SES13352867', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41114MA40074', 'SES13352880', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41114MA40075', 'SES13353031', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41114MA40076', 'SES13352868', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41114MA40077', 'SES13352769', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41114MA40078', 'SES13352788', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41116MA43029', 'E74706J5H128240', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N41116MA43030', 'E74706J5H128320', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N41116MA43031', 'E74706J5H128440', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N41116MA43032', 'E74706J5H128253', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N41116MA43033', 'E74706J5H128303', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N41116MA43034', 'E74706J5H128526', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N41116MA43035', 'E74706J5H128559', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N41116MA43036', 'E74706J5H128534', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N41116MA43037', 'E74706J5H128536', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N4114MA4025', 'ES12606165', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N41514MA40047', 'SES13352887', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41514MA40048', 'SES13352784', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41514MA40049', 'SES13352771', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41514MA40050', 'SES13352876', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41514MA40051', 'SES13352872', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41514MA40052', 'SES13352871', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41514MA40053', 'SES13352753', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41514MA40054', 'SES13352886', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41514MA40055', 'SES13352878', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41514MA40056', 'SES13352791', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41514MA40057', 'SES13352792', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41514MA40058', 'SES13353040', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41514MA40059', 'SES13353043', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41514MA40060', 'SES13353130', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N41516MA43009', 'E74706J5H128305', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N41516MA43010', 'E74706J5H128284', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N41516MA43011', 'E74706J5H128437', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N41516MA43012', 'E74706J5H128442', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N41516MA43013', 'E74706J5H128286', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N41516MA43014', 'E74706J5H128462', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N41516MA43015', 'E74706J5H128291', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N41516MA43016', 'E74706J5H128464', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N41516MA43017', 'E74706J5H128512', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N41516MA43018', 'E74706J5H128561', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N41516MA43019', 'E74706J5H128290', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N41516MA43020', 'E74706J5H128541', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N42216MA43021', 'E74706J5H128204', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N42216MA43022', 'E74706J5H128295', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N42216MA43023', 'E74706J5H128298', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N42216MA43024', 'E74706J5H128433', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N42216MA43025', 'E74706J5H128530', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N54014MA40020', 'ES12606573', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N54014MA40021', 'ES12614021', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N54014MA40079', 'SES13352848', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N54014MA40080', 'SES13352869', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N54014MA40081', 'SES13353178', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N54014MA4022', 'ES12606179', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N54014MA43064', '129498', 'Fuji Xerox DocuPrint P255dw', 'PCCOMP PRINTER LASER JET A4 MONOCOL'),
('N54016MA43027', 'E74706J5H128255', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N54016MA43028', 'E74706J5H128217', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N56114MA40001', 'ES12606324', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N56114MA40002', 'ES12359777', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N56116MA43008', 'E74706J5H128301', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N56814MA40003', 'ES12359158', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N56814MA40004', 'ES1212619333', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N56816MA43038', 'E74706J5H128538', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N5801MA43006', 'CN3CJ2JKQ9', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N60214MA40034', 'SES13352954', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N60214MA40035', 'SES13352764', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N60214MA40036', 'SES13352777', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N60214MA40037', 'SES13352779', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N62514MA40019', 'ES12606571', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N62516MA43003', 'E74706J5H128247', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N62516MA43040', 'E74706J5H128302', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N62714MA40093', 'SES13353054', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N62814MA40006', 'ES12606380', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N62814MA40007', 'ES12606526', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N62814MA43062', '129427', 'Fuji Xerox DocuPrint P255dw', 'PCCOMP PRINTER LASER JET A4 MONOCOL'),
('N62914MA40032', 'SES13352873', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N62914MA40033', 'SES13352768', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N62914MA43068', '130135', 'Fuji Xerox Docuprint P255dw', 'PCCOMP PRINTER LASER JET A4 MONOCOL'),
('N63716MA43026', 'E74706J5H128447', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N64514MA3010', 'CN3CJ2JK1D', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N64514MA40012', '358763', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N64514MA40039', 'SES13352775', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N64516MA43006', 'E74706J5H128535', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N65414MA40005', 'ES12358759', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N65414MA40031', 'SES13352879', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N66514MA40008', 'ES12606462', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N66514MA40009', 'ES12606465', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N66514MA40010', 'ES12359198', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N66514MA40011', 'ES12606537', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N68514MA40030', 'ES12606314', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N68514MA40091', 'SES13352787', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N68514MA430731', 'MK4Y105854', 'EPSON LQ 2190', 'PCCOMP PRINTER LQ 2190'),
('N68516MA43002', 'E74706J5H128335', 'BROTHER DCP-T300', 'PCCOMP PRINTER INK TANK A4 COLOUR'),
('N80014MA40092', 'SES13352798', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N80314MA40040', 'SES13352745', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N80314MA40041', 'SES13352782', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N80314MA40042', 'SES13352882', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N80314MA40082', 'SES13352874', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N80314MA40094', 'SES13352955', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N80314MA40095', 'SES13352778', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N80314MA40096', 'SES13353179', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N80314MA43072', 'MK4Y107444', 'EPSON LQ 2190', 'PCCOMP PRINTER LQ 2190'),
('N8114MA43005', 'CN3CJ2JKN8', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N82914MA43001', 'CN3CJ2JKQ8', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N82914MA43002', 'CN3CJ2JK0D', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N82914MA43003', 'CN3CJ2JK00', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N82914MA43004', 'CN3CJ2JKP0', 'HP Deskjet Ink Advantage 2520hc', 'PCCOMP PRINTER INKJET A4 COLOUR'),
('N86014MA40018', 'ES12359292', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N90014MA40013', 'ES12606547', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N90014MA40014', 'ES12606343', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N90014MA40015', 'ES12616095', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N90014MA40016', 'ES12606524', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N90014MA40017', 'ES12358766', 'Lenovo 530s', 'PCCOMP DESKTOP'),
('N90014MA40062', 'SES13352767', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N90014MA40063', 'SES13352776', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP'),
('N90014MA43061', '126110', 'Fuji Xerox DocuPrint P255dw', 'PCCOMP PRINTER LASER JET A4 MONOCOL'),
('N97314MA40100', 'SES13352761', 'Lenovo H530s Proccessor: intel core i3 4150(3.5Ghz); Harddisk: 1TB\'HDD SerialATA\'; Memory: 4GB DDR3; Dimensi: 38.6 x 10.16 x 30.48 Cm; Sound: 5.1 Channel Surround Sound; Optical Drive: DVD R/RW; Berat: 7.9 Kg', 'PCCOMP DESKTOP');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_detail`
--

CREATE TABLE `tb_detail` (
  `id_detail` int(10) NOT NULL,
  `no_pppk` varchar(10) NOT NULL,
  `no_aset` varchar(50) NOT NULL,
  `keluhan` varchar(170) NOT NULL,
  `diagnosa` varchar(250) DEFAULT NULL,
  `diambil` varchar(50) DEFAULT NULL,
  `alasan` varchar(150) DEFAULT NULL,
  `status` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_history_mutasi`
--

CREATE TABLE `tb_history_mutasi` (
  `id_history` int(10) NOT NULL,
  `tanggal` date NOT NULL,
  `no_aset` varchar(50) NOT NULL,
  `unit_kerja` varchar(150) NOT NULL,
  `pengguna` varchar(50) NOT NULL,
  `ext` varchar(14) NOT NULL,
  `ket_mutasi` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_log_status`
--

CREATE TABLE `tb_log_status` (
  `id_status` int(11) NOT NULL,
  `no_pppk` varchar(10) NOT NULL,
  `no_aset` varchar(50) NOT NULL,
  `status` varchar(40) NOT NULL,
  `tanggal` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_master`
--

CREATE TABLE `tb_master` (
  `no_pppk` varchar(10) NOT NULL,
  `tanggal` datetime NOT NULL,
  `tipe_aduan` varchar(70) NOT NULL,
  `unit_kerja` varchar(150) NOT NULL,
  `lokasi` varchar(170) NOT NULL,
  `cost_center` varchar(30) NOT NULL,
  `penghubung` varchar(70) NOT NULL,
  `telepon` varchar(14) NOT NULL,
  `email` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_sparepart`
--

CREATE TABLE `tb_sparepart` (
  `id_sparepart` varchar(30) NOT NULL,
  `nama_sparepart` varchar(170) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_sparepart`
--

INSERT INTO `tb_sparepart` (`id_sparepart`, `nama_sparepart`) VALUES
('S001', 'Jasa Instalasi'),
('S002', 'Jasa Servis'),
('S003', 'Power Supply'),
('S004', 'RAM'),
('S005', 'Motherboard'),
('S006', 'HDD'),
('S007', 'Processor'),
('S008', 'LCD'),
('S009', 'VGA Card'),
('S010', 'Catridge'),
('S011', 'Mainboard Printer'),
('S012', 'Sensor roll'),
('S013', 'Mainboard Monitor'),
('S014', 'Inverter Monitor'),
('S015', 'Kipas (Fan)'),
('S016', 'CD/DVD ROM');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_tindakan`
--

CREATE TABLE `tb_tindakan` (
  `id_tindakan` int(10) NOT NULL,
  `no_pppk` varchar(10) NOT NULL,
  `no_aset` varchar(50) NOT NULL,
  `nama_sparepart` varchar(170) NOT NULL,
  `keterangan` varchar(170) NOT NULL,
  `biaya` decimal(20,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_unitkerja`
--

CREATE TABLE `tb_unitkerja` (
  `kd_unitkerja` int(10) NOT NULL,
  `unit_kerja` varchar(150) NOT NULL,
  `lokasi` varchar(170) NOT NULL,
  `cost_center` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_unitkerja`
--

INSERT INTO `tb_unitkerja` (`kd_unitkerja`, `unit_kerja`, `lokasi`, `cost_center`) VALUES
(1, 'BAG. ADM & PERENC. MATERIAL', ' -', 'F003430000'),
(2, 'BAG. ADM UMUM & KESEKRETARIATAN KPJ', ' -', 'F006230000'),
(3, 'BAG. ADM. PENGADAAN & TURN AROUND', ' -', 'F003410000'),
(4, 'BAG. ADMINISTRASI & SISTEM SEKURITI', ' -', 'F006220000'),
(5, 'BAG. ADMINISTRASI ASET', ' -', 'F006250000'),
(6, 'BAG. ADMINISTRASI KEUANGAN KPJ', ' -', 'F006230000'),
(7, 'BAG. ADMINISTRASI PERSONALIA', ' -', 'F006130000'),
(8, 'BAG. AKUNTANSI BIAYA', ' -', 'F005120000'),
(9, 'BAG. ALAT - ALAT BERAT', ' -', 'F002230000'),
(10, 'BAG. AMMONIA P-IB', ' -', 'F002112000'),
(11, 'BAG. AMMONIA P-IIB', ' -', 'F002152000'),
(12, 'BAG. AMMONIA P-III', ' -', 'F002132000'),
(13, 'BAG. AMMONIA P-IV', ' -', 'F002142000'),
(14, 'BAG. BENGKEL MESIN', ' -', 'F002230000'),
(15, 'BAG. EVALUASI TEKNIS', ' -', 'F003430000'),
(16, 'BAG. FABRIKASI & PERBAIKAN PERALATAN', ' -', 'F002230000'),
(17, 'BAG. HIPERKES', ' -', 'F002330000'),
(18, 'BAG. HUB. INDUSTRIAL KETENAGAKERJAAN', ' -', 'F006130000'),
(19, 'BAG. INFORMASI & KOMUNIKASI', ' -', 'F001220000'),
(20, 'BAG. INVESTIGASI', ' -', 'F006220000'),
(21, 'BAG. KARIR & KONSELING', ' -', 'F006110000'),
(22, 'BAG. KASSA & BANK DAN ASURANSI', ' -', 'F005110000'),
(23, 'BAG. KNOWLEDGE MANAGEMENT', ' -', 'F006120000'),
(24, 'BAG. KONTRAK JASA & ADMINISTRASI', ' -', 'F003420000'),
(25, 'BAG. LAB KIMIA ANALISIS', ' -', 'F002320000'),
(26, 'BAG. LAB PENUNJANG SARANA', ' -', 'F002320000'),
(27, 'BAG. MANAJEMEN RISIKO', ' -', 'F001210000'),
(28, 'BAG. MEKANIKAL NPK & STG', ' -', 'F002210000'),
(29, 'BAG. MEKANIKAL P-IB', ' -', 'F002210000'),
(30, 'BAG. MEKANIKAL P-IIB', ' -', 'F002210000'),
(31, 'BAG. MEKANIKAL P-III', ' -', 'F002210000'),
(32, 'BAG. MEKANIKAL P-IV', ' -', 'F002210000'),
(33, 'BAG. MEKANIKAL PPU', ' -', 'F002210000'),
(34, 'BAG. OPERASI PABRIK NPK-1 & PUPUK HAYATI', ' -', 'F002181000'),
(35, 'BAG. OPERASI SEKURITI', ' -', 'F006220000'),
(36, 'BAG. PAJAK & PENAGIHAN', ' -', 'F005110000'),
(37, 'BAG. PELAKSANAAN DIKLAT', ' -', 'F006120000'),
(38, 'BAG. PEMBELIAN LUAR NEGERI & BAHAN BAKU', ' -', 'F003410000'),
(39, 'BAG. PEMBELIAN MATERIAL KHUSUS', ' -', 'F003410000'),
(40, 'BAG. PEMBELIAN MATERIAL TEKNIK', ' -', 'F003410000'),
(41, 'BAG. PEMBELIAN MATERIAL UMUM&INVESTASI', ' -', 'F003410000'),
(42, 'BAG. PEMEL KOM & KOMUNIKASI DATA', ' -', 'F003290000'),
(43, 'BAG. PEMUATAN PUPUK & DERMAGA KHUSUS', ' -', 'F004246000'),
(44, 'BAG. PENERIMAAN & PENEMPATAN SDM', ' -', 'F006110000'),
(45, 'BAG. PENGADAAN JASA I', ' -', 'F003420000'),
(46, 'BAG. PENGADAAN JASA II', ' -', 'F003420000'),
(47, 'BAG. PENGANGKUTAN', ' -', 'F004240000'),
(48, 'BAG. PENGAWASAN EVALUASI', ' -', 'F002330000'),
(49, 'BAG. PENGELASAN LAPANGAN', ' -', 'F002230000'),
(50, 'BAG. PENGELOLAAN JASA TEKNIK', ' -', 'F003200000'),
(51, 'BAG. PENGEMB. SISTEM INF KTR PUSAT&PROD', ' -', 'F003290000'),
(52, 'BAG. PENGEMBANGAN ORGANISASI', ' -', 'F006110000'),
(53, 'BAG. PENGENDALIAN PENCEMARAN', ' -', 'F002330000'),
(54, 'BAG. PENGOPERASIAN SISTEM KOMPUTER', ' -', 'F003290000'),
(55, 'BAG. PENUNJANG TEKNIK', ' -', 'F003220000'),
(56, 'BAG. PENYUSUNAN ANGGARAN', ' -', 'F005130000'),
(57, 'BAG. PERENC.KESEJAHTERAAN&PASCA KERJA', ' -', 'F006130000'),
(58, 'BAG. PERENC.REMUNERASI & PENGGAJIAN', ' -', 'F006130000'),
(59, 'BAG. PERENCANAAN DIKLAT', ' -', 'F006120000'),
(60, 'BAG. PERENCANAAN RUTIN', ' -', 'F002240000'),
(61, 'BAG. PERGUDANGAN', ' -', 'F003430000'),
(62, 'BAG. PFSO-ISPS CODE', ' -', 'F006220000'),
(63, 'BAG. PK & KK', ' -', 'F002330000'),
(64, 'BAG. PLOLAAN DANA & ANALISIS KEU&INV', ' -', 'F005110000'),
(65, 'BAG. PROTOKOL', ' -', 'F001220000'),
(66, 'BAG. RENDAL & MANAJEMEN PROYEK', ' -', 'F003220000'),
(67, 'BAG. SARANA TEKNIK', ' -', 'F002240000'),
(68, 'BAG. SIPIL & PENGERUKAN', ' -', 'F002230000'),
(69, 'BAG. STG DAN BOILER BATUBARA', ' -', 'F002164000'),
(70, 'BAG. TURN AROUND & MANAGEMENT SYSTEM', ' -', 'F002240000'),
(71, 'BAG. UREA P-IB', ' -', 'F002113000'),
(72, 'BAG. UREA P-IIB', ' -', 'F002153000'),
(73, 'BAG. UREA P-III', ' -', 'F002133000'),
(74, 'BAG. UREA P-IV', ' -', 'F002143000'),
(75, 'BAG. UTILISASI ASET', ' -', 'F006250000'),
(76, 'BAG. UTILITAS P-IB', ' -', 'F002111000'),
(77, 'BAG. UTILITAS P-IIB', ' -', 'F002151000'),
(78, 'BAG. UTILITAS P-III', ' -', 'F002131000'),
(79, 'BAG. UTILITAS P-IV', ' -', 'F002141000'),
(80, 'BAG. VERIFIKASI', ' -', 'F005120000'),
(81, 'BAG.DAAN JASA SAR DISTRIBUSI & PEMASARAN', ' -', 'F003420000'),
(82, 'BAG.PENGAWASAN ANGGARAN & PELAPORAN MAN.', ' -', 'F005130000'),
(83, 'BAG.PENGEMB.S.INF PMASARAN&UNIT PNUNJANG', ' -', 'F003290000'),
(84, 'BAGIAN ADMINISTRASI & KEUANGAN', ' -', 'F001240000'),
(85, 'BAGIAN ADMINISTRASI UMUM', ' -', 'F006210000'),
(86, 'BAGIAN AKUNTANSI UMUM', ' -', 'F005120000'),
(87, 'BAGIAN BENGKEL LISTRIK & INSTRUMEN', ' -', 'F002220000'),
(88, 'BAGIAN CSR', ' -', 'F001240000'),
(89, 'BAGIAN HANDLING BATU BARA', ' -', 'F002170000'),
(90, 'BAGIAN INSTRUMEN I', ' -', 'F002260000'),
(91, 'BAGIAN INSTRUMEN II', ' -', 'F002260000'),
(92, 'BAGIAN INSTRUMEN III', ' -', 'F002260000'),
(93, 'BAGIAN KEMITRAAN', ' -', 'F001240000'),
(94, 'BAGIAN LAB KONTROL', ' -', 'F002320000'),
(95, 'BAGIAN LISTRIK I', ' -', 'F002220000'),
(96, 'BAGIAN LISTRIK II', ' -', 'F002220000'),
(97, 'BAGIAN LISTRIK III', ' -', 'F002220000'),
(98, 'BAGIAN OPERASI STG & BOILER BATU BARA', ' -', 'F002172000'),
(99, 'BAGIAN PELAYANAN UMUM', ' -', 'F006210000'),
(100, 'BAGIAN PEMULIAAN TANAMAN & PANGAN', ' -', 'F003320000'),
(101, 'BAGIAN PENGEMB. PUPUK & PRODUK HAYATI', ' -', 'F003320000'),
(102, 'BAGIAN PENGEMBANGAN PASAR&PRE-MARKETING', ' -', 'F003350000'),
(103, 'BAGIAN PENGEMBANGAN PRODUK & BISNISANPER', ' -', 'F003350000'),
(104, 'BAGIAN PENGEMBANGAN SISTEM MANAJEMEN', ' -', 'F001210000'),
(105, 'BAGIAN PENGEMBANGAN TEKNOLOGI', ' -', 'F003310000'),
(106, 'BAGIAN PERENCANAAN & PEMELIHARAAN UMUM', ' -', 'F006210000'),
(107, 'BAGIAN PERENCANAAN KORPORASI', ' -', 'F003310000'),
(108, 'BAGIAN PPU-1', ' -', 'F002161000'),
(109, 'BAGIAN TATA KELOLA PERUSAHAAN', ' -', 'F001210000'),
(110, 'BID. INFRASTRUKTUR & PENGEMB. SI', ' -', 'F003200000'),
(111, 'BID. MARKETING & KEUANGAN', ' -', 'F003200000'),
(112, 'BID. TRANSFORMASI BISNIS DIGITAL', ' -', 'F003200000'),
(113, 'DEP. AKUNTANSI', ' -', 'F005120000'),
(114, 'DEP. ANGGARAN', ' -', 'F005130000'),
(115, 'DEP. BANG USAHA & TEKNOLOGI', ' -', 'F003310000'),
(116, 'DEP. CORPORATE SOCIAL RESPONSIBILITY', ' -', 'F001240000'),
(117, 'DEP. INSPEKSI TEKNIK', ' -', 'F002340000'),
(118, 'DEP. INSTRUMEN', ' -', 'F002260000'),
(119, 'DEP. KETENAGAKERJAAN', ' -', 'F006130000'),
(120, 'DEP. KEUANGAN', ' -', 'F005110000'),
(121, 'DEP. LISTRIK', ' -', 'F002220000'),
(122, 'DEP. LOGISTIK PEMASARAN', ' -', 'F004240000'),
(123, 'DEP. MEKANIKAL', ' -', 'F002210000'),
(124, 'DEP. OPERASI & PENGANTONGAN', ' -', 'F002160000'),
(125, 'DEP. OPERASI P-IB', ' -', 'F002110000'),
(126, 'DEP. OPERASI P-IIB', ' -', 'F002150000'),
(127, 'DEP. OPERASI P-III', ' -', 'F002130000'),
(128, 'DEP. OPERASI P-IV', ' -', 'F002140000'),
(129, 'DEP. OPERASI P-V', ' -', 'F002180000'),
(130, 'DEP. OPERASI P-VI', ' -', 'F002160000'),
(131, 'DEP. PENDIDIKAN & PELATIHAN', ' -', 'F006120000'),
(132, 'DEP. PENGADAAN BARANG', ' -', 'F003410000'),
(133, 'DEP. PENGADAAN JASA', ' -', 'F003420000'),
(134, 'DEP. PENGELOLAAN ASET', ' -', 'F006250000'),
(135, 'DEP. PENGEMB. PRODUK & PASAR', ' -', 'F003350000'),
(136, 'DEP. PENJUALAN KOMERSIL WILAYAH I', ' -', 'F004310000'),
(137, 'DEP. PENJUALAN KOMERSIL WILAYAH II', ' -', 'F004330000'),
(138, 'DEP. PENJUALAN KOMERSIL WILAYAH III', ' -', 'F004320000'),
(139, 'DEP. PENJUALAN PSO WILAYAH I', ' -', 'F004110000'),
(140, 'DEP. PENJUALAN PSO WILAYAH II', ' -', 'F004120000'),
(141, 'DEP. PERENC & PENGENDALIAN PEMASARAN', ' -', 'F004220000'),
(142, 'DEP. PERENC. MATERIAL & PERGUDANGAN', ' -', 'F003430000'),
(143, 'DEP. PERENCANAAN & PENGENDALIAN PRODUKSI', ' -', 'F002310000'),
(144, 'DEP. RANCANG BANGUN & PEREKAYASAAN', ' -', 'F003220000'),
(145, 'DEP. RELIABILITY', ' -', 'F002250000'),
(146, 'DEP. SARANA & UMUM', ' -', 'F006210000'),
(147, 'DEP. SEKURITI', ' -', 'F006220000'),
(148, 'DEP. STG & BOILER BATU BARA', ' -', 'F002170000'),
(149, 'DEP. TEKNOLOGI INFORMASI', ' -', 'F003290000'),
(150, 'DEP.TATA KELOLA & MANAJEMEN RISIKO', ' -', 'F001210000'),
(151, 'DEPARTEMEN COMMISIONING & START-UP', ' -', 'F007020026'),
(152, 'DEPARTEMEN ENGINEERING', ' -', 'F007020026'),
(153, 'DEPARTEMEN HUKUM', ' -', 'F001230000'),
(154, 'DEPARTEMEN K3 & LH', ' -', 'F002330000'),
(155, 'DEPARTEMEN LABORATORIUM', ' -', 'F002320000'),
(156, 'DEPARTEMEN PENGAWASAN KEUANGAN', ' -', 'F001110000'),
(157, 'DEPARTEMEN PENGAWASAN OPERASIONAL', ' -', 'F001120000'),
(158, 'DEPARTEMEN PROJECT CONTROL & ADM', ' -', 'F007020026'),
(159, 'DEPARTEMEN RISET', ' -', 'F003320000'),
(160, 'DIVISI  PENGADAAN', ' -', 'F003400000'),
(161, 'DIVISI ADMINISTRASI KEUANGAN', ' -', 'F005100000'),
(162, 'DIVISI DISTRIBUSI & PEMASARAN', ' -', 'F004200000'),
(163, 'DIVISI OPERASI', ' -', 'F002100000'),
(164, 'DIVISI PEMELIHARAAN', ' -', 'F002200000'),
(165, 'DIVISI PENJUALAN PRODUK KOMERSIL', ' -', 'F004300000'),
(166, 'DIVISI PENJUALAN PRODUK PSO', ' -', 'F004100000'),
(167, 'DIVISI RISET & PENGEMBANGAN', ' -', 'F003300000'),
(168, 'DIVISI SATUAN PENGAWAS INTERN', ' -', 'F001100000'),
(169, 'DIVISI SDM', ' -', 'F006100000'),
(170, 'DIVISI SEKRE. PERUSAHAAN & TATA KELOLA', ' -', 'F001200000'),
(171, 'DIVISI TEKNIK & SISTEM INFORMASI', ' -', 'F003200000'),
(172, 'DIVISI TEKNOLOGI', ' -', 'F002300000'),
(173, 'KANTOR PERWAKILAN JAKARTA', ' -', 'F006230000'),
(174, 'KEL. ADM & PEMESANAN PENGADAAN', ' -', 'F003430000'),
(175, 'KEL. AUDITOR WAS OPERASIONAL', ' -', 'F001120000'),
(176, 'KEL. BID. INSP T.BENGKEL P & A\"', ' -', 'F002340000'),
(177, 'KEL. BID. INSP.TEKNIK STG.B.BARA.NPK.JET\"', ' -', 'F002340000'),
(178, 'KEL. BID. INSPEKSI TEKNIK P-IB', ' -', 'F002340000'),
(179, 'KEL. BID. INSPEKSI TEKNIK P-II/P-IIB', ' -', 'F002340000'),
(180, 'KEL. BID. INSPEKSI TEKNIK P-III', ' -', 'F002340000'),
(181, 'KEL. BID. INSPEKSI TEKNIK P-IV', ' -', 'F002340000'),
(182, 'KEL. BID. LAB. INSPEKSI TEKNIK', ' -', 'F002340000'),
(183, 'KEL. BID. PENGENDALIAN MATERIAL PRODUKSI', ' -', 'F002310000'),
(184, 'KEL. BID. PERENC & PELAPORAN PRODUKSI', ' -', 'F002310000'),
(185, 'KEL. BID. QUALITY CONTROL', ' -', 'F002340000'),
(186, 'KEL. BIDANG OPERASIONAL', ' -', 'F004230000'),
(187, 'KEL. BIDANG SOSIALISASI &K AW.TEKNOLOGI', ' -', 'F004230000'),
(188, 'KEL. CIVIL & ARCHITECTURE ENGINEERING', ' -', 'F003220000'),
(189, 'KEL. ELECTRICAL ENGINEERING', ' -', 'F003220000'),
(190, 'KEL. ELECTRONIC INSTRUMENT ENGINEERING', ' -', 'F003220000'),
(191, 'KEL. GP3K DAERAH DIY', ' -', 'F004230000'),
(192, 'KEL. GP3K DAERAH JATENG', ' -', 'F004230000'),
(193, 'KEL. GP3K DAERAH LAMPUNG', ' -', 'F004230000'),
(194, 'KEL. INSPEKSI TEKNIK I', ' -', 'F002340000'),
(195, 'KEL. INSPEKSI TEKNIK II', ' -', 'F002340000'),
(196, 'KEL. INSPEKSI TEKNIK KHUSUS', ' -', 'F002340000'),
(197, 'KEL. KATALOGING ANALISIS & EVAL PERSEDIAAN', ' -', 'F003430000'),
(198, 'KEL. PENERIMAAN BARANG', ' -', 'F003430000'),
(199, 'KEL. PERENC.& PENGEND. MATERIAL', ' -', 'F003430000'),
(200, 'KEL. PERENC. EVALUASI & PELAPORAN', ' -', 'F004220000'),
(201, 'KEL. PERENC.PENGADAAN & PERKIRAAN HARGA', ' -', 'F003430000'),
(202, 'KEL. PIPING ENGINEERING', ' -', 'F003220000'),
(203, 'KEL. PLANNER / SCHEDULER INSTRUMEN', ' -', 'F002240000'),
(204, 'KEL. PLANNER / SCHEDULER LISTRIK', ' -', 'F002240000'),
(205, 'KEL. PLANNER / SCHEDULER MEKANIKAL', ' -', 'F002240000'),
(206, 'KEL. PLANNER / SCHEDULER PERBENGKELAN', ' -', 'F002240000'),
(207, 'KEL. PROCESS & SYSTEM ENGINEERING', ' -', 'F003220000'),
(208, 'KEL. ROTATING MACHINERY ENGINEERING', ' -', 'F003220000'),
(209, 'KEL. STAF DIR SDM & UMUM', ' -', 'F006900000'),
(210, 'KEL. STAF DIR SDM & UMUM', ' -', 'F006910000'),
(211, 'KEL. STAF DIR. KOMERSIL', ' -', 'F004900000'),
(212, 'KEL. STAF DIR. PRODUKSI', ' -', 'F002900000'),
(213, 'KEL. STATIC EQUIPMENT ENGINEERING', ' -', 'F003220000'),
(214, 'KEL. TEKNIK KESELAMATAN & LINGKUNGAN', ' -', 'F002330000'),
(215, 'KEL.ANALISIS PROD. PROMOSI & P.PASAR', ' -', 'F004220000'),
(216, 'KEL.AUDITOR WAS KEUANGAN', ' -', 'F001110000'),
(217, 'KEL.PERENC.& PENGEND.DISTRIBUSI', ' -', 'F004220000'),
(218, 'KELOMPOK BIDANG TEKNIK PROSES I', ' -', 'F002310000'),
(219, 'KELOMPOK BIDANG TEKNIK PROSES II', ' -', 'F002310000'),
(220, 'KELOMPOK GUDANG PENYIMPANAN PUPUK', ' -', 'F004110000'),
(221, 'KELOMPOK GUDANG PENYIMPANAN PUPUK', ' -', 'F004120000'),
(222, 'KELOMPOK GUDANG PENYIMPANAN PUPUK', ' -', 'F004330000'),
(223, 'KELOMPOK PERENC. & EVALUASI LOGSAR', ' -', 'F004240000'),
(224, 'KELOMPOK STAF DIR. TEK & BANG', ' -', 'F003900000'),
(225, 'KOMITE GP3K', ' -', 'F004230000'),
(226, 'MANAGEMEN KEUANGAN PROYEK', ' -', 'F005140000'),
(227, 'PENJUALAN KOMERSIL ACEH & SUMUT', ' -', 'F004331000'),
(228, 'PENJUALAN KOMERSIL JATIM', ' -', 'F004311700'),
(229, 'PENJUALAN KOMERSIL KALSELTENG&IND.TIMUR', ' -', 'F004335000'),
(230, 'PENJUALAN KOMERSIL RIAU & KEPRI', ' -', 'F004332000'),
(231, 'PENJUALAN KOMERSIL SUMBAR', ' -', 'F004333000'),
(232, 'PENJUALAN KOMERSIL SUMSEL', ' -', 'F004318000'),
(233, 'PENJUALAN PSO BABEL', ' -', 'F004115000'),
(234, 'PENJUALAN PSO BENGKULU', ' -', 'F004114000'),
(235, 'PENJUALAN PSO DIY & JATENG 3', ' -', 'F004129000'),
(236, 'PENJUALAN PSO JAMBI', ' -', 'F004111000'),
(237, 'PENJUALAN PSO JATENG 1', ' -', 'F004127000'),
(238, 'PENJUALAN PSO JATENG 2', ' -', 'F004121100'),
(239, 'PENJUALAN PSO KALBAR', ' -', 'F004121200'),
(240, 'PENJUALAN PSO LAMPUNG', ' -', 'F004113000'),
(241, 'PENJUALAN PSO SUMSEL', ' -', 'F004112000'),
(242, 'PROYEK NPK FUSION II & REV. PUSRI III/IV', ' -', 'F007020026'),
(243, 'PROYEK PEMB. NPK FUS-II & REV. P-III/IV', ' -', 'F007020026'),
(244, 'PWK.UNIT PENGANTONGAN PUPUK SEMARANG', ' -', 'F004245000'),
(245, 'REGU ALAT BERAT', ' -', 'F002230000'),
(246, 'REGU AMMONIA P-IB GR.A', ' -', 'F002112000'),
(247, 'REGU AMMONIA P-IB GR.B', ' -', 'F002112000'),
(248, 'REGU AMMONIA P-IB GR.C', ' -', 'F002112000'),
(249, 'REGU AMMONIA P-IB GR.D', ' -', 'F002112000'),
(250, 'REGU AMMONIA P-IIB GR.A', ' -', 'F002152000'),
(251, 'REGU AMMONIA P-IIB GR.B', ' -', 'F002152000'),
(252, 'REGU AMMONIA P-IIB GR.C', ' -', 'F002152000'),
(253, 'REGU AMMONIA P-IIB GR.D', ' -', 'F002152000'),
(254, 'REGU AMMONIA P-III GR.A', ' -', 'F002132000'),
(255, 'REGU AMMONIA P-III GR.B', ' -', 'F002132000'),
(256, 'REGU AMMONIA P-III GR.C', ' -', 'F002132000'),
(257, 'REGU AMMONIA P-III GR.D', ' -', 'F002132000'),
(258, 'REGU AMMONIA P-IV GR.A', ' -', 'F002142000'),
(259, 'REGU AMMONIA P-IV GR.B', ' -', 'F002142000'),
(260, 'REGU AMMONIA P-IV GR.C', ' -', 'F002142000'),
(261, 'REGU AMMONIA P-IV GR.D', ' -', 'F002142000'),
(262, 'REGU BENGKEL ALAT BERAT & ALAT BANTUAN', ' -', 'F002230000'),
(263, 'REGU BENGKEL INSTRUMEN & ELEKTRONIKA', ' -', 'F002220000'),
(264, 'REGU BENGKEL LISTRIK & PERALATAN PENDINGIN', ' -', 'F002220000'),
(265, 'REGU BENGKEL PIPA & LAS LAPANGAN-I', ' -', 'F002230000'),
(266, 'REGU BENGKEL PIPA & LAS LAPANGAN-II', ' -', 'F002230000'),
(267, 'REGU BENGKEL PIPA & LAS LAPANGAN-III', ' -', 'F002230000'),
(268, 'REGU BENGKEL REPARASI KHUSUS', ' -', 'F002230000'),
(269, 'REGU BENGKEL REPARASI MESIN', ' -', 'F002230000'),
(270, 'REGU BENGKEL REPARASI UMUM', ' -', 'F002230000'),
(271, 'REGU DERMAGA KHUSUS', ' -', 'F004246000'),
(272, 'REGU FABRIKASI PERALATAN & PROYEK', ' -', 'F002230000'),
(273, 'REGU INSTRUMEN NPK. STG. & JETTY', ' -', 'F002260000'),
(274, 'REGU INSTRUMEN P-IB', ' -', 'F002260000'),
(275, 'REGU INSTRUMEN P-IIB', ' -', 'F002260000'),
(276, 'REGU INSTRUMEN P-III', ' -', 'F002260000'),
(277, 'REGU INSTRUMEN P-IV', ' -', 'F002260000'),
(278, 'REGU INSTRUMEN PPU. UBS. & NPK', ' -', 'F002260000'),
(279, 'REGU INSTRUMEN PPU. UBS', ' -', 'F002260000'),
(280, 'REGU INSTRUMEN STG & JETTY', ' -', 'F002260000'),
(281, 'REGU ISOLASI', ' -', 'F002230000'),
(282, 'REGU LAB CHEM.CLEANING & BENFIELD', ' -', 'F002320000'),
(283, 'REGU LAB INVENTORY & SISTEM MUTU', ' -', 'F002320000'),
(284, 'REGU LAB KALIBRASI & PEMELIHARAAN', ' -', 'F002320000'),
(285, 'REGU LAB KONTROL P-IB GR.A', ' -', 'F002320000'),
(286, 'REGU LAB KONTROL P-IB GR.B', ' -', 'F002320000'),
(287, 'REGU LAB KONTROL P-IB GR.C', ' -', 'F002320000'),
(288, 'REGU LAB KONTROL P-IB GR.D', ' -', 'F002320000'),
(289, 'REGU LAB KONTROL P-II/PII-B/STG GR.A', ' -', 'F002320000'),
(290, 'REGU LAB KONTROL P-II/PII-B/STG GR.B', ' -', 'F002320000'),
(291, 'REGU LAB KONTROL P-II/PII-B/STG GR.C', ' -', 'F002320000'),
(292, 'REGU LAB KONTROL P-II/PII-B/STG GR.D', ' -', 'F002320000'),
(293, 'REGU LAB KONTROL P-III/UTIL P-II GR.A', ' -', 'F002320000'),
(294, 'REGU LAB KONTROL P-III/UTIL P-II GR.B', ' -', 'F002320000'),
(295, 'REGU LAB KONTROL P-III/UTIL P-II GR.C', ' -', 'F002320000'),
(296, 'REGU LAB KONTROL P-III/UTIL P-II GR.D', ' -', 'F002320000'),
(297, 'REGU LAB KONTROL P-IV GR.A', ' -', 'F002320000'),
(298, 'REGU LAB KONTROL P-IV GR.B', ' -', 'F002320000'),
(299, 'REGU LAB KONTROL P-IV GR.C', ' -', 'F002320000'),
(300, 'REGU LAB KONTROL P-IV GR.D', ' -', 'F002320000'),
(301, 'REGU LAB PENGUJIAN GAS & LUBE OIL', ' -', 'F002320000'),
(302, 'REGU LAB PENGUJIAN NPK. STG. BATU BARA', ' -', 'F002320000'),
(303, 'REGU LAB PENGUJIAN PRODUK', ' -', 'F002320000'),
(304, 'REGU LAB PENGUJIAN UMUM', ' -', 'F002320000'),
(305, 'REGU LAB PENGUJIAN UMUM (QC AIR)', ' -', 'F002320000'),
(306, 'REGU LISTRIK JARINGAN', ' -', 'F002220000'),
(307, 'REGU LISTRIK P-IB', ' -', 'F002220000'),
(308, 'REGU LISTRIK P-IIB', ' -', 'F002220000'),
(309, 'REGU LISTRIK P-III', ' -', 'F002220000'),
(310, 'REGU LISTRIK P-IV', ' -', 'F002220000'),
(311, 'REGU LISTRIK PPU I', ' -', 'F002220000'),
(312, 'REGU LISTRIK PPU II (UBS. 2D. & NPK)\"', ' -', 'F002220000'),
(313, 'REGU LISTRIK STG & JETTY', ' -', 'F002220000'),
(314, 'REGU MEKANIKAL AMMONIA P-IB', ' -', 'F002210000'),
(315, 'REGU MEKANIKAL AMMONIA P-IIB', ' -', 'F002210000'),
(316, 'REGU MEKANIKAL AMMONIA P-III', ' -', 'F002210000'),
(317, 'REGU MEKANIKAL AMMONIA P-IV', ' -', 'F002210000'),
(318, 'REGU MEKANIKAL PPU I', ' -', 'F002210000'),
(319, 'REGU MEKANIKAL PPU II', ' -', 'F002210000'),
(320, 'REGU MEKANIKAL SHIFT P-IB', ' -', 'F002210000'),
(321, 'REGU MEKANIKAL SHIFT P-IIB', ' -', 'F002210000'),
(322, 'REGU MEKANIKAL SHIFT P-III', ' -', 'F002210000'),
(323, 'REGU MEKANIKAL SHIFT P-IV', ' -', 'F002210000'),
(324, 'REGU MEKANIKAL SHIFT PPU', ' -', 'F002210000'),
(325, 'REGU MEKANIKAL UBS & PRODUK SAMPING', ' -', 'F002210000'),
(326, 'REGU MEKANIKAL UREA P-IB', ' -', 'F002210000'),
(327, 'REGU MEKANIKAL UREA P-IIB', ' -', 'F002210000'),
(328, 'REGU MEKANIKAL UREA P-III', ' -', 'F002210000'),
(329, 'REGU MEKANIKAL UREA P-IV', ' -', 'F002210000'),
(330, 'REGU MEKANIKAL UTILITAS P-IB', ' -', 'F002210000'),
(331, 'REGU MEKANIKAL UTILITAS P-II', ' -', 'F002210000'),
(332, 'REGU MEKANIKAL UTILITAS P-IIB', ' -', 'F002210000'),
(333, 'REGU MEKANIKAL UTILITAS P-III', ' -', 'F002210000'),
(334, 'REGU MEKANIKAL UTILITAS P-IV', ' -', 'F002210000'),
(335, 'REGU OPERASI SEKURITI', ' -', 'F006220000'),
(336, 'REGU PEKERJAAN SIPIL', ' -', 'F002230000'),
(337, 'REGU PEMUATAN PUPUK', ' -', 'F004246000'),
(338, 'REGU PENERIMAAN & PENGELUARAN GR.A', ' -', 'F004246000'),
(339, 'REGU PENERIMAAN & PENGELUARAN GR.B', ' -', 'F004246000'),
(340, 'REGU PENERIMAAN & PENGELUARAN GR.C', ' -', 'F004246000'),
(341, 'REGU PENERIMAAN & PENGELUARAN GR.D', ' -', 'F004246000'),
(342, 'REGU PENGAWASAN BONGKAR MUAT GR.A', ' -', 'F004246000'),
(343, 'REGU PENGAWASAN BONGKAR MUAT GR.B', ' -', 'F004246000'),
(344, 'REGU PENGAWASAN BONGKAR MUAT GR.C', ' -', 'F004246000'),
(345, 'REGU PENGAWASAN BONGKAR MUAT GR.D', ' -', 'F004246000'),
(346, 'REGU PENGERUKAN', ' -', 'F002230000'),
(347, 'REGU PERBAIKAN PERALATAN', ' -', 'F002230000'),
(348, 'REGU PK & KK GR.A', ' -', 'F002330000'),
(349, 'REGU PK & KK GR.B', ' -', 'F002330000'),
(350, 'REGU PK & KK GR.C', ' -', 'F002330000'),
(351, 'REGU PK & KK GR.D', ' -', 'F002330000'),
(352, 'REGU SHIFT BENGKEL LISTRIK INSTRUMEN', ' -', 'F002220000'),
(353, 'REGU SHIFT INSTRUMEN I', ' -', 'F002260000'),
(354, 'REGU SHIFT INSTRUMEN II', ' -', 'F002260000'),
(355, 'REGU SHIFT INSTRUMEN III', ' -', 'F002260000'),
(356, 'REGU SHIFT LISTRIK I', ' -', 'F002220000'),
(357, 'REGU SHIFT LISTRIK II', ' -', 'F002220000'),
(358, 'REGU SHIFT LISTRIK III', ' -', 'F002220000'),
(359, 'REGU STG & BOILER BATU BARA GR.A', ' -', 'F002172000'),
(360, 'REGU STG & BOILER BATU BARA GR.B', ' -', 'F002172000'),
(361, 'REGU STG & BOILER BATU BARA GR.C', ' -', 'F002172000'),
(362, 'REGU STG & BOILER BATU BARA GR.D', ' -', 'F002172000'),
(363, 'REGU TELKOM & SECURITY SYSTEM', ' -', 'F002260000'),
(364, 'REGU UREA P-IB GR.A', ' -', 'F002113000'),
(365, 'REGU UREA P-IB GR.B', ' -', 'F002113000'),
(366, 'REGU UREA P-IB GR.C', ' -', 'F002113000'),
(367, 'REGU UREA P-IB GR.D', ' -', 'F002113000'),
(368, 'REGU UREA P-IIB GR.A', ' -', 'F002153000'),
(369, 'REGU UREA P-IIB GR.B', ' -', 'F002153000'),
(370, 'REGU UREA P-IIB GR.C', ' -', 'F002153000'),
(371, 'REGU UREA P-IIB GR.D', ' -', 'F002153000'),
(372, 'REGU UREA P-III GR.A', ' -', 'F002133000'),
(373, 'REGU UREA P-III GR.B', ' -', 'F002133000'),
(374, 'REGU UREA P-III GR.C', ' -', 'F002133000'),
(375, 'REGU UREA P-III GR.D', ' -', 'F002133000'),
(376, 'REGU UREA P-IV GR.A', ' -', 'F002143000'),
(377, 'REGU UREA P-IV GR.B', ' -', 'F002143000'),
(378, 'REGU UREA P-IV GR.C', ' -', 'F002143000'),
(379, 'REGU UREA P-IV GR.D', ' -', 'F002143000'),
(380, 'REGU UTILITAS P-IB GR.A', ' -', 'F002111000'),
(381, 'REGU UTILITAS P-IB GR.B', ' -', 'F002111000'),
(382, 'REGU UTILITAS P-IB GR.C', ' -', 'F002111000'),
(383, 'REGU UTILITAS P-IB GR.D', ' -', 'F002111000'),
(384, 'REGU UTILITAS P-III GR.A', ' -', 'F002131000'),
(385, 'REGU UTILITAS P-III GR.B', ' -', 'F002131000'),
(386, 'REGU UTILITAS P-III GR.C', ' -', 'F002131000'),
(387, 'REGU UTILITAS P-III GR.D', ' -', 'F002131000'),
(388, 'REGU UTILITAS P-IV GR.A', ' -', 'F002141000'),
(389, 'REGU UTILITAS P-IV GR.B', ' -', 'F002141000'),
(390, 'REGU UTILITAS P-IV GR.C', ' -', 'F002141000'),
(391, 'REGU UTILITAS P-IV GR.D', ' -', 'F002141000'),
(392, 'SEKSI AMMONIA P-IB GR.A', ' -', 'F002112000'),
(393, 'SEKSI AMMONIA P-IB GR.B', ' -', 'F002112000'),
(394, 'SEKSI AMMONIA P-IB GR.C', ' -', 'F002112000'),
(395, 'SEKSI AMMONIA P-IB GR.D', ' -', 'F002112000'),
(396, 'SEKSI AMMONIA P-IIB GR.A', ' -', 'F002152000'),
(397, 'SEKSI AMMONIA P-IIB GR.B', ' -', 'F002152000'),
(398, 'SEKSI AMMONIA P-IIB GR.C', ' -', 'F002152000'),
(399, 'SEKSI AMMONIA P-III GR.A', ' -', 'F002132000'),
(400, 'SEKSI AMMONIA P-III GR.B', ' -', 'F002132000'),
(401, 'SEKSI AMMONIA P-III GR.C', ' -', 'F002132000'),
(402, 'SEKSI AMMONIA P-III GR.D', ' -', 'F002132000'),
(403, 'SEKSI AMMONIA P-IV GR.A', ' -', 'F002142000'),
(404, 'SEKSI AMMONIA P-IV GR.B', ' -', 'F002142000'),
(405, 'SEKSI AMMONIA P-IV GR.C', ' -', 'F002142000'),
(406, 'SEKSI AMMONIA P-IV GR.D', ' -', 'F002142000'),
(407, 'SEKSI B.LISTRIK & PERALATAN PENDINGIN', ' -', 'F002220000'),
(408, 'SEKSI BENGKEL ALAT BERAT & ALAT BANTUAN', ' -', 'F002230000'),
(409, 'SEKSI BENGKEL INSTRUMEN & ELEKTRONIKA', ' -', 'F002220000'),
(410, 'SEKSI BENGKEL PIPA & LAS LAPANGAN-I', ' -', 'F002230000'),
(411, 'SEKSI BENGKEL PIPA & LAS LAPANGAN-II', ' -', 'F002230000'),
(412, 'SEKSI BENGKEL PIPA & LAS LAPANGAN-III', ' -', 'F002230000'),
(413, 'SEKSI BENGKEL REPARASI KHUSUS', ' -', 'F002230000'),
(414, 'SEKSI BENGKEL REPARASI MESIN', ' -', 'F002230000'),
(415, 'SEKSI BENGKEL REPARASI UMUM', ' -', 'F002230000'),
(416, 'SEKSI DERMAGA KHUSUS', ' -', 'F004246000'),
(417, 'SEKSI FABRIKASI PERALATAN & PROYEK', ' -', 'F002230000'),
(418, 'SEKSI INSTRUMEN P-IB', ' -', 'F002260000'),
(419, 'SEKSI INSTRUMEN P-IIB', ' -', 'F002260000'),
(420, 'SEKSI INSTRUMEN P-III', ' -', 'F002260000'),
(421, 'SEKSI INSTRUMEN P-IV', ' -', 'F002260000'),
(422, 'SEKSI INSTRUMEN PPU. UBS. & NPK', ' -', 'F002260000'),
(423, 'SEKSI INSTRUMEN STG. & JETTY', ' -', 'F002260000'),
(424, 'SEKSI ISOLASI', ' -', 'F002230000'),
(425, 'SEKSI LAB CHEM.CLEANING & BENFIELD', ' -', 'F002320000'),
(426, 'SEKSI LAB INVENTORY & SISTEM MUTU', ' -', 'F002320000'),
(427, 'SEKSI LAB KALIBRASI & PEMELIHARAAN', ' -', 'F002320000'),
(428, 'SEKSI LAB KONTROL P-IB', ' -', 'F002320000'),
(429, 'SEKSI LAB KONTROL P-II/PII-B/STG', ' -', 'F002320000'),
(430, 'SEKSI LAB KONTROL P-III/UTILITAS P-II', ' -', 'F002320000'),
(431, 'SEKSI LAB KONTROL P-IV', ' -', 'F002320000'),
(432, 'SEKSI LAB PENGUJIAN GAS & LUBE OIL', ' -', 'F002320000'),
(433, 'SEKSI LAB PENGUJIAN NPK.STG.BATUBARA', ' -', 'F002320000'),
(434, 'SEKSI LAB PENGUJIAN PRODUK', ' -', 'F002320000'),
(435, 'SEKSI LAB PENGUJIAN UMUM', ' -', 'F002320000'),
(436, 'SEKSI LISTRIK JARINGAN', ' -', 'F002220000'),
(437, 'SEKSI LISTRIK P-IB', ' -', 'F002220000'),
(438, 'SEKSI LISTRIK P-IIB', ' -', 'F002220000'),
(439, 'SEKSI LISTRIK P-III', ' -', 'F002220000'),
(440, 'SEKSI LISTRIK P-IV', ' -', 'F002220000'),
(441, 'SEKSI LISTRIK PPU I', ' -', 'F002220000'),
(442, 'SEKSI LISTRIK PPU II (UBS.2D.&NPK)', ' -', 'F002220000'),
(443, 'SEKSI LISTRIK STG & JETTY', ' -', 'F002220000'),
(444, 'SEKSI MEKANIKAL AMMONIA P-IB', ' -', 'F002210000'),
(445, 'SEKSI MEKANIKAL AMMONIA P-IIB', ' -', 'F002210000'),
(446, 'SEKSI MEKANIKAL AMMONIA P-III', ' -', 'F002210000'),
(447, 'SEKSI MEKANIKAL AMMONIA P-IV', ' -', 'F002210000'),
(448, 'SEKSI MEKANIKAL NPK', ' -', 'F002210000'),
(449, 'SEKSI MEKANIKAL PPU I', ' -', 'F002210000'),
(450, 'SEKSI MEKANIKAL PPU II', ' -', 'F002210000'),
(451, 'SEKSI MEKANIKAL STG & JETTY', ' -', 'F002210000'),
(452, 'SEKSI MEKANIKAL UBS & PRODUK SAMPING', ' -', 'F002210000'),
(453, 'SEKSI MEKANIKAL UREA & UTILITAS P-IIB', ' -', 'F002210000'),
(454, 'SEKSI MEKANIKAL UREA P-IB', ' -', 'F002210000'),
(455, 'SEKSI MEKANIKAL UREA P-III', ' -', 'F002210000'),
(456, 'SEKSI MEKANIKAL UREA P-IV', ' -', 'F002210000'),
(457, 'SEKSI MEKANIKAL UTILITAS P-IB', ' -', 'F002210000'),
(458, 'SEKSI MEKANIKAL UTILITAS P-II', ' -', 'F002210000'),
(459, 'SEKSI MEKANIKAL UTILITAS P-III', ' -', 'F002210000'),
(460, 'SEKSI MEKANIKAL UTILITAS P-IV', ' -', 'F002210000'),
(461, 'SEKSI OP. PABRIK NPK-1 & P. HAYATI GR. A', ' -', 'F002181000'),
(462, 'SEKSI OP. PABRIK NPK-1 & P. HAYATI GR. B', ' -', 'F002181000'),
(463, 'SEKSI OP. PABRIK NPK-1 & P. HAYATI GR. C', ' -', 'F002181000'),
(464, 'SEKSI OP. PABRIK NPK-1 & P. HAYATI GR. D', ' -', 'F002181000'),
(465, 'SEKSI OPERASI ALAT BERAT', ' -', 'F002230000'),
(466, 'SEKSI OPERASI SEKURITI', ' -', 'F006220000'),
(467, 'SEKSI PEKERJAAN SIPIL', ' -', 'F002230000'),
(468, 'SEKSI PEMUATAN PUPUK', ' -', 'F004246000'),
(469, 'SEKSI PERBAIKAN PERALATAN', ' -', 'F002230000'),
(470, 'SEKSI PERGUDANGAN BAHAN BAKU', ' -', 'F003430000'),
(471, 'SEKSI PERGUDANGAN I', ' -', 'F003430000'),
(472, 'SEKSI PERGUDANGAN II', ' -', 'F003430000'),
(473, 'SEKSI PERLENGKAPAN & MATERIAL', ' -', 'F002330000'),
(474, 'SEKSI PK & KK GR.A', ' -', 'F002330000'),
(475, 'SEKSI PK & KK GR.B', ' -', 'F002330000'),
(476, 'SEKSI PK & KK GR.C', ' -', 'F002330000'),
(477, 'SEKSI PK & KK GR.D', ' -', 'F002330000'),
(478, 'SEKSI PPU-1 GR.A', ' -', 'F002161100'),
(479, 'SEKSI PPU-1 GR.B', ' -', 'F002161100'),
(480, 'SEKSI PPU-1 GR.C', ' -', 'F002161100'),
(481, 'SEKSI PPU-2 & PRODUK SAMPING GR.A', ' -', 'F002163000'),
(482, 'SEKSI PPU-2 & PRODUK SAMPING GR.B', ' -', 'F002163000'),
(483, 'SEKSI PPU-2 & PRODUK SAMPING GR.D', ' -', 'F002163000'),
(484, 'SEKSI STG & BOILER BATU BARA GRUP.A', ' -', 'F002172000'),
(485, 'SEKSI STG & BOILER BATU BARA GRUP.C', ' -', 'F002172000'),
(486, 'SEKSI STG & BOILER BATU BARA GRUP.D', ' -', 'F002172000'),
(487, 'SEKSI TELKOM & SECURITY SISTEM', ' -', 'F002260000'),
(488, 'SEKSI UREA P-IB GR.A', ' -', 'F002113000'),
(489, 'SEKSI UREA P-IB GR.B', ' -', 'F002113000'),
(490, 'SEKSI UREA P-IB GR.C', ' -', 'F002113000'),
(491, 'SEKSI UREA P-IB GR.D', ' -', 'F002113000'),
(492, 'SEKSI UREA P-IIB GR.A', ' -', 'F002153000'),
(493, 'SEKSI UREA P-IIB GR.B', ' -', 'F002153000'),
(494, 'SEKSI UREA P-IIB GR.C', ' -', 'F002153000'),
(495, 'SEKSI UREA P-IIB GR.D', ' -', 'F002153000'),
(496, 'SEKSI UREA P-III GR.A', ' -', 'F002133000'),
(497, 'SEKSI UREA P-III GR.B', ' -', 'F002133000'),
(498, 'SEKSI UREA P-III GR.C', ' -', 'F002133000'),
(499, 'SEKSI UREA P-III GR.D', ' -', 'F002133000'),
(500, 'SEKSI UREA P-IV GR.A', ' -', 'F002143000'),
(501, 'SEKSI UREA P-IV GR.B', ' -', 'F002143000'),
(502, 'SEKSI UREA P-IV GR.C', ' -', 'F002143000'),
(503, 'SEKSI UREA P-IV GR.D', ' -', 'F002143000'),
(504, 'SEKSI UTILITAS P-IB GR.A', ' -', 'F002111000'),
(505, 'SEKSI UTILITAS P-IB GR.B', ' -', 'F002111000'),
(506, 'SEKSI UTILITAS P-IB GR.C', ' -', 'F002111000'),
(507, 'SEKSI UTILITAS P-IB GR.D', ' -', 'F002111000'),
(508, 'SEKSI UTILITAS P-IIB GR.A', ' -', 'F002151000'),
(509, 'SEKSI UTILITAS P-IIB GR.B', ' -', 'F002151000'),
(510, 'SEKSI UTILITAS P-IIB GR.C', ' -', 'F002151000'),
(511, 'SEKSI UTILITAS P-IIB GR.D', ' -', 'F002151000'),
(512, 'SEKSI UTILITAS P-III GR.A', ' -', 'F002131000'),
(513, 'SEKSI UTILITAS P-III GR.B', ' -', 'F002131000'),
(514, 'SEKSI UTILITAS P-III GR.C', ' -', 'F002131000'),
(515, 'SEKSI UTILITAS P-III GR.D', ' -', 'F002131000'),
(516, 'SEKSI UTILITAS P-IV GR.A', ' -', 'F002141000'),
(517, 'SEKSI UTILITAS P-IV GR.B', ' -', 'F002141000'),
(518, 'SEKSI UTILITAS P-IV GR.C', ' -', 'F002141000'),
(519, 'SEKSI UTILITAS P-IV GR.D', ' -', 'F002141000'),
(520, 'SUB-SEKSI AMMONIA P-IB GR.A', ' -', 'F002112000'),
(521, 'SUB-SEKSI AMMONIA P-IB GR.B', ' -', 'F002112000'),
(522, 'SUB-SEKSI AMMONIA P-IB GR.C', ' -', 'F002112000'),
(523, 'SUB-SEKSI AMMONIA P-IB GR.D', ' -', 'F002112000'),
(524, 'SUB-SEKSI AMMONIA P-IIB GR.A', ' -', 'F002152000'),
(525, 'SUB-SEKSI AMMONIA P-IIB GR.B', ' -', 'F002152000'),
(526, 'SUB-SEKSI AMMONIA P-IIB GR.C', ' -', 'F002152000'),
(527, 'SUB-SEKSI AMMONIA P-IIB GR.D', ' -', 'F002152000'),
(528, 'SUB-SEKSI AMMONIA P-III GR.A', ' -', 'F002132000'),
(529, 'SUB-SEKSI AMMONIA P-III GR.B', ' -', 'F002132000'),
(530, 'SUB-SEKSI AMMONIA P-III GR.C', ' -', 'F002132000'),
(531, 'SUB-SEKSI AMMONIA P-III GR.D', ' -', 'F002132000'),
(532, 'SUB-SEKSI AMMONIA P-IV GR.A', ' -', 'F002142000'),
(533, 'SUB-SEKSI AMMONIA P-IV GR.B', ' -', 'F002142000'),
(534, 'SUB-SEKSI AMMONIA P-IV GR.C', ' -', 'F002142000'),
(535, 'SUB-SEKSI AMMONIA P-IV GR.D', ' -', 'F002142000'),
(536, 'SUB-SEKSI HANDLING AMMONIA', ' -', 'F002163000'),
(537, 'SUB-SEKSI OP.PABRIK NPK-1&P.HAYATI GR.A', ' -', 'F002181000'),
(538, 'SUB-SEKSI OP.PABRIK NPK-1&P.HAYATI GR.B', ' -', 'F002181000'),
(539, 'SUB-SEKSI OP.PABRIK NPK-1&P.HAYATI GR.C', ' -', 'F002181000'),
(540, 'SUB-SEKSI OP.PABRIK NPK-1&P.HAYATI GR.D', ' -', 'F002181000'),
(541, 'SUB-SEKSI PABEAN', ' -', 'F002163300'),
(542, 'SUB-SEKSI PPU-1 GR.A', ' -', 'F002161100'),
(543, 'SUB-SEKSI PPU-1 GR.A', ' -', 'F002161200'),
(544, 'SUB-SEKSI PPU-1 GR.B', ' -', 'F002161100'),
(545, 'SUB-SEKSI PPU-1 GR.B', ' -', 'F002161200'),
(546, 'SUB-SEKSI PPU-1 GR.C', ' -', 'F002161100'),
(547, 'SUB-SEKSI PPU-1 GR.C', ' -', 'F002161200'),
(548, 'SUB-SEKSI PPU-1 GR.D', ' -', 'F002161100'),
(549, 'SUB-SEKSI PPU-1 GR.D', ' -', 'F002161200'),
(550, 'SUB-SEKSI PPU-2 & PRODUK SAMPING GR.A', ' -', 'F002163000'),
(551, 'SUB-SEKSI PPU-2 & PRODUK SAMPING GR.A', ' -', 'F002163400'),
(552, 'SUB-SEKSI PPU-2 & PRODUK SAMPING GR.A', ' -', 'F002163100'),
(553, 'SUB-SEKSI PPU-2 & PRODUK SAMPING GR.B', ' -', 'F002163000'),
(554, 'SUB-SEKSI PPU-2 & PRODUK SAMPING GR.B', ' -', 'F002163400'),
(555, 'SUB-SEKSI PPU-2 & PRODUK SAMPING GR.B', ' -', 'F002163100'),
(556, 'SUB-SEKSI PPU-2 & PRODUK SAMPING GR.B', ' -', 'F002163200'),
(557, 'SUB-SEKSI PPU-2 & PRODUK SAMPING GR.C', ' -', 'F002163000'),
(558, 'SUB-SEKSI PPU-2 & PRODUK SAMPING GR.C', ' -', 'F002163400'),
(559, 'SUB-SEKSI PPU-2 & PRODUK SAMPING GR.C', ' -', 'F002163200'),
(560, 'SUB-SEKSI PPU-2 & PRODUK SAMPING GR.C', ' -', 'F002163100'),
(561, 'SUB-SEKSI PPU-2 & PRODUK SAMPING GR.D', ' -', 'F002163000'),
(562, 'SUB-SEKSI PPU-2 & PRODUK SAMPING GR.D', ' -', 'F002163400'),
(563, 'SUB-SEKSI PPU-2 & PRODUK SAMPING GR.D', ' -', 'F002163100'),
(564, 'SUB-SEKSI STG&BOILER BATUBARA GR.A', ' -', 'F002172000'),
(565, 'SUB-SEKSI STG&BOILER BATUBARA GR.B', ' -', 'F002172000'),
(566, 'SUB-SEKSI STG&BOILER BATUBARA GR.C', ' -', 'F002172000'),
(567, 'SUB-SEKSI STG&BOILER BATUBARA GR.D', ' -', 'F002172000'),
(568, 'SUB-SEKSI UREA P-IB GR.A', ' -', 'F002113000'),
(569, 'SUB-SEKSI UREA P-IB GR.B', ' -', 'F002113000'),
(570, 'SUB-SEKSI UREA P-IB GR.C', ' -', 'F002113000'),
(571, 'SUB-SEKSI UREA P-IB GR.D', ' -', 'F002113000'),
(572, 'SUB-SEKSI UREA P-IIB GR.A', ' -', 'F002153000'),
(573, 'SUB-SEKSI UREA P-IIB GR.B', ' -', 'F002153000'),
(574, 'SUB-SEKSI UREA P-IIB GR.C', ' -', 'F002153000'),
(575, 'SUB-SEKSI UREA P-IIB GR.D', ' -', 'F002153000'),
(576, 'SUB-SEKSI UREA P-III GR.A', ' -', 'F002133000'),
(577, 'SUB-SEKSI UREA P-III GR.B', ' -', 'F002133000'),
(578, 'SUB-SEKSI UREA P-III GR.C', ' -', 'F002133000'),
(579, 'SUB-SEKSI UREA P-III GR.D', ' -', 'F002133000'),
(580, 'SUB-SEKSI UREA P-IV GR.A', ' -', 'F002143000'),
(581, 'SUB-SEKSI UREA P-IV GR.B', ' -', 'F002143000'),
(582, 'SUB-SEKSI UREA P-IV GR.C', ' -', 'F002143000'),
(583, 'SUB-SEKSI UREA P-IV GR.D', ' -', 'F002143000'),
(584, 'SUB-SEKSI UTILITAS P-IB GR.B', ' -', 'F002111000'),
(585, 'SUB-SEKSI UTILITAS P-IB GR.C', ' -', 'F002111000'),
(586, 'SUB-SEKSI UTILITAS P-IB GR.D', ' -', 'F002111000'),
(587, 'SUB-SEKSI UTILITAS P-IB GR.R', ' -', 'F002111000'),
(588, 'SUB-SEKSI UTILITAS P-IIB GR.A', ' -', 'F002151000'),
(589, 'SUB-SEKSI UTILITAS P-IIB GR.B', ' -', 'F002151000'),
(590, 'SUB-SEKSI UTILITAS P-IIB GR.C', ' -', 'F002151000'),
(591, 'SUB-SEKSI UTILITAS P-IIB GR.C', ' -', 'F007020004'),
(592, 'SUB-SEKSI UTILITAS P-IIB GR.D', ' -', 'F002151000'),
(593, 'SUB-SEKSI UTILITAS P-III GR.A', ' -', 'F002131000'),
(594, 'SUB-SEKSI UTILITAS P-III GR.B', ' -', 'F002131000'),
(595, 'SUB-SEKSI UTILITAS P-III GR.C', ' -', 'F002131000'),
(596, 'SUB-SEKSI UTILITAS P-III GR.D', ' -', 'F002131000'),
(597, 'SUB-SEKSI UTILITAS P-IV GR.A', ' -', 'F002141000'),
(598, 'SUB-SEKSI UTILITAS P-IV GR.B', ' -', 'F002141000'),
(599, 'SUB-SEKSI UTILITAS P-IV GR.C', ' -', 'F002141000'),
(600, 'SUB-SEKSI UTILITAS P-IV GR.D', ' -', 'F002141000'),
(601, 'UNIT PENGANTONGAN PUPUK BELAWAN', ' -', 'F004241000'),
(602, 'UNIT PENGANTONGAN PUPUK CILACAP', ' -', 'F004242000'),
(603, 'UNIT PENGANTONGAN PUPUK MENENG', ' -', 'F004244000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `no_badge` varchar(25) NOT NULL,
  `username` varchar(120) NOT NULL,
  `password` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `no_badge`, `username`, `password`) VALUES
(1, '1687', 'admin', 'admin'),
(2, '00001', 'Pusri_jaya', 'Palembang21');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_aset`
--
ALTER TABLE `tb_aset`
  ADD PRIMARY KEY (`no_aset`);

--
-- Indeks untuk tabel `tb_detail`
--
ALTER TABLE `tb_detail`
  ADD PRIMARY KEY (`id_detail`),
  ADD KEY `fk_master_detail` (`no_pppk`);

--
-- Indeks untuk tabel `tb_history_mutasi`
--
ALTER TABLE `tb_history_mutasi`
  ADD PRIMARY KEY (`id_history`);

--
-- Indeks untuk tabel `tb_log_status`
--
ALTER TABLE `tb_log_status`
  ADD PRIMARY KEY (`id_status`);

--
-- Indeks untuk tabel `tb_master`
--
ALTER TABLE `tb_master`
  ADD PRIMARY KEY (`no_pppk`);

--
-- Indeks untuk tabel `tb_sparepart`
--
ALTER TABLE `tb_sparepart`
  ADD PRIMARY KEY (`id_sparepart`);

--
-- Indeks untuk tabel `tb_tindakan`
--
ALTER TABLE `tb_tindakan`
  ADD PRIMARY KEY (`id_tindakan`),
  ADD KEY `id_detail` (`no_aset`);

--
-- Indeks untuk tabel `tb_unitkerja`
--
ALTER TABLE `tb_unitkerja`
  ADD PRIMARY KEY (`kd_unitkerja`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_detail`
--
ALTER TABLE `tb_detail`
  MODIFY `id_detail` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_history_mutasi`
--
ALTER TABLE `tb_history_mutasi`
  MODIFY `id_history` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_log_status`
--
ALTER TABLE `tb_log_status`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_tindakan`
--
ALTER TABLE `tb_tindakan`
  MODIFY `id_tindakan` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_unitkerja`
--
ALTER TABLE `tb_unitkerja`
  MODIFY `kd_unitkerja` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=604;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tb_detail`
--
ALTER TABLE `tb_detail`
  ADD CONSTRAINT `fk_master_detail` FOREIGN KEY (`no_pppk`) REFERENCES `tb_master` (`no_pppk`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
