 <?php
session_start();
require'koneksi.php';

function create_random($length)
{
    $data = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $string = '';
    for($i = 0; $i < $length; $i++) {
        $pos = rand(0, strlen($data)-1);
        $string .= $data ($pos);
    }
    return $string;
}

?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Helpdesk Infrastruktur - Departement TI</title>
    <!-- Bootstrap CSS -->
    <script src="link/js1.js"></script>
    <link rel="stylesheet" href="link/css1.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="link/js2.js" type="text/javascript"></script>
    <link href="link/css2.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="link/css3.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="asset/bootstrap-4.3.1/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="costum.css">
    <link href="font/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="asset/select2.min.css" rel="stylesheet" />

</head>

<body>
     
<section class="form_pppk" id="form_pppk">

  

 <div class="container">
  <div class="row">
  <div class="col-sm-12 text-center ">
  <h3 class="text-center"> <b> <font color="000080"> </font> </b> </h3>
  <br>
  </div>
  </div>

  <div class="row">

    <div class="col-sm-8 offset-sm-2  daftar">
    <div class="scheduleTab">
    <div class="tab-content">
      <div id="day1" class="tab-pane active">
        
        <a href="login.php" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">Login Admin!</a>
        |
        <a href="track_aduan.php" class="btn btn-success btn-sm active" role="button" aria-pressed="true">Track Aduan!</a>
        |
        <a href="info.php" class="btn btn-info btn-sm active" role="button" aria-pressed="true">Informasi!</a>
        
         <div class="head-form">
              <p><b>Helpdesk Infrastruktur - Departemen TI</b><p>
         </div>

  <div class="isiform">
  <form method="post" action="proses_register.php">

  <div class="form-group">
    <label for="no_pppk">No. Tiket</label>
    <input required="" name="no_pppk" type="text" class="form-control input-sm" id="nopppk" value="<?php echo  $num = create_random(5);?>" readonly >
  </div>


<div class="form-group">
    <label for="tipe_aduan">Tipe Aduan</label>
    <select class="form-control" id="tipe_aduan" name="tipe_aduan" placeholder="Pilih Tipe Aduan Anda">
        <option>--Pilih Tipe Aduan Anda--</option>
        <option>Perbaikan PC/Hardware</option>
        <option>Perbaikan Jaringan</option>
        <option>Remote Support</option>
        <option>Video Conference</option>
        <option>Lainnya</option>
    </select>
  </div>

  <div class="form-group">
    <label for="unit_kerja">Unit Kerja</label>
    <!-- <input list="no_asets" id="unit_kerja" name="unit_kerja" placeholder="Cari no aset..." class="form-control input-sm" autocomplete="off">
      <datalist id="no_asets">
        <?php
        include'../ext/db.php';
         $tampil3 = $conn->query("SELECT * FROM tb_unitkerja ORDER BY kd_unitkerja ASC");
         while ($row3 = mysqli_fetch_array($tampil3)){
         ?>             
         <option value="<?php echo $row3['unit_kerja'];?>"></option>';
      <?php }?>
      </datalist> -->

    <select class="form-control" id="unit_kerja" name="unit_kerja">
      <option></option>
      <?php
        include'../ext/db.php';
         $tampil4 = $conn->query("SELECT * FROM tb_unitkerja ORDER BY kd_unitkerja ASC");
         while ($row4 = mysqli_fetch_array($tampil4)){
         ?>
         <option value="<?php echo $row4['kd_unitkerja'];?>"><?php echo $row4['unit_kerja'];?></option>';
      <?php }?>
    </select>
  </div>

  <!-- auto unit kerja -->
  <div id="onajax"></div>

  <div class="form-group">
    <label for="penghubung">Penghubung</label>
    <input type="text" required oninvalid="this.setCustomValidity('Harap Lengkapi Semua Kolom')"  class="form-control"  id="penghubung" name="penghubung" placeholder="Masukan nama anda" autocomplete="off">
  </div>

  <div class="form-group">
    <label for="telepon">Telepon/ext.</label>
    <input type="text" required oninvalid="this.setCustomValidity('Harap Lengkapi Semua Kolom')" class="form-control" id="telepon" placeholder="Tambahkan nomor telepon/ext. anda" name="telepon" autocomplete="off">
  </div>

  <div class="form-group">
    <label for="email">E-mail Penghubung</label>
    <input type="email" class="form-control" id="email" required oninvalid="this.setCustomValidity('Harap Lengkapi Semua Kolom')" placeholder="Masukan e-mail anda" name="email" required="" autocomplete="off">
  </div>

  <div class="form-group">
    <label for="email">E-mail Atasan</label>
    <input type="email" class="form-control" id="email" required oninvalid="this.setCustomValidity('Harap Lengkapi Semua Kolom')" placeholder="Masukan e-mail atasan anda" name="emailatasan" required="" autocomplete="off">
  </div>

  <div class="form-group">
    <button type="submit" class="btn btn-primary btn-md" name="submit">Submit</button>
  </div>
</div>
</section>
  
    <script src="link/js3.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="link/js4.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="asset/jQuary-3.3.1/jquery-3.3.1.min.js"></script>
<script src="asset/bootstrap-4.3.1/js/bootstrap.min.js"></script>
<script src="asset/select2.min.js"></script>
    <script>
        $("#unit_kerja").change(function(){
            var id = $("#unit_kerja").val();
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "ajax_lokasi.php?id="+id,
                success: function(msg){
                     // alert(msg);
                    if(msg == ''){
                        alert('Tidak ada data');
                    }
                    else{
                        $("#onajax").html(msg);
                    }
                }
            });
        });

        $("#captinput").change(function(){
            var code = $("#captinput").val();

            $.ajax({
                type: "POST",
                dataType: "html",
                url: "chapta/periksa_captcha.php?nilaiCaptcha="+code,
                success: function(msg){
                    // alert(msg);
                    if(msg == ''){
                        alert('Tidak ada data');
                    }
                    else{
                        $("#respon").html(msg);
                    }
                }
            });
        });

    $(document).ready(function(){
        $('#input_keluhan').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            //menggunakan fungsi ajax untuk pengambilan data
             // alert(rowid);
            $.ajax({
                type : 'post',
                // url : 'ajax_keluhan.php',
                url : 'ajax_keluhan.php?idppk='+rowid,
                data :  'rowid='+ rowid,
                success : function(data){

                $('.fetched-data').html(data);//menampilkan data ke dalam modal
                }
            });
         });
    });

    </script>

    <script type="text/javascript">
$(document).ready(function() {
     $('#unit_kerja').select2({
      placeholder: 'Pilih Unit Kerja',
      allowClear: true
     });
 });
</script>
  </body>
</html>
