<?php
require'../koneksi.php';

?>
<link href="../asset/select2.min.css" rel="stylesheet" />
<form method="post" action="proses/pro_mutasi.php">
  <div class="form-group">
    <label for="no_pppk">Berlaku Mulai</label>
    <input required=""  name="tgl" type="date" class="form-control input-sm" >
  </div>
  
  <div class="form-group">

    <label for="no_aset">No. Aset</label>
    <input list="no_asets" name="no_aset" placeholder="Masukan No. Aset" class="form-control input-sm" autocomplete="off">
      <datalist id="no_asets">
        <?php
         $tampil3 = $conn->query("SELECT * FROM tb_aset ORDER BY no_aset ASC");
         while ($row3 = mysqli_fetch_array($tampil3)){
         ?>              
         <option value="<?php echo $row3['no_aset'];?>"></option>';
      <?php }?>
      </datalist>

<!--     <select class="form-control" name="no_aset">
      <option value="">--Pilih No. Aset--</option>
      <?php
        
         $tampil3 = $conn->query("SELECT * FROM tb_aset ORDER BY no_aset ASC");
         while ($row3 = mysqli_fetch_array($tampil3)){
         ?>             
         <option value="<?php echo $row3['no_aset'];?>"><?php echo $row3['no_aset'];?></option>';
      <?php }?>
    </select>
 -->  </div>

  <div class="form-group">
    <label for="no_aset">Unit Kerja</label>
    <input list="unit" name="uk" placeholder="Masukan Unit Kerja" class="form-control input-sm" autocomplete="off">
    <datalist id="unit" >
      <!-- <option value=""></option> -->
      <?php
        
         $tampil3 = $conn->query("SELECT * FROM tb_unitkerja ORDER BY kd_unitkerja ASC");
         while ($row3 = mysqli_fetch_array($tampil3)){
         ?>             
         <option value="<?php echo $row3['unit_kerja'];?>"></option>';
      <?php }?>
</datalist>
  </div>

  <div class="form-group">
    <label>Pengguna</label>
    <input type="text" class="form-control" placeholder="Masukan nama pengguna" name="pengguna" autocomplete="off"></input>
  </div>

  <div class="form-group">
    <label>Telp/ext</label>
    <input type="text" class="form-control" placeholder="Masukan telp/ext pengguna" name="ext" autocomplete="off"></input>
  </div>

<div class="form-group">
    <label>Keterangan</label>
    <textarea class="form-control" name="ket_mutasi" rows="6"></textarea>
  </div>


  <button type="submit" class="btn btn-success" name="add">Simpan</button>
</form>
<script src="../asset/select2.min.js"></script>
<script>
function myFunction() {
  document.getElementById("frm1").submit();
}
</script>
<script type="text/javascript">
$(document).ready(function() {
     $('#unit_kerja').select2({
      placeholder: 'Pilih Unit Kerja',
      allowClear: true
     });
 });
</script>