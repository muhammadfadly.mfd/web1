<div class="table-responsive">  
                    <table border="1" cellspacing="0" width="1855" style="background-color: #FFFFFF;">
                      <thead style="background-color: #FFD700; color: #FFFFFF;">
                        <tr>
  
                          <th width="100" height="30"><center><b>No. Tiket</b></center></th>
                          <th width="100" height="30"><center><b>Tanggal</b></center></th>
                          <th width="200" height="30"><center><b>Unit Kerja</b></center></th>
                          <th width="285" height="30"><center><b>Lokasi</b></center></th>
                          <th width="120" height="30"><center><b>Cost center</b></center></th>
                          <th width="200" height="30"><center><b>Penghubung</b></center></th>
                          <th width="150" height="30"><center><b>Telepon</b></center></th>
                          <th width="400" height="30"><center><b>Email</b></center></th>
                          <th width="100" height="30"><center><b>Aksi 1</b></center></th>
                          <th width="100" height="30"><center><b>Aksi 2</b></center></th>
                          <th width="100" height="30"><center><b>Aksi 3</b></center></th>
                        
                        </tr>
                      </thead>
   <tbody>
    <?php
    include "../../koneksi.php";
    // $A =array();
    $page = (isset($_POST['page']))? $_POST['page'] : 1;
    $limit = 10; // Jumlah data per halamannya
    $no = (($page - 1) * $limit) + 1; // Untuk setting awal nomor pada halaman yang aktif
    $limit_start = ($page - 1) * $limit;
    if(isset($_POST['search']) && $_POST['search'] == true){ 
      $param = '%'.mysqli_real_escape_string($conn, $keyword).'%';

      $sql = mysqli_query($conn, "SELECT tb_master.no_pppk, tanggal, tipe_aduan, tb_unitkerja.unit_kerja, tb_master.lokasi, tb_master.cost_center, penghubung, telepon, email FROM tb_master join tb_unitkerja on tb_master.unit_kerja=tb_unitkerja.kd_unitkerja WHERE tb_master.no_pppk LIKE '".$param."' OR tanggal LIKE '".$param."' OR tipe_aduan LIKE '".$param."' OR tb_unitkerja.unit_kerja LIKE '".$param."' OR tb_master.lokasi LIKE '".$param."' OR tb_master.cost_center LIKE '".$param."' OR penghubung LIKE '".$param."' OR telepon LIKE '".$param."' OR email LIKE '".$param."' LIMIT ".$limit_start.",".$limit);

      $sql2 = mysqli_query($conn, "SELECT COUNT(*) AS jumlah,tb_master.no_pppk, tanggal, tipe_aduan, tb_unitkerja.unit_kerja, tb_master.lokasi, tb_master.cost_center, penghubung, telepon, email FROM tb_master join tb_unitkerja on tb_master.unit_kerja=tb_unitkerja.kd_unitkerja where tb_master.no_pppk LIKE '".$param."' OR tanggal LIKE '".$param."' OR tipe_aduan LIKE '".$param."' OR tb_unitkerja.unit_kerja LIKE '".$param."' OR tb_master.lokasi LIKE '".$param."' OR tb_master.cost_center LIKE '".$param."' OR penghubung LIKE '".$param."' OR telepon LIKE '".$param."' OR email LIKE '".$param."'");
      $get_jumlah = mysqli_fetch_array($sql2);
    }else{ 
      $sql = mysqli_query($conn, "SELECT tb_master.no_pppk, tanggal, tipe_aduan, tb_unitkerja.unit_kerja, tb_master.lokasi, tb_master.cost_center, penghubung, telepon, email FROM tb_master join tb_unitkerja on tb_master.unit_kerja=tb_unitkerja.kd_unitkerja ORDER by tanggal DESC LIMIT ".$limit_start.",".$limit);
      $sql2 = mysqli_query($conn, "SELECT COUNT(*) AS jumlah FROM tb_master");
      $get_jumlah = mysqli_fetch_array($sql2);
    }
    $i=0;
    while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
      $forpage[$i]=$no;
      ?>
      <tr>
        <td width="100"><center><?= $data['no_pppk']; ?></center></td>
        <td width="100"><center><?php echo date('d-m-Y H:i:s', strtotime($data['tanggal'])); ?></center></td>
        <td width="200"><center><?= $data['unit_kerja']; ?></center></td>
        <td width="230"><center><?= $data['lokasi']; ?></center></td>
        <td width="120"><center><?= $data['cost_center']; ?></center></td>
        <td width="200"><center><?= $data['penghubung']; ?></center></td>
        <td width="150"><center><?= $data['telepon']; ?></center></td>
        <td width="400"><center><?= $data['email']; ?></center></td>
        <td width="100"><center>               
        <a href="diagnosa.php?id_pppk=<?php echo $data['no_pppk']; ?>"><button type="button" class="btn btn-warning btn-sm" name="diagnosa" value="1">Diagnosa</button></a>
        <br></center>
        </td>
        <td width="100"><center>
        <a href="tindakan.php?id_pppk=<?php echo $data['no_pppk']; ?>"> <button type="button" class="btn btn-info btn" name="tindakan" value="2">Tindakan</button></a><br></center>
        </td> 
        <td width="100"><center>
          <a href="print-tiket.php?id_pppk=<?php echo $data['no_pppk']; ?>"> <button type="button" class="btn btn-primary" name="tindakan" value="2">Cetak Tiket</button></a><br></center>
        </td> 

      </tr>
      <?php
      $no++;
      $i++;
    }
    ?>
    </tbody>
  </table>
</div>

<?php
$count = mysqli_num_rows($sql);

if($count > 0){ // Jika datanya ada, tampilkan paginationnya
    ?>

<!-- <div class="row"> -->

  <!-- <div class="col-md-3"> -->
    <div class="informasi-data "><p>Menampilkan <?= min($forpage) ?> hingga <?= max($forpage) ?> dari <?= $get_jumlah['jumlah'] ?> entri</div>
<!-- </div> -->

<!-- <div class="col-md-3"> -->

    <ul class="pagination ">
      <!-- LINK FIRST AND PREV -->
      <?php
      if($page == 1){ // Jika page adalah page ke 1, maka disable link PREV
      ?>
        <li class="disabled"><a href="#">First</a></li>
        <li class="disabled"><a href="#">&laquo;</a></li>
      <?php
      }else{ // Jika page bukan page ke 1
        $link_prev = ($page > 1)? $page - 1 : 1;
      ?>
        <li><a href="javascript:void(0);" onclick="searchWithPagination(1, false)">First</a></li>
        <li><a href="javascript:void(0);" onclick="searchWithPagination(<?php echo $link_prev; ?>, false)">&laquo;</a></li>
      <?php
      }
      ?>

      <!-- LINK NUMBER -->
      <?php
      $jumlah_page = ceil($get_jumlah['jumlah'] / $limit); // Hitung jumlah halamannya
      $jumlah_number = 3; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
      $start_number = ($page > $jumlah_number)? $page - $jumlah_number : 1; // Untuk awal link number
      $end_number = ($page < ($jumlah_page - $jumlah_number))? $page + $jumlah_number : $jumlah_page; // Untuk akhir link number

      for($i = $start_number; $i <= $end_number; $i++){
        $link_active = ($page == $i)? ' class="active"' : '';
      ?>
        <li<?php echo $link_active; ?>><a href="javascript:void(0);" onclick="searchWithPagination(<?php echo $i; ?>, false)"><?php echo $i; ?></a></li>
      <?php
      }
      ?>

      <!-- LINK NEXT AND LAST -->
      <?php
      // Jika page sama dengan jumlah page, maka disable link NEXT nya
      // Artinya page tersebut adalah page terakhir
      if($page == $jumlah_page){ // Jika page terakhir
      ?>
        <li class="disabled"><a href="#">&raquo;</a></li>
        <li class="disabled"><a href="#">Last</a></li>
      <?php
      }else{ // Jika Bukan page terakhir
        $link_next = ($page < $jumlah_page)? $page + 1 : $jumlah_page;
      ?>
        <li><a href="javascript:void(0);" onclick="searchWithPagination(<?php echo $link_next; ?>, false)">&raquo;</a></li>
        <li><a href="javascript:void(0);" onclick="searchWithPagination(<?php echo $jumlah_page; ?>, false)">Last</a></li>
      <?php
      }
      ?>
    </ul>

    <!-- </div> -->
    
    <!-- </div> -->

    <?php
}
?>
