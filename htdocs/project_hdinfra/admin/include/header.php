<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Dashboard admin</title>

    <!-- Bootstrap -->
    <link href="asset_admin/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../font/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="asset_admin/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="asset_admin/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="asset_admin/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="asset_admin/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="asset_admin/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="asset_admin/build/css/custom.css" rel="stylesheet">

     <!-- <link rel="stylesheet" type="text/css" href="../../asset/dist/sweetalert.css"> -->
<!-- <script type="text/javascript" src="../../asset/dist/sweetalert.min.js"></script> -->
  </head>

<?php
session_start();
if (!isset($_SESSION['badge'])) {

  header('location:../login.php');
  }
?>

<style type="text/css">
  .img-logo{
    background-image: url("../img/DB_LOGO.jpg");
    background-size: 230px ;
    background-repeat: no-repeat;
    padding: 30px;
  }


</style>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title img-logo" style="border: 0;"> 
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <!-- <img src="../img/gb.jpg" alt="..." class="img-circle profile_img"> -->
              </div>
              <!-- <div class="profile_info">
                <span>Welcome,</span>
                <h2>Admin Bengkel</h2>
              </div> -->
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <!-- <h3>General</h3> -->
                <ul class="nav side-menu">
                  <li><a href="index.php"><i class="fa fa-list-alt"></i>Data Aduan</a></li>

                  <li><a href="aset.php"><i class="fa fa-th-large"></i>Data Aset</a></li>
                  
                  <li><a href="unit_kerja.php"><i class="fa fa-building"></i>Data Unit Kerja</a></li>

                  <li><a href="Histori_mutasi.php"><i class="fa fa-table"></i>Mutasi Aset</a></li>
                  
                   <li><a href="pemeliharaan.php"><i class="fa fa-cog"></i>Pemeliharaan</a></li>

                  <li><a href="data_admin.php"><i class="fa fa-user"></i>Data Admin</a></li>
    
                </ul>
              </div>
              

            </div>
            
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
    
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div> 
               <ul class="nav navbar-nav navbar-right">
               <li class="">

                  <a href="logout.php" class="button" style="color: #FFFFFF;"><i class="fa fa-power-off"></i>&nbsp;<b>Logout</b></a>
                  
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->