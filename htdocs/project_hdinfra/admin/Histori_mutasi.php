

<?php 
require_once ('include/header.php');

include "../koneksi.php";


?>

<style type="text/css">
  .search{
    width: 200px;
  }

</style>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3><b>Mutasi Aset (History)</b></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">                  
                  <div class="x_content">

                    <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                          <!-- Buat sebuah textbox dan beri id keyword -->
                          <input type="text" class="form-control" placeholder="Pencarian..." id="keyword">
                          
                          <span class="input-group-btn">
                            <!-- Buat sebuah tombol search dan beri id btn-search -->
                            <button class="btn btn-primary" type="button" id="btn-search">SEARCH</button>
                            <a href="" class="btn btn-warning">RESET</a>
                          </span>
                        </div> 
                      </div>
                      <!-- float-right -->
                      <div class="col-md-10 float-right">
                        <div class="form-group">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#input_hm" data-id="<?php echo $idpppk; ?>">&plus; Mutasi</button> 
                        </div>
                      </div>
                    </div>


                    <div id="view"><?php include "include/table-hm.php"; ?></div>
                    


                    <form action="report.php"  method="post">
                      <select class="bg-info" name="by">
                        <option value="1">Report All</option>
                        <?php
                           $tampil3 = $conn->query("SELECT DISTINCT jenis FROM tb_aset GROUP BY no_aset ASC");
                           while ($row3 = mysqli_fetch_array($tampil3)){
                           ?>             
                           <option value="<?php echo $row3['jenis'];?>">Report by <?php echo $row3['jenis'];?></option>';
                        <?php }?>
        
                      </select>
                      <br>
                      <br>
              <button type="submit" value="pdf" name="pdf" class="btn btn-success"><i class="fa fa-file-o"></i>&nbsp;Buat Laporan</button>
              <button type="submit" value="exl" name="exl" class="btn btn-success"><i class="fa fa-file"></i>&nbsp;Export to Excel</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        
        <!-- /footer content -->
      </div>
    </div>





    <div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <b >Anda yakin ingin menghapus data ini?</b><br><br>
                    <a class="btn btn-danger btn-ok"> Hapus</a>
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
                </div>
            </div>
        </div>
    </div>                
  

<div class="modal fade" id="input_hm" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Mutasi</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="fetched-data"></div>
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button> -->
                </div>
            </div>
        </div>
</div>
<!-- 
<div class="modal fade" id="edit_uk" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Unit Kerja</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="fetched-data"></div>
                </div>
                <div class="modal-footer">
                   </div>
            </div>
        </div>
</div>
 -->

 <script src="asset_admin/vendors/jquery/dist/jquery.min.js"></script>
<script src="include/ajax-hm.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="asset_admin/build/js/custom.min.js"></script>
    <script>
function searchTable() {
    var input;
    var saring;
    var status; 
    var tbody; 
    var tr; 
    var td;
    var i; 
    var j;
    input = document.getElementById("input");
    saring = input.value.toUpperCase();
    tbody = document.getElementsByTagName("tbody")[0];;
    tr = tbody.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td");
        for (j = 0; j < td.length; j++) {
            if (td[j].innerHTML.toUpperCase().indexOf(saring) > -1) {
                status = true;
            }
        }
        if (status) {
            tr[i].style.display = "";
            status = false;
        } else {
            tr[i].style.display = "none";
        }
    }
}

$(document).ready(function() {
        $('#konfirmasi_hapus').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });
    });

    $(document).ready(function(){
        $('#input_hm').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            //menggunakan fungsi ajax untuk pengambilan data
             // alert(rowid);
            $.ajax({
                type : 'post',
                // url : 'ajax_keluhan.php',
                url : 'ajax_mutasi.php',
                data :  'rowid='+ rowid,
                success : function(data){
                
                $('.fetched-data').html(data);//menampilkan data ke dalam modal
                }
            });
         });
    });

    $(document).ready(function(){
        $('#edit_uk').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            //menggunakan fungsi ajax untuk pengambilan data
             // alert(rowid);
            $.ajax({
                type : 'post',
                // url : 'ajax_keluhan.php',
                url : 'ajax_edituk.php',
                data :  'rowid='+ rowid,
                success : function(data){
                
                $('.fetched-data').html(data);//menampilkan data ke dalam modal
                }
            });
         });
    });
</script>
<?php
include 'include/footer.php';
?>
  </body>
</html>