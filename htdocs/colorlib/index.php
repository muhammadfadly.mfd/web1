<?php

function create_random($length)
{
    $data = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $string = '';
    for($i = 0; $i < $length; $i++) 
    {
        $pos = rand(0, strlen($data)-1);
        $string .= $data[$pos];
    }
    return $string;
}

?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Heldesk Infrastruktur">
    <meta name="author" content="Fadhli Dzil Ikram">
    <meta name="keywords" content="Heldesk Infrastruktur">

    <!-- Title Page-->
    <title>Heldesk Infrastruktur</title>

    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
        rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-blue p-t-100 p-b-100 font-robo">
        <div class="wrapper wrapper--w680">
            <div class="card card-1">
                <div class="card-heading"></div>
                <div class="card-body">
                    <h2 class="title">Helpdesk Infrastruktur - IT Services </h2>
                    <form method="post" action="proses_register.php">


                        <div class="input-group">
                            <label for="no_pppk">No. Tiket</label>
                            <input required="" name="no_pppk" type="text" class="input--style-1" id="nopppk"
                                value="<?php echo  $num = create_random(5);?>" readonly>
                        </div>


                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label for="no_pppk">Tipe Aduan</label>
                                    <div class="rs-select2 js-select-simple select--no-search">
                                        <select name="gender">
                                            <option disabled="disabled" selected="selected">--Pilih Tipe Aduan Anda--
                                            </option>
                                            <option>Perbaikan PC/Hardware</option>
                                            <option>Perbaikan Jaringan</option>
                                            <option>Remote Support</option>
                                            <option>Video Conference</option>
                                            <option>Lainnya</option>
                                        </select>
                                        <div class="select-dropdown"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label for="no_pppk">Unit Kerja Bagian</label>
                                    <div class="rs-select2 js-select-simple ">
                                        <select name="gender">
                                            <option disabled="disabled" selected="selected">--Pilih Tipe Aduan Anda--
                                            </option>
                                            <option>Perbaikan PC/Hardware</option>
                                            <option>Perbaikan Jaringan</option>
                                            <option>Remote Support</option>
                                            <option>Video Conference</option>
                                            <option>Lainnya</option>
                                        </select>
                                        <div class="select-dropdown"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="input-group">
                            <label for="no_pppk">Penghubung</label>
                            
                                <input class="input--style-1" type="text" placeholder="Masukkan Nama Anda"
                                    name="res_code">
                            
                        </div>

                        <div class="input-group">
                            <label for="no_pppk">Telepon/Ext</label>
                            
                                <input class="input--style-1" type="text" placeholder="Tambahkan No.Telp/Ext"
                                    name="res_code">
                            
                        </div>

                        <div class="input-group">
                            <label for="no_pppk">Email Penghubung</label>
                            <input class="input--style-1" type="email" placeholder="Masukkan Email Anda" 
                            name="name">
                        </div>

                        <div class="input-group">
                            <label for="no_pppk">Email Atasan</label>
                            <input class="input--style-1" type="email" placeholder="Masukkan Email Atasan Anda"
                                name="name">
                        </div>




                        <div class="p-t-20">
                            <button class="btn btn--radius btn--green" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js/global.js"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->