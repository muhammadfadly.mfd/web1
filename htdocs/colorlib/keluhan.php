<?php
session_start();
require'koneksi.php';
$idpppk = $_GET['id'];
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Input data keluhan</title>
    <!-- Bootstrap CSS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- <link rel="stylesheet" type="text/css" href="style.css"> -->
    <link rel="stylesheet" type="text/css" href="costum.css">
    <link href="font/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
   
    
  </head>
  <body>



<section class="form_pppk" id="form_pppk">
 <div class="container">
  <div class="row">
  <div class="col-sm-12 text-center ">
  <h3 class="text-center"> <b> <font color="000080">  </font> </b> </h3>
  <br>
  </div>
  </div>

  <div class="row">
    <!-- <div class="col-sm-5 img-pusri2">
     

      <img src="img/logo_bkl.png" class="pusri">
      <p><b>Your Satisfaction is Our Commitment "CRS Dept. TI"</b> <br><br>contact : 3208</p>
      
    </div> -->
    <!--  -->  
    <div class="col-sm-10 offset-sm-1 daftar">
    <div class="scheduleTab">
    
   

    <div class="tab-content">
      <div id="day1" class="tab-pane active">
         <div class="head-form">
          <p><b>Isi Data Keluhan Anda !</b></p>      
    </div>
  <div class="isiform">
  <!-- <form method="post" action="proses_register.php"> -->
              <div class="form-group">

                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#input_keluhan" data-id="<?php echo $idpppk; ?>">&plus; Tambah Keluhan</button> 
          
              </div>
              
              <!-- <div id="data"></div> -->
              
<?php
// session_start();
// require'koneksi.php';
// $idpppk = $_GET['id'];
?>
<hr>
<!-- id='here' -->
<div id="here">
<table class="table table-bordered table-dark" id="sampleTable">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>No. Tiket</th>
                    <th>No. Aset</th>
                    <th>Spesifikasi</th>
                    <th>Jenis</th>
                    <th>Keluhan</th>
                    <th>Hapus</th>
                  </tr>
                </thead>
                <tbody>

<?php
    $sql1 = "SELECT * FROM tb_detail join tb_aset on tb_detail.no_aset=tb_aset.no_aset where no_pppk = '$idpppk' ORDER BY tb_detail.no_aset ASC";
    $query2 = mysqli_query($conn, $sql1);
    $no   = 1;
    while ( $row = mysqli_fetch_array($query2))
    {
      echo ' <tr class="odd gradeX">
          <td>'.$no.'</td>
          <td>'.$row['no_pppk'].'</td>
          <td>'.$row['no_aset'].'</td>
          <td>'.$row['spesifikasi'].'</td>
          <td>'.$row['jenis'].'</td>
          <td>'.$row['keluhan'].'</td>
          <td align="center">
          <a data-href="hapus.php?idhps='.$row['id_detail'].'&idppk='.$row['no_pppk'].'&idaset='.$row['no_aset'].'" data-toggle="modal" data-target="#konfirmasi_hapus"><button type="button" class="btn btn-warning btn-circle"><i class="fa fa-times"></i>
          </button></a></td></tr>
      '; 
      $no++;       
    }
?>   

                  
  <script type="text/javascript">
    //Hapus Data
    $(document).ready(function() {
        $('#konfirmasi_hapus').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });
    });
  </script>                           
                </tbody>
              </table>
              <br>
              <!-- <a class="btn btn-info" target="_blank" href="report.php?id=<?php echo $idpppk; ?>">Print</a> -->
   

     <br><br>
     </div>
              <!-- <div id="tabel"></div> -->
     <!-- captcha -->


     <div>
      <label for="keluhan">Captcha</label>
      <img src="chapta/captcha.php" alt="gambar" />
      <a id="captcha_reload" href="keluhan.php?id=<?php echo $idpppk; ?> & #captcha_reload"> <i class="fa fa-refresh"></i> Reload</a>
     </div>

     <?php 
    if(isset($_GET['pesan'])){
      if($_GET['pesan'] == "salah"){

        echo "<br><div class='alert alert-warning alert-dismissible fade show' role='alert'>
  <strong>Captcha tidak sesuai!</strong> Input captcha sesuai gambar diatas.
  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
    <span aria-hidden='true'>&times;</span>
  </button>
</div>";
      }
    }
    ?>
<form action="report.php" method="post">
     <div>
     <label >Enter the code from the image here:</label>
     <input hidden="" type="text" class="form-control" value="<?php echo $idpppk; ?>" name="idpppk">
     
     <input required="" type="text" class="form-control" id="captinput" name="nilaiCaptcha" placeholder="Masukan kode pada gambar di atas" autocomplete="off">
     </div>

<br>
     
     <div class="form-group">
     <button type="submit" class="btn btn-success" name="add">Lanjutkan >></button>
     </div>
      </form> 
</div>
 </section>


<div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <b >Anda yakin ingin menghapus data ini?</b><br><br>
                    <a class="btn btn-danger btn-ok"> Hapus</a>
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
                </div>
            </div>
        </div>
    </div>                
  

<div class="modal fade" id="input_keluhan" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Input keluhan</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="fetched-data"></div>
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button> -->
                </div>
            </div>
        </div>
</div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="asset/jQuary-3.3.1/jquery-3.3.1.min.js"></script>
           
    <script>
        $("#unit_kerja").change(function(){
            var id = $("#unit_kerja").val();
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "ajax_lokasi.php?id="+id,
                success: function(msg){
                     // alert(msg);
                    if(msg == ''){
                        alert('Tidak ada data');
                    }
                    else{
                        $("#onajax").html(msg);
                    }
                }
            });
        });

        $("#captinput").change(function(){
            var code = $("#captinput").val();
            
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "chapta/periksa_captcha.php?nilaiCaptcha="+code,
                success: function(msg){
                    // alert(msg);
                    if(msg == ''){
                        alert('Tidak ada data');
                    }
                    else{
                        $("#respon").html(msg);
                    }
                }
            });
        });

    

    $(document).ready(function(){
        $('#input_keluhan').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            //menggunakan fungsi ajax untuk pengambilan data
             // alert(rowid);
            $.ajax({
                type : 'post',
                // url : 'ajax_keluhan.php',
                url : 'ajax_keluhan.php?idppk='+rowid,
                data :  'rowid='+ rowid,
                success : function(data){
                
                $('.fetched-data').html(data);//menampilkan data ke dalam modal
                }
            });
         });
    });

    var auto_refresh = setInterval(
    function()
    {
     var noppk = $("#nopppk").val();  
    $('#data').load('ajax_tabel.php?id='+noppk);
    }, );
     </script>

<script type="text/javascript">
$(".chosen").chosen();
</script>
 
  </body>
</html>


