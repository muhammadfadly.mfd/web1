<?php
class ControllerDesignthemesetting extends Controller {
	private $error = array();

	public function edit() {
		$this->load->language('design/themesetting');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('design/themesetting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_design_themesetting->editthemesetting($this->request->get['setting_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('design/themesetting/edit&setting_id=1', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}



	

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['setting_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_facebook'] = $this->language->get('entry_facebook');
		$data['entry_twitter'] = $this->language->get('entry_twitter');
		$data['entry_googleplus'] = $this->language->get('entry_googleplus');
		$data['entry_pintrace'] = $this->language->get('entry_pintrace');
		$data['entry_linkedin'] = $this->language->get('entry_linkedin');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_banner_add'] = $this->language->get('button_banner_add');
		$data['button_remove'] = $this->language->get('button_remove');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('design/themesetting', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['setting_id'])) {
			$data['action'] = $this->url->link('design/themesetting/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('design/themesetting/edit', 'token=' . $this->session->data['token'] . '&setting_id=' . $this->request->get['setting_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('design/themesetting', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['setting_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$setting_info = $this->model_design_themesetting->getsetting($this->request->get['setting_id']);
		}

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($setting_info)) {
			$data['name'] = $setting_info['name'];
		} else {
			$data['name'] = '';
		}
		
		if (isset($this->request->post['facebook'])) {
			$data['facebook'] = $this->request->post['facebook'];
		} elseif (!empty($setting_info)) {
			$data['facebook'] = $setting_info['facebook'];
		} else {
			$data['facebook'] = '';
		}
		if (isset($this->request->post['twitter'])) {
			$data['twitter'] = $this->request->post['twitter'];
		} elseif (!empty($setting_info)) {
			$data['twitter'] = $setting_info['twitter'];
		} else {
			$data['twitter'] = '';
		}
		
		if (isset($this->request->post['googleplus'])) {
			$data['googleplus'] = $this->request->post['googleplus'];
		} elseif (!empty($setting_info)) {
			$data['googleplus'] = $setting_info['googleplus'];
		} else {
			$data['googleplus'] = '';
		}
		
		
		
			if (isset($this->request->post['pintrace'])) {
			$data['pintrace'] = $this->request->post['pintrace'];
		} elseif (!empty($setting_info)) {
			$data['pintrace'] = $setting_info['pintrace'];
		} else {
			$data['pintrace'] = '';
		}
		
			if (isset($this->request->post['linkedin'])) {
			$data['linkedin'] = $this->request->post['linkedin'];
		} elseif (!empty($setting_info)) {
			$data['linkedin'] = $setting_info['linkedin'];
		} else {
			$data['linkedin'] = '';
		}
		
		

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($setting_info)) {
			$data['status'] = $setting_info['status'];
		} else {
			$data['status'] = true;
		}

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		$this->load->model('tool/image');

		

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('design/themesetting_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'design/themesetting')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		if (isset($this->request->post['banner_image'])) {
			foreach ($this->request->post['banner_image'] as $language_id => $value) {
				foreach ($value as $banner_image_id => $banner_image) {
					if ((utf8_strlen($banner_image['title']) < 2) || (utf8_strlen($banner_image['title']) > 64)) {
						$this->error['banner_image'][$language_id][$banner_image_id] = $this->language->get('error_title');
					}
				}
			}
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'design/themesetting')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}