<?php 
$dbhost = 'localhost';
$dbname = 'fadli';
$dbuser = 'root';
$dbpass = '';

try{
    $conn = new PDO("mysql:host={$dbhost};dbname={$dbname}",$dbuser, $dbpass);
    $conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
}catch(PDOException $ex){
    die($ex->getMessage());
}
$stmt = $conn->prepare("SELECT * FROM tbl_amonia WHERE waktu >= CURDATE()");
$stmt->execute();
$json=[];
while($row= $stmt->fetch(PDO::FETCH_ASSOC)){
    extract($row);
    $json[]= [(int)$nilai];
}
echo json_encode($json);
?>
