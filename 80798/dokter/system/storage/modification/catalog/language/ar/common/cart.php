<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com

// Text

				$_['text_items2']  = '%s منتجات';
				$_['text_items3']  = '%s';
				$_['text_shopping_cart'] = 'خريطة';
				$_['text_wishlist'] = 'مفضلة';
				$_['text_delete'] = 'حذف';
				$_['text_cancel'] = 'إلغاء';
				
$_['text_items']    = '%s منتجات - %s';
$_['text_empty']    = 'سلة الشراء فارغة !';
$_['text_cart']     = 'معاينة السلة';
$_['text_checkout'] = 'إنهاء الطلب';
$_['text_recurring']  = 'ملف الدفع';