<?php 
 
 
//ini adalah require yang dibutuhkan cukup merequire file pertama di PHP Excel. 
//sesuaikan dengan Path Milik anda
require_once 'asset_admin/PHPExcel-1.8/Classes/PHPExcel.php';
	// require_once "/var/www/html/teambaru/smis-libs-out/php-excel/PHPExcel.php"; 
/*start - BLOCK PROPERTIES FILE EXCEL*/
	$file = new PHPExcel ();
	$file->getProperties ()->setCreator ( "Sistem Informasi Bengkel Pusri" );
	$file->getProperties ()->setLastModifiedBy ( "Admin" );
	$file->getProperties ()->setTitle ( "Data Aset" );
	$file->getProperties ()->setSubject ( "aset" );
	$file->getProperties ()->setDescription ( "Data aset Pusri" );
	$file->getProperties ()->setKeywords ( "Aset Pusri" );
	$file->getProperties ()->setCategory ( "Aset" );
/*end - BLOCK PROPERTIES FILE EXCEL*/
 
/*start - BLOCK SETUP SHEET*/
	$file->createSheet ( NULL,0);
	$file->setActiveSheetIndex ( 0 );
	$sheet = $file->getActiveSheet ( 0 );
	//memberikan title pada sheet
	$sheet->setTitle ("Data Aset Pusri");
/*end - BLOCK SETUP SHEET*/
 
/*start - BLOCK HEADER*/
	$sheet	->setCellValue ( "A1", "No." )
		->setCellValue ( "B1", "No. Aset" )
		->setCellValue ( "C1", "Serial Number" )
		->setCellValue ( "D1", "Spesifikasi" )
		->setCellValue ( "E1", "Jenis" );
/*end - BLOCK HEADER*/
 
/* start - BLOCK MEMASUKAN DATABASE*/
	//ganti dengan database anda
include "../koneksi.php";
	// $link = mysqli_connect("localhost", "root", "123456", "goblooge"); 
	$sql = mysqli_query($conn, "SELECT * FROM tb_aset");
	$nomor=1;
	$no=0;
	while($row=mysqli_fetch_array($sql)){
		$nomor++; $no++;
		$sheet	->setCellValue ( "A".$nomor, $no )
			->setCellValue ( "B".$nomor, $row["no_aset"] )
			->setCellValue ( "C".$nomor, $row["serial_number"] )
			->setCellValue ( "D".$nomor, $row["spesifikasi"] )
			->setCellValue ( "E".$nomor, $row["jenis"] );
	}
/* end - BLOCK MEMASUKAN DATABASE*/
 
/* start - BLOCK MEMBUAT LINK DOWNLOAD*/
	header ( 'Content-Type: application/vnd.ms-excel' );
	//namanya adalah keluarga.xls
	header ( 'Content-Disposition: attachment;filename="DATA ASET.xls"' ); 
	header ( 'Cache-Control: max-age=0' );
	$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
	$writer->save ( 'php://output' );
/* start - BLOCK MEMBUAT LINK DOWNLOAD*/
 
?>