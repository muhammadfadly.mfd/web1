<?php
require'../koneksi.php';
$idpppk= $_GET['idpppk'];
$aset= $_GET['aset'];
?>
<form method="post" action="proses/proses-diagnosa.php">
  <div class="form-group">
    <label for="no_pppk">No. Tiket</label>
    <input required=""  name="no_pppk" type="text" class="form-control input-sm" id="no_pppk" value="<?php echo $idpppk; ?>" readonly>
  </div>
  
  <div class="form-group">
    <label for="no_aset">No. Aset</label>
    <input required=""  name="no_aset" type="text" class="form-control input-sm" id="no_aset" value="<?php echo $aset; ?>" readonly>
  </div>

  <div class="form-group">
    <label for="diagnosa">Diagnosa</label>
    <textarea class="form-control" name="diagnosa" rows="3"></textarea>
  </div>

  <button type="submit" class="btn btn-success" name="add">Simpan</button>
</form>

<script>
function myFunction() {
  document.getElementById("frm1").submit();
}
</script>