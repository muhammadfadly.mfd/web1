

<?php 
require_once ('include/header.php');
$idn = $_GET['id_pppk'];
?>

<style type="text/css">
  .search{
    width: 200px;

  }
  .bg-al{
    background-color: #2d7cf1de;
    color: #ffffff;
  }

</style>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
               <div class="page-title">
                  <div class="alert alert-info" role="alert">

                  <h4 class="mb-0">Tindak Aduan dengan No. Tiket : "<b><?php echo $idn ?></b>" </h4>
                </div>

              <div class="title_left">
                <h3>Akan Ditindak <small></small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">                  
                  <div class="x_content">

                   

                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th><center>No. </center></th>
                          <th><center>No. Aset</center></th>
                          <th><center>Spesifikasi</center></th>
                          <th><center>Jenis</center></th>
                          <!-- <th><center>Status</center></th> -->
                          <th><center>Diagnosa</center></th>
                          <th><center>Input</center></th>
                          <th><center>Status 1</center></th>
                          <th><center>Status 2</center></th>
                        </tr>
                      </thead>
                     
                      <tbody>
                         <?php 
                          include "../koneksi.php";
                              $id = $_GET['id_pppk'];

                              $sql = "SELECT DISTINCT tb_detail.no_aset,tb_aset.spesifikasi,tb_aset.jenis,tb_detail.diagnosa FROM tb_detail left join tb_tindakan on tb_detail.no_pppk = tb_tindakan.no_pppk join tb_aset on tb_aset.no_aset=tb_detail.no_aset where tb_detail.no_pppk='$id' and status='Diagnosa'";
                              $query = mysqli_query($conn, $sql);
                              $no=0;      
                              while ($hasil=mysqli_fetch_array($query)) {
                              $no=$no+1;

                      ?>
                        <tr>

                          <td><center><?= "$no"; ?></center></td>
                          <td><center><?= $hasil['no_aset']; ?></center></td>
                          <!-- <td><center><?= $hasil[5]; ?></center></td> -->
                          <td><center><?= $hasil['spesifikasi']; ?></center></td>
                          <td><center><?= $hasil['jenis']; ?></center></td>
                          <td><center><?= $hasil['diagnosa']; ?></center></td>                         
                          <td align="center">

                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#input_diagnosa" data-id="<?php echo $hasil['no_aset']; ?>"><i class="fa fa-pencil"></i><br>Tindakan</button>

                          </td>
                          <td align="center">

                          <button type="button" class="btn btn-success" data-toggle="modal" data-target="#alasan" data-id="<?php echo $hasil['no_aset']; ?>"><i class="fa fa-pencil"></i><br>Diambil</button>

                          </td>
                          <td align="center">

                          <a href="gettunggu.php?id_pppk=<?php echo $id ?> &id_aset=<?php echo $hasil['no_aset']; ?>"><button type="button" class="btn btn-warning"><i class="fa fa-check"></i><br>Tunggu</button></a>

                          </td>
                  
                          
                        <?php } ?>
                      </tbody>
                    
                    </table>					
					
                  </div>
                </div>

<?php
 $sqlkembali = "SELECT * from tb_detail join tb_aset on tb_detail.no_aset=tb_aset.no_aset where no_pppk='$id' and status='Diambil'";
  $querykembali = mysqli_query($conn, $sqlkembali);     
  $no=1; 
$hitung= mysqli_num_rows($querykembali);
if($hitung > 0 ){
?>

<div class="alert alert-warning" role="alert">
  <h4 class="alert-heading">*Aset Telah Dikembalikan/Diambil Oleh Penghubung!</h4>
  <!-- <p>Terdapat Aset yang telah dikembalikan atau di ambil oleh pemilik atau yang membuat pengaduan.  </p>
  <hr>
  <p class="mb-0">Berikut data aset yang telah dikembalikan/diambil</p> -->
<br>
  <table id="datatable-responsive" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr style="background-color: #2F4F4F;">
                          <th><center>No. </center></th>
                          <th><center>No. Aset</center></th>
                          <th><center>Spesifikasi</center></th>
                          <th><center>Jenis</center></th>
                          <th><center>Diambil Oleh</center></th>
                          <th><center>Alasan</center></th>
                        </tr>
                      </thead>
                     
                      <tbody>
                         <?php  
                              while ($hasil=mysqli_fetch_array($querykembali)) {
                          ?>
                          <tr>

                          <td><center><?php echo $no; ?></center></td>
                          <td><center><?= $hasil['no_aset']; ?></center></td>
                          <td><center><?= $hasil['spesifikasi']; ?></center></td>
                          <td><center><?= $hasil['jenis']; ?></center></td>
                          <td><center><?= $hasil['diambil']; ?>  </center></td>
                          <td><center><?= $hasil['alasan']; ?>  </center></td>
                        <?php $no++;
                         } ?>
                      </tbody>
                    
                    </table>

</div>
<?php
}
?>
<!-- <br> -->
<?php
 $sqlkembali = "SELECT * from tb_detail join tb_aset on tb_detail.no_aset=tb_aset.no_aset where no_pppk='$id' and status='Menunggu Sparepart'";
  $querykembali = mysqli_query($conn, $sqlkembali);     
  $no=1; 
$hitung= mysqli_num_rows($querykembali);
if($hitung > 0 ){
?>

<div class="alert alert-info" role="alert">
  <h4 class="alert-heading">*Aset belum ditindak, dan dalam masa menunggu ketersediaan sparepart!</h4>
<br>
  <table id="datatable-responsive" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr style="background-color: #FF7F00;">
                          <th><center>No. </center></th>
                          <th><center>No. Aset</center></th>
                          <th><center>Spesifikasi</center></th>
                          <th><center>Jenis</center></th>
                          <!-- <th><center>Diambil Oleh</center></th> -->
                          <th><center>Input</center></th>
                        </tr>
                      </thead>
                     
                      <tbody>
                         <?php  
                              while ($hasil=mysqli_fetch_array($querykembali)) {
                          ?>
                          <tr>

                          <td><center><?php echo $no; ?></center></td>
                          <td><center><?= $hasil['no_aset']; ?></center></td>
                          <td><center><?= $hasil['spesifikasi']; ?></center></td>
                          <td><center><?= $hasil['jenis']; ?></center></td>
                          <td align="center">
                          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#input_diagnosa" data-id="<?php echo $hasil['no_aset']; ?>"><i class="fa fa-pencil"></i><br>Tindakan</button>

                          </td>
                          
                        <?php $no++;
                         } ?>
                      </tbody>
                    
                    </table>

</div>
<?php
}
?>


                <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Telah Ditindak <small></small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">                  
                  <div class="x_content">

                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th><center>No. </center></th>
                          <th><center>No. Aset</center></th>
                          <th><center>Spesifikasi</center></th>
                          <th><center>Jenis</center></th>
                          <!-- <th><center>Status</center></th> -->
                          <th><center>Input</center></th>
                          <th><center>Tindakan</center></th>
                          <th><center>Status</center></th>
                        </tr>
                      </thead>
                     
                      <tbody>
                         <?php  
 // and diambil IS null
                            $sql2 = "SELECT DISTINCT tb_detail.no_aset, status, spesifikasi, jenis,diambil FROM tb_detail join tb_tindakan on tb_detail.no_pppk = tb_tindakan.no_pppk join tb_aset on tb_aset.no_aset=tb_detail.no_aset where tb_tindakan.no_pppk='$id' and status='Finish' and diambil is null";
                              $query2 = mysqli_query($conn, $sql2);     
                              $no=1; 
                              while ($hasil=mysqli_fetch_array($query2)) {
                              
                          ?>
                          <tr>

                          <td><center><?php echo $no; ?></center></td>
                          <td><center><?= $hasil['no_aset']; ?></center></td>
                          <td><center><?= $hasil['spesifikasi']; ?></center></td>
                          <td><center><?= $hasil['jenis']; ?></center></td>
                          <!-- <td><center><?= $hasil['status']; ?></center></td>                          -->
                          <td align="center">
                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#input_diagnosa" data-id="<?php echo $hasil['no_aset']; ?>"><i class="fa fa-plus"></i><br>Tindakan</button>
                          </td>
                          <td align="center">
                          <button type="button" class="btn btn-info" data-toggle="modal" data-target="#lihat_tindakan" data-id="<?php echo $hasil['no_aset']; ?>"><i class="fa fa-eye"></i><br>Lihat</button>
                          </td>
                          <td align="center">
                           <button type="button" class="btn btn-success" data-toggle="modal" data-target="#diambil" data-id="<?php echo $hasil['no_aset']; ?>"><i class="fa fa-pencil"></i><br>Diambil</button>
                          </td>
                        <?php $no++;
                         } ?>
                      </tbody>
                    
                    </table>

<div class="alert alert-info" role="alert">
  Total Biaya Keseluruhan Aduan dengan No. Tiket "<b><?php echo $idn ?></b>" = 
  <?php 
  $sqlkembali = "SELECT sum(biaya) as amoun from tb_tindakan where no_pppk='$id'";
  $querykembali = mysqli_query($conn, $sqlkembali); 
  foreach ($querykembali as $total);  
  echo "Rp. ";  
  echo number_format($total['amoun']);  
  echo ",-";
  ?>
</div>

                    <a href="print.php?idpppk=<?php echo $idn; ?>" class="btn btn-primary btn-md active"><i class="fa fa-print"></i> Cetak Form</a>
                    <!-- <META HTTP-EQUIV="REFRESH" CONTENT="1; URL='admin/index.php'"> -->
                    

          
                  </div>

                </div>
              </div>  
              

              </div>  
              
<?php
 $sqlkembali = "SELECT * from tb_detail join tb_aset on tb_detail.no_aset=tb_aset.no_aset where no_pppk='$id' and status='finish' and diambil != ''";
  $querykembali = mysqli_query($conn, $sqlkembali);     
  $no=1; 
$hitung= mysqli_num_rows($querykembali);
if($hitung > 0 ){
?>

<div class="alert alert-success" role="alert">
  <h4 class="alert-heading">*Aset Telah Selesai Di Perbaiki & Telah Dikembalikan/Diambil oleh penghubung!</h4>
  <!-- <p>Terdapat Aset yang telah dikembalikan atau di ambil oleh pemilik atau yang membuat pengaduan.  </p>
  <hr>
  <p class="mb-0">Berikut data aset yang telah dikembalikan/diambil</p> -->
<br>
  <table id="datatable-responsive" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr style="background-color: #1E90FF">
                          <th><center>No. </center></th>
                          <th><center>No. Aset</center></th>
                          <th><center>Spesifikasi</center></th>
                          <th><center>Jenis</center></th>
                          <th><center>Diambil Oleh</center></th>
                          <th><center>Alasan</center></th>
                        </tr>
                      </thead>
                     
                      <tbody>
                         <?php  
                              while ($hasil=mysqli_fetch_array($querykembali)) {
                          ?>
                          <tr>

                          <td><center><?php echo $no; ?></center></td>
                          <td><center><?= $hasil['no_aset']; ?></center></td>
                          <td><center><?= $hasil['spesifikasi']; ?></center></td>
                          <td><center><?= $hasil['jenis']; ?></center></td>
                          <td><center><?= $hasil['diambil']; ?>  </center></td>
                          <td><center><?= $hasil['alasan']; ?>  </center></td>
                        <?php $no++;
                         } ?>
                      </tbody>
                    
                    </table>

</div>
<?php
}
?>

            </div>
          </div>

 <!-- modal input diagnosa -->
                    <div class="modal fade" id="input_diagnosa" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Input Tindakan</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="fetched-data"></div>
                                    </div>
                                    <div class="modal-footer">
                                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button> -->
                                    </div>
                                </div>
                            </div>
                    </div>

                    <div class="modal fade" id="alasan" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Diambil Sebelum di Tindak</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="fetched-data"></div>
                                    </div>
                                    <div class="modal-footer">
                                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button> -->
                                    </div>
                                </div>
                            </div>
                    </div>

                    <div class="modal fade" id="diambil" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Diambil</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="fetched-data"></div>
                                    </div>
                                    <div class="modal-footer">
                                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button> -->
                                    </div>
                                </div>
                            </div>
                    </div>

                    <div class="modal fade" id="lihat_tindakan" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Tindakan</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="fetched-data"></div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </div>
                    </div>

                    <div class="modal fade" id="edit_tindakan" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Edit Tindakan</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="fetched-data"></div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </div>
                    </div>
         



        </div>
        <!-- /page content -->

        <!-- footer content -->
        
        <!-- /footer content -->
      </div>
    </div>


 


    <!-- jQuery -->
    <script src="asset_admin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
   
    <!-- Custom Theme Scripts -->
    <!-- <script src="asset_admin/build/js/custom.min.js"></script> -->
    <script>
function searchTable() {
    var input;
    var saring;
    var status; 
    var tbody; 
    var tr; 
    var td;
    var i; 
    var j;
    input = document.getElementById("input");
    saring = input.value.toUpperCase();
    tbody = document.getElementsByTagName("tbody")[0];;
    tr = tbody.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td");
        for (j = 0; j < td.length; j++) {
            if (td[j].innerHTML.toUpperCase().indexOf(saring) > -1) {
                status = true;
            }
        }
        if (status) {
            tr[i].style.display = "";
            status = false;
        } else {
            tr[i].style.display = "none";
        }
    }
}
</script>
    <script src="../asset/jQuary-3.3.1/jquery-3.3.1.min.js"></script>
    <script>
    $(document).ready(function(){
        $('#input_diagnosa').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            var val = "<?php echo $id ?>";
            //menggunakan fungsi ajax untuk pengambilan data
              // alert(rowid);
            $.ajax({
                type : 'post',
                // url : 'ajax_keluhan.php',
                url : 'ajax_tindakan.php?idpppk='+val+'&aset='+rowid,
                // data :  'rowid='+ rowid,
                success : function(data){
                
                $('.fetched-data').html(data);//menampilkan data ke dalam modal
                }
            });
         });
    });

    $(document).ready(function(){
        $('#lihat_tindakan').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            var val = "<?php echo $id ?>";
            //menggunakan fungsi ajax untuk pengambilan data
              // alert(rowid);
            $.ajax({
                type : 'post',
                // url : 'ajax_keluhan.php',
                url : 'ajax_lihat_tindakan.php?idpppk='+val+'&aset='+rowid,
                // data :  'rowid='+ rowid,
                success : function(data){
                
                $('.fetched-data').html(data);//menampilkan data ke dalam modal
                }
            });
         });
    });

    $(document).ready(function(){
        $('#edit_tindakan').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            var val = "<?php echo $id ?>";
            //menggunakan fungsi ajax untuk pengambilan data
              // alert(rowid);
            $.ajax({
                type : 'post',
                // url : 'ajax_keluhan.php',
                url : 'ajax_edit_tindakan.php?idpppk='+val+'&aset='+rowid,
                // data :  'rowid='+ rowid,
                success : function(data){
                
                $('.fetched-data').html(data);//menampilkan data ke dalam modal
                }
            });
         });
    });

     $(document).ready(function(){
        $('#alasan').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            var val = "<?php echo $id ?>";
            //menggunakan fungsi ajax untuk pengambilan data
              // alert(rowid);
            $.ajax({
                type : 'post',
                // url : 'ajax_keluhan.php',
                url : 'ajax_alasan.php?idpppk='+val+'&aset='+rowid,
                // data :  'rowid='+ rowid,
                success : function(data){
                
                $('.fetched-data').html(data);//menampilkan data ke dalam modal
                }
            });
         });
    });

     $(document).ready(function(){
        $('#diambil').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            var val = "<?php echo $id ?>";
            //menggunakan fungsi ajax untuk pengambilan data
              // alert(rowid);
            $.ajax({
                type : 'post',
                // url : 'ajax_keluhan.php',
                url : 'ajax_diambil.php?idpppk='+val+'&aset='+rowid,
                // data :  'rowid='+ rowid,
                success : function(data){
                
                $('.fetched-data').html(data);//menampilkan data ke dalam modal
                }
            });
         });
    });

    </script>

<?php
include 'include/footer.php';
?>
  </body>
</html>