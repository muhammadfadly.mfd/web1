<?php
require'../koneksi.php';
$idpppk= $_GET['idpppk'];
$aset= $_GET['aset'];
$sql = "SELECT DISTINCT tb_detail.no_aset, status, nama_sparepart, keterangan, biaya,id_tindakan FROM tb_detail join tb_tindakan on tb_detail.no_aset = tb_tindakan.no_aset where tb_detail.no_aset='$aset' and tb_tindakan.no_pppk='$idpppk' and status='Finish'";
$query = mysqli_query($conn, $sql);
$no=1;      
$total=0;
while ($hasil=mysqli_fetch_array($query)) {
?>
<div class="row">
  <div class="col-md-3"><B>Tindakan <?php echo $no; ?> </B><br></div>

  <div class="col-md-5"></div>
</div>
<div class="row">
  <div class="col-md-3">Sparepart</div>
  <div class="col-md-5">: <?php echo $hasil[2]; ?></div>
</div>
<div class="row">
  <div class="col-md-3">Keterangan</div>
  <div class="col-md-5">: <?php echo $hasil[3]; ?></div>
</div>
<div class="row">
  <div class="col-md-3">Biaya</div>
  <div class="col-md-5">: Rp. <?php echo number_format($hasil[4]);  $id=$hasil['id_tindakan']; ?>,-</div>
</div>
<div class="row">
  <div class="col-md-10"></div>
  <div class="col-md-1">
    <a href="edit-tindakan.php?idtindakan=<?php echo $id; ?>" class="btn btn-primary btn-md active" role="button" aria-pressed="true"><i class="fa fa-pencil-square-o"></i> Edit</a>
  </div>
</div>
<hr>

<?php
$total=$total+$hasil[4];
$no++;
}
echo "<div> Total Biaya : Rp. ";
echo number_format($total);
echo ",-</div>";
$cek = mysqli_num_rows($query);
if ($cek <= 0){
  echo "Data Kosong, atau belum ada tindakan apapun!";
}

?>
