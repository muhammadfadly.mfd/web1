<?php
require'../koneksi.php';
$aset= $_POST['rowid'];

$sql = "SELECT * FROM tb_aset where no_aset='$aset'";
$query = mysqli_query($conn, $sql);    
$hasil=mysqli_fetch_array($query);
?>
<form method="post" action="proses/updateaset.php">
  <div class="form-group">
    <label for="no_pppk">No. Aset</label>
    <input required=""  name="no_aset" type="text" class="form-control input-sm" value="<?php echo $aset; ?>" autocomplete="off" readonly>
  </div>

   <div class="form-group">
    <label for="serial_number">Serial Number</label>
    <input required=""  name="serial_number" type="text" class="form-control input-sm" value="<?php echo $hasil['serial_number']; ?>" autocomplete="off">
  </div>
  
  <div class="form-group">
    <label for="spesifikasi">Spesifikasi</label>
    <textarea class="form-control" name="spesifikasi" rows="3"><?php echo $hasil['spesifikasi']; ?></textarea>
  </div>

  <div class="form-group">
    <label for="Jenis">Jenis</label>
    <input required=""  name="jenis" type="text" class="form-control input-sm" value="<?php echo $hasil['jenis']; ?>" autocomplete="off">
  </div>

  <button type="submit" class="btn btn-success" name="add">Simpan</button>
</form>

<script>
function myFunction() {
  document.getElementById("frm1").submit();
}
</script>