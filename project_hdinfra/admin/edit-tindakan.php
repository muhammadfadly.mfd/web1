

<?php 
require_once ('include/header.php');

?>

<style type="text/css">
  .search{
    width: 200px;
  }

</style>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Edit Tindakan</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_panel">                  
                  <div class="x_content">

<?php
require'../koneksi.php';
// $idpppk= $_GET['idpppk'];
$id= $_GET['idtindakan'];
$sql = "SELECT * FROM tb_detail join tb_tindakan on tb_detail.no_aset = tb_tindakan.no_aset where id_tindakan=$id ";
$query = mysqli_query($conn, $sql);      
$hasil=mysqli_fetch_array($query);
?>
<form method="post" action="proses/proses-edit-tindakan.php">
  <div class="form-group">
    <label for="no_pppk">Id Tindakan</label>
    <input required=""  name="id" type="text" class="form-control input-sm" id="no_pppk" value="<?php echo $hasil['8']; ?>" readonly>
  </div>

  <div class="form-group">
    <label for="no_pppk">No. Tiket</label>
    <input required=""  name="no_pppk" type="text" class="form-control input-sm" id="no_pppk" value="<?php echo $hasil['9']; ?>" readonly>
  </div>
  
  <div class="form-group">
    <label for="no_aset">No. Aset</label>
    <input required=""  name="no_aset" type="text" class="form-control input-sm" id="no_pppk" value="<?php echo $hasil['10']; ?>" readonly>
  </div>

  <div class="form-group">
    <label for="Sparepart">Sparepart</label>
    <select class="form-control"  name="sparepart">
      <option value="<?php echo $hasil['11']; ?>"><?php echo $hasil['11']; ?></option>
      <?php
         $tampil3 = $conn->query("SELECT * FROM tb_sparepart ORDER BY id_sparepart ASC");
         while ($row3 = mysqli_fetch_array($tampil3)){
         ?>             
         <option value="<?php echo $row3['nama_sparepart'];?>"><?php echo $row3['nama_sparepart'];?></option>';
      <?php }?>
    </select>
  </div>

  <div class="form-group">
    <label for="keluhan">Keterangan</label>
    <textarea class="form-control" name="keterangan" rows="3"><?php echo $hasil['12']; ?></textarea>
  </div>

  <div class="form-group">
    <label for="no_aset">Biaya</label>
    <input required="" value="<?php echo $hasil['13']; ?>" name="biaya" type="text" class="form-control input-sm" autocomplete="off">
  </div>

  <button type="submit" class="btn btn-success" name="add">Simpan</button>
  <a href="tindakan.php?id_pppk=<?php echo $hasil['9']; ?>" class="btn btn-warning btn-md active" role="button" aria-pressed="true"><i class="fa fa-times"></i>&nbsp;Batal</a>
</form>

<script>
function myFunction() {
  document.getElementById("frm1").submit();
}
</script>
                  </div>
                </div>
              </div>
            </div>
          </div>

          



        </div>
        <!-- /page content -->

        <!-- footer content -->
        
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="asset_admin/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="asset_admin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="asset_admin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="asset_admin/vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="asset_admin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="asset_admin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="asset_admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="asset_admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="asset_admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="asset_admin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="asset_admin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="asset_admin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="asset_admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="asset_admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="asset_admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="asset_admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="asset_admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="asset_admin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="asset_admin/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="asset_admin/vendors/pdfmake/build/vfs_fonts.js"></script>

<?php
include 'include/footer.php';
?>
  </body>
</html>