<?php 
 
 
//ini adalah require yang dibutuhkan cukup merequire file pertama di PHP Excel. 
//sesuaikan dengan Path Milik anda
require_once 'asset_admin/PHPExcel-1.8/Classes/PHPExcel.php';
	// require_once "/var/www/html/teambaru/smis-libs-out/php-excel/PHPExcel.php"; 
/*start - BLOCK PROPERTIES FILE EXCEL*/
	$file = new PHPExcel ();
	$file->getProperties ()->setCreator ( "Sistem Informasi Bengkel Pusri" );
	$file->getProperties ()->setLastModifiedBy ( "Admin" );
	$file->getProperties ()->setTitle ( "Data Pemeliharaan" );
	$file->getProperties ()->setSubject ( "Pemeliharaan" );
	$file->getProperties ()->setDescription ( "Data Pemeliharaan aset Pusri" );
	$file->getProperties ()->setKeywords ( "Pemeliharaan" );
	$file->getProperties ()->setCategory ( "Pemeliharaan" );
/*end - BLOCK PROPERTIES FILE EXCEL*/
 
/*start - BLOCK SETUP SHEET*/
	$file->createSheet ( NULL,0);
	$file->setActiveSheetIndex ( 0 );
	$sheet = $file->getActiveSheet ( 0 );
	//memberikan title pada sheet
	$sheet->setTitle ("Data Pemeliharaan Pusri");
/*end - BLOCK SETUP SHEET*/
 
/*start - BLOCK HEADER*/
	$sheet	->setCellValue ( "A1", "No" )
		->setCellValue ( "B1", "No Tiket" )
		->setCellValue ( "C1", "No Aset" )
		->setCellValue ( "D1", "Jenis" )
		->setCellValue ( "E1", "Keluhan" )
		->setCellValue ( "F1", "Tindakan" )
		->setCellValue ( "G1", "Tanggal" );
/*end - BLOCK HEADER*/
 
/* start - BLOCK MEMASUKAN DATABASE*/
	//ganti dengan database anda
include "../koneksi.php";
	// $link = mysqli_connect("localhost", "root", "123456", "goblooge"); 
	$sql = mysqli_query($conn, "SELECT DISTINCT tb_aset.jenis,tb_detail.no_pppk,tb_detail.no_aset,tb_detail.keluhan,tb_master.tanggal,(SELECT GROUP_CONCAT(keterangan) from tb_tindakan where tb_tindakan.no_pppk = tb_detail.no_pppk and tb_tindakan.no_aset = tb_detail.no_aset GROUP BY tb_detail.no_aset ) as tindakan FROM tb_master JOIN tb_detail on tb_master.no_pppk=tb_detail.no_pppk join tb_tindakan on tb_master.no_pppk=tb_tindakan.no_pppk and tb_detail.no_aset=tb_tindakan.no_aset JOIN tb_aset on tb_aset.no_aset=tb_detail.no_aset");
	$nomor=1;
	$no=0;
	while($row=mysqli_fetch_array($sql)){
		$nomor++; $no++;
		$sheet	->setCellValue ( "A".$nomor, $no )
			->setCellValue ( "B".$nomor, $row["no_pppk"] )
			->setCellValue ( "C".$nomor, $row["no_aset"] )
			->setCellValue ( "D".$nomor, $row["jenis"] )
			->setCellValue ( "E".$nomor, $row["keluhan"] )
			->setCellValue ( "F".$nomor, $row["tindakan"] )
			->setCellValue ( "G".$nomor, date('d-m-Y H:i:s', strtotime($row['tanggal'])));
	}
/* end - BLOCK MEMASUKAN DATABASE*/
 
/* start - BLOCK MEMBUAT LINK DOWNLOAD*/
	header ( 'Content-Type: application/vnd.ms-excel' );
	//namanya adalah keluarga.xls
	header ( 'Content-Disposition: attachment;filename="Data Pemeliharaan Aset (History Perbaikan Aset).xls"' ); 
	header ( 'Cache-Control: max-age=0' );
	$writer = PHPExcel_IOFactory::createWriter ( $file, 'Excel5' );
	$writer->save ( 'php://output' );
/* start - BLOCK MEMBUAT LINK DOWNLOAD*/
 
?>