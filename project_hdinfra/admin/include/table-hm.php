<div class="table-responsive">
  <table border="1" cellspacing="0" width="960" align="center">
   <thead style="background-color: #FFD700; color: #FFFFFF;">
     <tr>
       <th style="height: 30px; width: 50px"><center>No.</center></th>
       <th style="height: 30px; width: 100px"><center>Berlaku Mulai</center></th>
       <th style="height: 30px; width: 100px"><center>No. Aset</center></th>
       <th style="height: 30px; width: 180px"><center>Unit Kerja</center></th>
       <th style="height: 30px; width: 150px"><center>Pengguna</center></th>
       <th style="height: 30px; width: 120px"><center>Telp</center></th>
       <th style="height: 30px; width: 200px"><center>Keterangan</center></th>
       <th style="height: 30px; width: 60px"><center>Hapus</center></th>
     </tr>
   </thead>
   <tbody>
    <?php
    include "../../koneksi.php";
    $page = (isset($_POST['page']))? $_POST['page'] : 1;
    $limit = 10; // Jumlah data per halamannya
    $no = (($page - 1) * $limit) + 1; // Untuk setting awal nomor pada halaman yang aktif
    $limit_start = ($page - 1) * $limit;
    if(isset($_POST['search']) && $_POST['search'] == true){ 
      $param = '%'.mysqli_real_escape_string($conn, $keyword).'%';
      $sql = mysqli_query($conn, "SELECT * FROM tb_history_mutasi WHERE tanggal LIKE '".$param."' OR unit_kerja LIKE '".$param."' OR no_aset LIKE '".$param."' OR pengguna LIKE '".$param."' OR ext LIKE '".$param."' LIMIT ".$limit_start.",".$limit);
      $sql2 = mysqli_query($conn, "SELECT COUNT(*) AS jumlah FROM tb_history_mutasi WHERE tanggal LIKE '".$param."' OR unit_kerja LIKE '".$param."' OR no_aset LIKE '".$param."' OR pengguna LIKE '".$param."' OR ext LIKE '".$param."'");
      $get_jumlah = mysqli_fetch_array($sql2);
    }else{ 
      $sql = mysqli_query($conn, "SELECT * FROM tb_history_mutasi LIMIT ".$limit_start.",".$limit);
      $sql2 = mysqli_query($conn, "SELECT COUNT(*) AS jumlah FROM tb_history_mutasi");
      $get_jumlah = mysqli_fetch_array($sql2);
    }
    $i=0;
    while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
      ?>
      <tr>
        <td><center><?= $forpage[$i]=$no; ?></center></td>
        <td><center><?php echo date('d-m-Y', strtotime($data['tanggal'])); ?></center></td>
        <td><center><?= $data['no_aset']; ?></center></td>
        <td><center><?= $data['unit_kerja']; ?></center></td>
        <td><center><?= $data['pengguna']; ?></center></td>
        <td><center><?= $data['ext']; ?></center></td>
        <td><center><?= $data['ket_mutasi']; ?></center></td>
        <td><center>           
        
        <a data-href="proses/hapushistory.php?id=<?php echo $data['id_history']; ?>" data-toggle="modal" data-target="#konfirmasi_hapus"><button type="button" class="btn btn-warning btn-circle btn-sm"><i class="fa fa-trash"></i>
        </button></a>
        </center></td>
      </tr>
      <?php
      $no++;
      $i++;
    }
    ?>
    </tbody>
  </table>
</div>

<?php
$count = mysqli_num_rows($sql);

if($count > 0){ // Jika datanya ada, tampilkan paginationnya
    ?>

    <div class="informasi-data "><p>Menampilkan <?= min($forpage) ?> hingga <?= max($forpage) ?> dari <?= $get_jumlah['jumlah'] ?> entri</div>
   
    <ul class="pagination">
      <!-- LINK FIRST AND PREV -->
      <?php
      if($page == 1){ // Jika page adalah page ke 1, maka disable link PREV
      ?>
        <li class="disabled"><a href="#">First</a></li>
        <li class="disabled"><a href="#">&laquo;</a></li>
      <?php
      }else{ // Jika page bukan page ke 1
        $link_prev = ($page > 1)? $page - 1 : 1;
      ?>
        <li><a href="javascript:void(0);" onclick="searchWithPagination(1, false)">First</a></li>
        <li><a href="javascript:void(0);" onclick="searchWithPagination(<?php echo $link_prev; ?>, false)">&laquo;</a></li>
      <?php
      }
      ?>

      <!-- LINK NUMBER -->
      <?php
      $jumlah_page = ceil($get_jumlah['jumlah'] / $limit); // Hitung jumlah halamannya
      $jumlah_number = 3; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
      $start_number = ($page > $jumlah_number)? $page - $jumlah_number : 1; // Untuk awal link number
      $end_number = ($page < ($jumlah_page - $jumlah_number))? $page + $jumlah_number : $jumlah_page; // Untuk akhir link number

      for($i = $start_number; $i <= $end_number; $i++){
        $link_active = ($page == $i)? ' class="active"' : '';
      ?>
        <li<?php echo $link_active; ?>><a href="javascript:void(0);" onclick="searchWithPagination(<?php echo $i; ?>, false)"><?php echo $i; ?></a></li>
      <?php
      }
      ?>

      <!-- LINK NEXT AND LAST -->
      <?php
      // Jika page sama dengan jumlah page, maka disable link NEXT nya
      // Artinya page tersebut adalah page terakhir
      if($page == $jumlah_page){ // Jika page terakhir
      ?>
        <li class="disabled"><a href="#">&raquo;</a></li>
        <li class="disabled"><a href="#">Last</a></li>
      <?php
      }else{ // Jika Bukan page terakhir
        $link_next = ($page < $jumlah_page)? $page + 1 : $jumlah_page;
      ?>
        <li><a href="javascript:void(0);" onclick="searchWithPagination(<?php echo $link_next; ?>, false)">&raquo;</a></li>
        <li><a href="javascript:void(0);" onclick="searchWithPagination(<?php echo $jumlah_page; ?>, false)">Last</a></li>
      <?php
      }
      ?>
    </ul>
    <?php
}
?>
