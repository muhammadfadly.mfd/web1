<?php
session_start();
require'koneksi.php';
function create_random($length)
{
    $data = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $string = '';
    for($i = 0; $i < $length; $i++) 
    {
        $pos = rand(0, strlen($data)-1);
        $string .= $data[$pos];
    }
    return $string;
}

?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Heldesk Infrastruktur">
    <meta name="author" content="Fadhli Dzil Ikram">
    <meta name="keywords" content="Heldesk Infrastruktur">

    <!-- Title Page-->
    <title>Heldesk Infrastruktur</title>

    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
        rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">
</head>

<body>
    <section class="form_pppk" id="form_pppk">
        <div class="page-wrapper bg-blue p-t-100 p-b-100 font-robo">
            <div class="wrapper wrapper--w680">
                <div class="card card-1">
                    <div class="card-heading"></div>
                    <div class="card-body">
                        <h2 class="title">Helpdesk Infrastruktur - IT Services </h2>

                        <div class="isiform">
                            <form method="post" action="proses_register.php">

                                <div class="input-group">
                                    <label for="no_pppk">No. Tiket</label>
                                    <input required="" name="no_pppk" type="text" class="input--style-1" id="nopppk"
                                        value="<?php echo  $num = create_random(5);?>" readonly>
                                </div>


                                <div class="row row-space">
                                    <div class="col-2">
                                        <div class="input-group">
                                            <label for="tipe_aduan">Tipe Aduan</label>
                                            <div class="rs-select2 js-select-simple select--no-search">
                                                <select class="form-control" id="tipe_aduan" name="tipe_aduan">
                                                    <option disabled="disabled" selected="selected">--Pilih Tipe Aduan
                                                        Anda--
                                                    </option>
                                                    <option>Perbaikan PC/Hardware</option>
                                                    <option>Perbaikan Jaringan</option>
                                                    <option>Remote Support</option>
                                                    <option>Video Conference</option>
                                                    <option>Lainnya</option>
                                                </select>
                                                <div class="select-dropdown"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="input-group">
                                            <label for="unit_kerja">Unit Kerja Bagian</label>
<!-- <input list="no_asets" id="unit_kerja" name="unit_kerja" placeholder="Cari no aset..." class="form-control input-sm" autocomplete="off">
      <datalist id="no_asets">
      <?php
        include'../ext/db.php';
         $tampil3 = $conn->query("SELECT * FROM tb_unitkerja ORDER BY kd_unitkerja ASC");
         while ($row3 = mysqli_fetch_array($tampil3)){
         ?>             
         <option value="<?php echo $row3['unit_kerja'];?>"></option>';
      <?php }?>
      </datalist> -->
                                            <div class="rs-select2 js-select-simple ">
                                                <select class="form-control" id="unit_kerja" name="unit_kerja" >
                                                    <option disabled="disabled" selected="selected">--Pilih Tipe Aduan
                                                        Anda--
                                                    </option>
                                                   
      <?php
        include'../ext/db.php';
         $tampil4 = $conn->query("SELECT * FROM tb_unitkerja ORDER BY kd_unitkerja ASC");
         while ($row4 = mysqli_fetch_array($tampil4)){
         ?>
         <option value="<?php echo $row4['kd_unitkerja'];?>"><?php echo $row4['unit_kerja'];?></option>';
      <?php }?>
    </select>
                                                <div class="select-dropdown"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                 <!-- auto unit kerja -->
  <div id="onajax"></div>

                                <div class="input-group">
                                    <label for="no_pppk">Penghubung</label>

                                    <input class="input--style-1" type="text" placeholder="Masukkan Nama Anda"
                                    required oninvalid="this.setCustomValidity('Harap Lengkapi Semua Kolom')"  class="form-control"  id="penghubung" name="penghubung" placeholder="Masukan nama anda" autocomplete="off">

                                </div>

                                <div class="input-group">
                                    <label for="no_pppk">Telepon/Ext</label>

                                    <input class="input--style-1" type="text"
                                    required oninvalid="this.setCustomValidity('Harap Lengkapi Semua Kolom')" class="form-control" id="telepon" placeholder="Tambahkan nomor telepon/ext. anda" name="telepon" autocomplete="off">

                                </div>

                                <div class="input-group">
                                    <label for="no_pppk">Email Penghubung</label>
                                    <input class="input--style-1" type="email" placeholder="Masukkan Email Anda"
                                    class="form-control" id="email" required oninvalid="this.setCustomValidity('Harap Lengkapi Semua Kolom')" placeholder="Masukan e-mail anda" name="email" required="" autocomplete="off">
                                </div>

                                <div class="input-group">
                                    <label for="no_pppk">Email Atasan</label>
                                    <input class="input--style-1" type="email" placeholder="Masukkan Email Atasan Anda"
                                    class="form-control" id="email" required oninvalid="this.setCustomValidity('Harap Lengkapi Semua Kolom')"  name="emailatasan" required="" autocomplete="off">
                                </div>

                                <div class="p-t-20">
                                    <button class="btn btn--radius btn--green" type="submit" name="submit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </section>

    <!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js/global.js"></script>

    <script src="link/js3.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="link/js4.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="asset/jQuary-3.3.1/jquery-3.3.1.min.js"></script>
<script src="asset/bootstrap-4.3.1/js/bootstrap.min.js"></script>
<script src="asset/select2.min.js"></script>
    <script>
        $("#unit_kerja").change(function(){
            var id = $("#unit_kerja").val();
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "ajax_lokasi.php?id="+id,
                success: function(msg){
                     // alert(msg);
                    if(msg == ''){
                        alert('Tidak ada data');
                    }
                    else{
                        $("#onajax").html(msg);
                    }
                }
            });
        });

        $("#captinput").change(function(){
            var code = $("#captinput").val();

            $.ajax({
                type: "POST",
                dataType: "html",
                url: "chapta/periksa_captcha.php?nilaiCaptcha="+code,
                success: function(msg){
                    // alert(msg);
                    if(msg == ''){
                        alert('Tidak ada data');
                    }
                    else{
                        $("#respon").html(msg);
                    }
                }
            });
        });

    $(document).ready(function(){
        $('#input_keluhan').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            //menggunakan fungsi ajax untuk pengambilan data
             // alert(rowid);
            $.ajax({
                type : 'post',
                // url : 'ajax_keluhan.php',
                url : 'ajax_keluhan.php?idppk='+rowid,
                data :  'rowid='+ rowid,
                success : function(data){

                $('.fetched-data').html(data);//menampilkan data ke dalam modal
                }
            });
         });
    });

    </script>

    <script type="text/javascript">
$(document).ready(function() {
     $('#unit_kerja').select2({
      placeholder: 'Pilih Unit Kerja',
      allowClear: true
     });
 });
</script>

</body>

</html>
<!-- end document-->