<?php
session_start();
require'koneksi.php';

$idpppk = $_GET['id'];
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- <link rel="stylesheet" type="text/css" href="style.css"> -->
    <link rel="stylesheet" type="text/css" href="costum.css">
    <link href="font/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
   
    <title>index.php</title>
  </head>
  <body>

<div class="jumbotron jumbotron-fluid">
  <div class="container text-center">
    <div class="runing"><b> <center> <marquee direction="left" scrollamount="4" align="center">
      'Selamat Datang di Form PPPK - Bengkel Komputer - Departemen TI - PT PUSRI PALEMBANG'
    </marquee> </center> </b></div>
    <img src="img/pusri.png" class="logo">
     <h1 class="display-10">PT PUSRI PALEMBANG</h1> 
    <p class="lead">Departemen TI</p>
  </div>
</div>

<section class="form_pppk" id="form_pppk">
 <div class="container">
  <div class="row">
  <div class="col-sm-12 text-center ">
  <h3 class="text-center"> <b> <font color="000080">  </font> </b> </h3>
  <br>
  </div>
  </div>

  <div class="row">
    <div class="col-sm-5 img-pusri">
      <div class="p-3 mb-2 bg-info text-white">
      <!-- <div class="title-petunjuk">Pentunjuk</div> -->
      <!-- <hr> -->
      <div class="isi-petunjuk">
        <!-- <ul>
          <li>Jika anda belum pernah mengajukan keluhan silahkan pilih menu registrasi</li>
          <li>Jika anda pernah mengajukan keluhan silahkan pilih menu keluhan</li>
        </ul> -->
        <!-- <p> Jika anda belum pernah mengajukan keluhan silahkan pilih menu registrasi</p>
        <p> Jika anda pernah mengajukan keluhan silahkan pilih menu keluhan</p> -->
      </div>
    </div>
      <ul class="nav nav-tabs">
             <!--  <li class="nav-item text-center">
                <a class="btn btn-success"  href="index.php">
                  <h4>Registrasi </h4>
                </a>
              </li>


              <li class="nav-item text-center">
                <a  class="btn btn-info"  href="keluhan.php">
                  <h4>Keluhan</h4>
                </a>
              </li> -->
    </ul>

      <img src="img/pusri.png" class="pusri">
      <p>Pusri Untuk Kemandirian Pangan Dan Kehidupan Yang Lebih Baik</p>
      <!-- <img src="img/bg.jpg" width="80"> -->
    </div>
   
    <div class="col-sm-7  daftar">
    <div class="scheduleTab">

    <div class="tab-content">
      <div id="day1" class="tab-pane active">
         <div class="head-form">
          <p>Registrasi<p>    
        </div>
       <div class="isiform">
              <div class="form-group">
                <a href="keluhan.php?idppk=<?php echo $idpppk; ?>"> <button type="submit" class="btn btn-success" name="add">&plus; Tambah Keluhan</button> </a>
              </div>
              <hr>
              
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>No pppk</th>
                    <th>No Aset</th>
                    <th>Keluhan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
<?php
    

    $sql1 = "SELECT * FROM tb_detail where no_pppk = '$idpppk' ORDER BY no_aset ASC";
    $query2 = mysqli_query($conn, $sql1);
    $no   = 1;
    while ( $row = mysqli_fetch_array($query2))
    {
      echo ' <tr class="odd gradeX">
          <td>'.$no.'</td>
          <td>'.$row['no_pppk'].'</td>
          <td>'.$row['no_aset'].'</td>
          <td>'.$row['keluhan'].'</td>
          <td align="center">
          <a data-href="hapus.php?idhps='.$row['id_detail'].'&idppk='.$row['no_pppk'].'" data-toggle="modal" data-target="#konfirmasi_hapus"><button type="button" class="btn btn-warning btn-circle"><i class="fa fa-times"></i>
          </button></a></td></tr>
      '; 
      $no++;       
    }
?>   
<div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <b>Anda yakin ingin menghapus data surat masuk ini?</b><br><br>
                    <a class="btn btn-danger btn-ok"> Hapus</a>
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
                </div>
            </div>
        </div>
    </div>                
                  
  <script type="text/javascript">
    //Hapus Data
    $(document).ready(function() {
        $('#konfirmasi_hapus').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });
    });
  </script>                           
                </tbody>
              </table>
              <br>
              <a class="btn btn-info" target="_blank" href="report.php?id=<?php echo $idpppk; ?>">Print</a>
        </div>
    </div>
  </div>

</div>
</div>
 
  

 

  

  

  
  
    
    
  
</section>
<br>
<br>


        

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="asset/jQuary-3.3.1/jQuary-3.3.1.min.js"></script> -->
    <!-- <script src="asset/bootstrap-4.3.1/js/bootstrap.min.js"></script> -->
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
