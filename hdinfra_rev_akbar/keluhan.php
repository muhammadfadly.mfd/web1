<?php
session_start();
require'koneksi.php';
$idpppk = $_GET['id'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Heldesk Infrastruktur">
    <meta name="author" content="Fadhli Dzil Ikram">
    <meta name="keywords" content="Heldesk Infrastruktur">

    <!-- Title Page-->
    <title>Keluhan</title>

    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
        rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS -->
    <link href="css/main.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- <link rel="stylesheet" type="text/css" href="style.css"> -->
    <link rel="stylesheet" type="text/css" href="costum.css">
    <link href="font/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>
    <section class="form_pppk" id="form_pppk">
        <div class="page-wrapper bg-blue p-t-100 p-b-100 font-robo">
            <div class="wrapper wrapper--w680">
                <div class="card card-1">
                    <div class="card-heading"></div>
                    <div class="card-body center">
                        <h2 class="text-center">Harap Diisikan Keluhan Anda</h2>

                    

                            <div class="container">
                                <div><br></div>
                                <div class="form-group">
                                    <div class="col text-center">
                                        <button type="button" class="btn btn-center btn-primary" data-toggle="modal"
                                            data-target="#input_keluhan" data-id="<?php echo $idpppk; ?>">&plus; Tambah
                                            Keluhan</button></div>

                                    <div class="container">
                                        <div class="row">
                                            <div class="d-flex justify-content-center">
                                                <button class="text-center">"Silahkan isi Keluhan dari masalah komputer
                                                    anda, dapat mengisi keluhan Lebih dari 1 kali"</button><br>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div id="data"></div> -->

                                    <?php
// session_start();
// require'koneksi.php';
// $idpppk = $_GET['id'];
?>
                            
            <div class="card">

          <div class="card-tools">
              <div class="input-group-append">
                </div>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0" style="height: 800px;">
          
                                    <div id="here">
                                        <table class="table table-bordered table-dark" id="sampleTable">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>No. Tiket</th>
                                                    <th>No. Aset</th>
                                                    <th>Spesifikasi</th>
                                                    <th>Jenis</th>
                                                    <th>Keluhan</th>
                                                    <th>Hapus</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    <!-- id='here' -->
                                                <?php
    $sql1 = "SELECT * FROM tb_detail join tb_aset on tb_detail.no_aset=tb_aset.no_aset where no_pppk = '$idpppk' ORDER BY tb_detail.no_aset ASC";
    $query2 = mysqli_query($conn, $sql1);
    $no   = 1;
    while ( $row = mysqli_fetch_array($query2))
    {
      echo ' <tr class="odd gradeX">
          <td>'.$no.'</td>
          <td>'.$row['no_pppk'].'</td>
          <td>'.$row['no_aset'].'</td>
          <td>'.$row['spesifikasi'].'</td>
          <td>'.$row['jenis'].'</td>
          <td>'.$row['keluhan'].'</td>
          <td align="center">
          <a data-href="hapus.php?idhps='.$row['id_detail'].'&idppk='.$row['no_pppk'].'&idaset='.$row['no_aset'].'" data-toggle="modal" data-target="#konfirmasi_hapus"><button type="button" class="btn btn-warning btn-circle"><i class="fa fa-times"></i>
          </button></a></td></tr>
      '; 
      $no++;       
    }
?>

                                                <script type="text/javascript">
                                                    //Hapus Data
                                                    $(document).ready(function () {
                                                        $('#konfirmasi_hapus').on('show.bs.modal', function (
                                                            e) {
                                                            $(this).find('.btn-ok').attr('href', $(e
                                                                .relatedTarget).data('href'));
                                                        });
                                                    });
                                                </script>
                                            </tbody>
                                        </table>
                                        <br>
                                        <!-- <a class="btn btn-info" target="_blank" href="report.php?id=<?php echo $idpppk; ?>">Print</a> -->


                                        <br><br>
                                    </div>

                                    <!-- <div id="tabel"></div> -->
                                    <!-- captcha -->


                                    <div>
                                        <label for="keluhan">Captcha</label>
                                        <img src="chapta/captcha.php" alt="gambar" />
                                        <a id="captcha_reload"
                                            href="keluhan.php?id=<?php echo $idpppk; ?> & #captcha_reload"> <i
                                                class="fa fa-refresh"></i> Reload</a>
                                    </div>

                                    <?php 
    if(isset($_GET['pesan'])){
      if($_GET['pesan'] == "salah"){

        echo "<br><div class='alert alert-warning alert-dismissible fade show' role='alert'>
  <strong>Captcha tidak sesuai!</strong> Input captcha sesuai gambar diatas.
  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
    <span aria-hidden='true'>&times;</span>
  </button>
</div>";
      }
    }
    ?>
                                    <form action="report.php" method="post">
                                        <div>
                                            <label>Enter the code from the image here:</label>
                                            <input hidden="" type="text" class="form-control"
                                                value="<?php echo $idpppk; ?>" name="idpppk">

                                            <input required="" type="text" class="form-control" id="captinput"
                                                name="nilaiCaptcha" placeholder="Masukan kode pada gambar di atas"
                                                autocomplete="off">
                                        </div>

                                        <br>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success" name="add">Lanjutkan
                                                >></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>


    <div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <b>Anda yakin ingin menghapus data ini?</b><br><br>
                    <a class="btn btn-danger btn-ok"> Hapus</a>
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i>
                        Batal</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="input_keluhan" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Input keluhan</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="fetched-data"></div>
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button> -->
                </div>
            </div>
        </div>
    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="asset/jQuary-3.3.1/jquery-3.3.1.min.js"></script>

    <script>
        $("#unit_kerja").change(function () {
            var id = $("#unit_kerja").val();
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "ajax_lokasi.php?id=" + id,
                success: function (msg) {
                    // alert(msg);
                    if (msg == '') {
                        alert('Tidak ada data');
                    } else {
                        $("#onajax").html(msg);
                    }
                }
            });
        });

        $("#captinput").change(function () {
            var code = $("#captinput").val();

            $.ajax({
                type: "POST",
                dataType: "html",
                url: "chapta/periksa_captcha.php?nilaiCaptcha=" + code,
                success: function (msg) {
                    // alert(msg);
                    if (msg == '') {
                        alert('Tidak ada data');
                    } else {
                        $("#respon").html(msg);
                    }
                }
            });
        });



        $(document).ready(function () {
            $('#input_keluhan').on('show.bs.modal', function (e) {
                var rowid = $(e.relatedTarget).data('id');
                //menggunakan fungsi ajax untuk pengambilan data
                // alert(rowid);
                $.ajax({
                    type: 'post',
                    // url : 'ajax_keluhan.php',
                    url: 'ajax_keluhan.php?idppk=' + rowid,
                    data: 'rowid=' + rowid,
                    success: function (data) {

                        $('.fetched-data').html(data); //menampilkan data ke dalam modal
                    }
                });
            });
        });

        var auto_refresh = setInterval(
            function () {
                var noppk = $("#nopppk").val();
                $('#data').load('ajax_tabel.php?id=' + noppk);
            }, );
    </script>

    <script type="text/javascript">
        $(".chosen").chosen();
    </script>
</body>>

</html>
<!-- end document-->